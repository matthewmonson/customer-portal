export class Product {
    id : number;
    erpItemId : number;
    erpItemCode : string;
    description : string;
    uom : string;
    quantity : number;
}
