export class ServiceRegistration {
    serviceUsername: string = "";
	servicePassword: string = "";
	serviceUrl: string = "";
}

export class SupplierSystemSpec {
    logo : string = "";
	proformaTemplate: string = "";
	theme: string = "";
    serviceRegistration: ServiceRegistration;

    constructor() {
        this.serviceRegistration = new ServiceRegistration();
    }
}
