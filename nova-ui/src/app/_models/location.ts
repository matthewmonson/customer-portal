import { Audit } from "./audit";
import { LocationType } from "./locationtype";
import { ContactPerson } from "./contactperson";
import { Address } from "./address";

export class Location {
    effectiveFrom: Date;
    effectiveTo: Date;
    code: string;
    name: string;
    description: string;
    _id: string;
    organization: string;
    owners: string[];
    audit : Audit;
    locationType: LocationType;
    parentLocation: string;
    childLocations: string[];
    contactPerson: ContactPerson;
    address: Address;
}
