export class Child2 {
    label: string;
    icon: string;
    data: string;
}

export class Child {
    label: string;
    data: string;
    expandedIcon: string;
    collapsedIcon: string;
    children: Child2[];
    icon: string;
}

export class Datum {
    label: string;
    data: string;
    expandedIcon: string;
    collapsedIcon: string;
    children: Child[];
}

export class DataTree {
    data: Datum[];
}