import { Product } from '../_models/product';
import { OrderPrice } from './orderdetail';

// Models used in UI
export class QuotationUILine {
    line : number;
    description : string;
    erpItemCode : string;
    posProductId : number;
    warehouseId : string;  
    quantity : number;
    acceptedquantity : number;
    grossAmount : number;
    taxAmount : number;
    discountAmount : number;
    netAmount : number;    
    uom : string;
    available : number;
    origavailable : number;
    product : Product;
}
export class QuotationUI {    
    quotationNumber : string = "";
    createdDate : string  = "";
    createdByUser : string = "";
    issuedWarehouse : string = "";
    status : string = "";    
    issuedWarehouseId : string = "";
    quoteStatusId : string = "";
    quoteLines : QuotationUILine[] = [];
    grossAmount : number = 0;
    taxAmount : number = 0;
    netAmount : number = 0;
    discountAmount : number = 0;
    paymentType : number;
    orderNumber : string = "";
    //acceptedDate : string = "";
    //acceptedByUser : string = "";
}

// DTOs used for API invocations
export class QuotationDetails {
    id : number;
	quotationNumber: string;
    context: string;
    currencyCode: string;
	billToId: number;
	shipToId: number;
	paymentTypeId: number;
    paymentTerm : string;
	quotationStatusId: number;
	warehouseId: number;
    createdByUser: string; // MongoDB field only
    acceptedByUser: string; // MongoDB field only
    quotationDetails: QuotationDetailLine[] = [];
    quotationPrice: OrderPrice;
}
export class QuotationDetailLine {
    lineNumber: number;
	quantity: number;
    uom: string;
	stockDetailId: number;
}
export class QuotationStatus {
    id: string;
    name: string;
}
export class Quotation {
    amount: number;
    clientId: number;
    createdDate: string;
    id: number;
    quotationNumber: string;
    quotationStatus: QuotationStatus;
    status: string;
    warehouse: string;
    warehouseId: number;
    orderNumber : string;
    quotationPrice: OrderPrice;
    quotationDetails: QuotationDetails;
}