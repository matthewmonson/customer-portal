import { TrackingDefinition } from "./trackingdefinition";
import { UOM } from "./uom";

export class InvItemUOMTracking {
    uom: UOM;
    trackingDefinition: TrackingDefinition;
}
