import { Audit } from "./audit";

export class TrackingDefinition {
    _id: any;
    code: string;
    organization: string;
    owners: string[];
    audit : Audit;
    type: string;
    length: number;
    radix: number;
    regexPattern: string;
}
