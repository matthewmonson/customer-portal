export class Address {
    addressLine1: String;
	addressLine2: String;
	addressLine3: String;
	city: String;
	state: String;
	zipCode: String;
	countryCode: String;
	gpsPoint: GpsPoint;	
	siteName : String;
	contactPerson : String;
	contactMobile : String;
	erpSiteNumber : String;	
	erpSiteId : number;
	posClientId : number;
	constructor() {
		this.gpsPoint = new GpsPoint();
	}
}

export class GpsPoint {
    latitude: String;
    longitude: String;
}