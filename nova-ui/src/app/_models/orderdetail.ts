import { Quotation } from './quotation';


export class OrderStatus {
    id: number;
    name: string;
}

export class Warehouse {
    id: number;
    name: string;
}

export class PaymentType {
    id: number;
    btbEnabled: number;
    btcEnabled: number;
    enabled: number;
    name: string;
    qtEnabled: number;
}

export class Warehouse2 {
    id: number;
    name: string;
}

export class StockDetail {
    id: number;
    label: string;
    primaryUom: string;
}

export class OrderDetailPrice {
    id: number;
    discountRateFraction: number;
    discountValue: number;
    grossValue: number;
    lineNumber: number;
    netValue: number;
    paymentTerm: string;
    posProductId: number;
    posWarehouseId: number;
    quantity: number;
    taxValue: number;
    taxValueNoDiscount: number;
    unitValue: number;
    uom: string;
    value: number;
    valueNoDiscount: number;
}

export class OrderDetailTax {
    id: number;
    tax: string;
    taxAmount: number;
    taxRate: number;
    taxType: string;
    taxAmountNoDiscount: number;
    taxRateCode: string;
    taxRegime: string;
}

export class OrderDetailPriceResolution {
    id: number;
    arithmeticOperator: string;
    levelCode: string;
    listLine: string;
    modifierNumber: string;
    name: string;
    operand: number;
    sourceSystemCode: string;
}

export class OrderDetailLine {
    id: number;
    discount: number;
    discountValue: number;
    netValue: number;
    quantity: number;
    serialEnd?: any;
    serialStart?: any;
    value: number;
    warehouse: Warehouse2;
    stockDetail: StockDetail;
    orderDetailPrice: OrderDetailPrice;
    orderDetailTaxes: OrderDetailTax[];
    orderDetailPriceResolutions: OrderDetailPriceResolution[];
}

export class BillTo {
    id: number;
    name: string;
}

export class Client {
    id: number;
    name: string;
}

export class OrderTax {
    id: number;
    tax: string;
    taxAmount: number;
    taxAmountNoDiscount: number;
    taxRate?: any;
    taxRateCode?: any;
    taxRegime?: any;
}

export class Client2 {
    id: number;
    name: string;
}

export class OrderPrice {
    id: number;
    currencyCode: string;
    paymentTypeDiscountApplied: boolean;
    priceDate: number;
    priceList: string;
    totalDiscountValue: number;
    totalGrossValue: number;
    totalNetValue: number;
    totalTaxValue: number;
    totalTaxValueNoDiscount: number;
    totalValue: number;
    totalValueNoDiscount: number;
    client: Client2;
}

export class ShipTo {
    id: number;
    name: string;
}

export class OrderDetail {
    quotationNumber: string;
    orderNumber: string;
    quotationId: number;
    lastStatusChangeTime: number;
    lastStatusChangeUserId: number;
    orderStatus: OrderStatus;
    warehouse: Warehouse;
    orderPriceResolutions: any[];
    dispatchedDate?: any;
    paymentType: PaymentType;
    creationUserId: number;
    orderDetails: OrderDetailLine[];
    clientMasterId: number;
    createdDate: string;
    billTo: BillTo;
    createdTime: number;
    client: Client;
    orderTaxes: OrderTax[];
    typeId: number;
    orderPrice: OrderPrice;
    id: number;
    shipTo: ShipTo;
    quotation: Quotation;
}
