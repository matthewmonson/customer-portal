import { Audit } from "./audit";

export class Image {
    code: string;
    name: string;
    description: string;
    _id: string;
    organization: string;
    owners: string[];
    audit : Audit;
    mimeType: string;
    size: number;
    'x-resolution': number;
    'y-resolution': number;
    file: File;
    src: any;
}
