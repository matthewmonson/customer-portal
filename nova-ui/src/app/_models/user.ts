﻿export class User {
    username: string;
    firstname: string;
    lastname: string;
    mobile: string;
    email: string;
    organization: string;
    roles : string[];
    mustChangePassword : boolean;
    orgType : string;
}
