export class Order {
    orderNumber : string;
    quotationNumber : string;
    createdDate : string;
    dispatchedDate : string;
    warehouse : string;
    status : string;
    customerName : string;
    clientId: string;
    warehouseId : string;
    orderStatusId : string;
    id : string;
}