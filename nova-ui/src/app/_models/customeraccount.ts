export class CustomerAccount {
    customerName : string;
    erpCustomerId : number;
    erpCustomerAccountNumber : string = "";
    blacklisted : boolean = false;
}