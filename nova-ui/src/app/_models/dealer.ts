import { ContactPerson } from './contactperson';
import { Address } from './address';
import { CustomerAccount } from './customeraccount';
import { SupplierSystemSpec } from './suppliersystemspec';

export class Dealer {
	accountNumber: string;
    code: string;
	name: string;
	taxNumber: string;
	supplier: string;
    billToAddress: Address;
	shipToAddress: Address;
    registrationNumber: string;
    contactPersons: ContactPerson[] = [];
	supplierSystemSpec: SupplierSystemSpec;	
	customerAccount : CustomerAccount;	
	isSupplier : boolean = false;
	constructor() {
		this.contactPersons.push(new ContactPerson());
		this.billToAddress = new Address();
		this.shipToAddress = new Address();
		this.supplierSystemSpec = new SupplierSystemSpec();
		this.customerAccount = new CustomerAccount();
	}
}

export class ClientDetail {
	id: number;
	accountBalance: number;
	address1: string;
	address2?: any;
	address3: string;
	address4?: any;
	address5?: any;
	address6?: any;
	billTo: boolean;
	blocked: boolean;
	chequeLimit: boolean;
	clientTypeId: number;
	contactMsisdn?: any;
	contactPerson: string;
	contactTelephone?: any;
	creditLimit: number;
	delivery1: string;
	delivery2?: any;
	delivery3: string;
	delivery4?: any;
	effectiveFrom: string;
	effectiveTo?: any;
	email?: any;
	erpCustomerTaxStatusId: number;
	erpSiteId: number;
	erpSiteNumber: string;
	financialCode?: any;
	ispLogin?: any;
	name: string;
	postpaidRef?: any;
	primaryBillTo: boolean;
	primaryShipTo: boolean;
	purchaseOnCredit: number;
	registrationNumber?: any;
	shipTo: boolean;
	taxNumber?: any;
	taxExempt: boolean;
	transfer: boolean;
	vat?: any;
}

export class DealerAccountDetail {
	id: number;
	blacklisted: boolean;
	creditLimit: number;
	effectiveFrom: string;
	effectiveTo: string;
	accountNumber: string;
	erpCustomerId: number;
	paymentTerms: string;
	clientDetails: ClientDetail[];
}

