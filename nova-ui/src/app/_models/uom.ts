import { Audit } from "./audit";

export class UOM {
    code: string;
    name: string;
    description: string;
    _id: any;
    organization: string;
    owners: string[];
    audit : Audit;
    quantityOf: string;
    quantity: number;
    isPrimary: boolean;
}
