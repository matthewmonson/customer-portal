import { Audit } from "./audit";

export class LocationType {
    code: string;
    name: string;
    description: string;
    _id: string;
    organization: string;
    owners: string[];
    audit : Audit;
}
