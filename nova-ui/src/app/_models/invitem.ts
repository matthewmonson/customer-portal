import { Audit } from "./audit";
import { Category } from "./category";
import { Image } from "./image";
import { InvItemUOMTracking } from "./invitemuomtracking";
import { Location } from './location';

export class InvItemLocation {
    location : Location;
    effectiveFrom: Date;
    effectiveTo: Date;    

    constructor() {
        this.location=new Location();
    }
}

export class InvItem {
    effectiveFrom: Date;
    effectiveTo: Date;
    code: string;
    name: string;
    description: string;
    _id: string;
    organization: string;
    owners: string[];
    audit : Audit;
    category : Category;
    brand: string;
    barcode: string;
    weight: number;
    weightUnit: string;
    itemUomTracking: InvItemUOMTracking[] = [];
    locations: InvItemLocation[] = [];
    images: Image[] = [];
}
