import { ContactPerson } from './contactperson';
import { Address } from './address';
import { SupplierSystemSpec } from './suppliersystemspec';
export class Opco {
    code: String;
	name: String;
	taxNumber: String;
	parentOpCo: String;
    address: Address;
    registrationNumber: String;
    contactPersons: ContactPerson[] = [];
	supplierSystemSpec: SupplierSystemSpec;
	constructor() {
		this.address = new Address();
		this.contactPersons.push(new ContactPerson());
		this.supplierSystemSpec = new SupplierSystemSpec();
	}
}
