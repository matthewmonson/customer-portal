export class CreditExposure {
    erpexchangeRate: string;
    erptotalExposure: string;
    erpcustomerHoldFlag: string;
    erpcreditLimitCurrencyCode: string;
    erporderLimitAmount: string;
    erpreceivablesExposureAmount: string;
    erpexternalExposureAmount: string;
    erpstatusMessage: string;
    erporderExposureAmount: string;
    erpoverallCreditLimit: string;
    erpcreditExposureDate: string;
    postotalUnpostedReceipts: number;
    erpreceivablesAmountPastDue: string;
    postotalUnpostedCreditNotes: number;
    poscalculatedCreditAvailable: number;
    postotalUnpostedSalesOrders: number;
    erpstatus: string;
}