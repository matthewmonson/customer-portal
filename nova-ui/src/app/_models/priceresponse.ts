export class TaxDetail {
	tax: string;
	taxAmount: number;
	taxAmountNoDiscount: number;
	taxResolutionDetail?: any;
}

export class TotalTaxDetails {
	taxDetail: TaxDetail[];
}

export class TaxResolutionDetail {
	taxRate: number;
	taxRateCode: string;
	taxRegime: string;
}

export class TaxDetail2 {
	tax: string;
	taxAmount: number;
	taxAmountNoDiscount: number;
	taxResolutionDetail: TaxResolutionDetail;
}

export class TaxDetails {
	taxDetail: TaxDetail2[];
}

export class PriceResolutionDetail {
	arithmeticOperator: string;
	levelCode: string;
	listLine: string;
	modifierNumber: string;
	name: string;
	operand: number;
	sourceSystemCode: string;
}

export class PriceResolutionDetails {
	priceResolutionDetail: PriceResolutionDetail[];
}

export class LineResponse {
	discountRateFraction: number;
	discountValue: number;
	discountValueInclusive: number;
	grossValue: number;
	lineNumber: number;
	netValue: number;
	paymentTerm: string;
	posProductId: number;
	posWarehouseId: number;
	quantity: number;
	taxValue: number;
	taxValueNoDiscount: number;
	unitValue: number;
	unitValueInclusive: number;
	uom: string;
	value: number;
	valueNoDiscount: number;
	taxDetails: TaxDetails;
	priceResolutionDetails: PriceResolutionDetails;
}

export class LineResponses {
	lineResponse: LineResponse[];
}

export class PriceResolutionDetails2 {
	priceResolutionDetail: any[];
}

export class PriceResponseDetails {
	currencyCode: string;
	paymentTypeDiscountApplied: boolean;
	posCustomerId: number;
	priceDate: number;
	priceList: string;
	totalDiscountValue: number;
	totalDiscountValueInclusive: number;
	totalGrossValue: number;
	totalNetValue: number;
	totalTaxDetails: TotalTaxDetails;
	totalTaxValue: number;
	totalTaxValueNoDiscount: number;
	totalValue: number;
	totalValueNoDiscount: number;
	lineResponses: LineResponses;
	priceResolutionDetails: PriceResolutionDetails2;
}

export class PriceResponse {
	statusCode: string;
	statusDescription: string;
	priceResponseDetails: PriceResponseDetails;
}



