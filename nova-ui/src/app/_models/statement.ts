

export class CustomerAccountDetailsLine {
    transactionId: number;
    requestId?: any;
    sourceName: string;
    transactionClass: string;
    transactionType: string;
    customerAccNum: string;
    customerAccountId: number;
    customerName: string;
    customerPartyId: number;
    customerSiteNum: string;
    customerSiteUseId: number;
    customerLocation: number;
    customerLocationId: number;
    transactionDate: any;
    ebsTransactionNum: string;
    invoiceNum: string;
    salesOrderNum: string;
    receiptNum: string;
    sourceRefNum: string;
    transactionCurrency: string;
    soHeaderId?: number;
    salesOrderAmount?: number;
    transactionAmount: number;
    outstdTrxAmt: number;
    operatingUnit: string;
    dueDate: any;
    ebsId: number;
    cashReceiptId?: any;
    custTrxId: number;
    createdBy: number;
    creationDate: any;
    lastUpdatedBy: number;
    lastUpdateDate: any;
    userId?: any;
    respId?: any;
    respAppId?: any;
    cattribute3?: any;
    dattribute2?: any;
    dattribute3?: any;
    nattribute1?: any;
    nattribute2?: any;
    cattribute2?: any;
    cattribute1?: any;
    dattribute1?: any;
    nattribute3?: any;
}

export class CustomerAccountDetailsSalesLines {
    customerAccountDetailsLine: CustomerAccountDetailsLine[];
}

export class CustomerAccountDetailsLine2 {
    transactionId: number;
    requestId?: any;
    sourceName: string;
    transactionClass: string;
    transactionType: string;
    customerAccNum: string;
    customerAccountId: number;
    customerName: string;
    customerPartyId: number;
    customerSiteNum: string;
    customerSiteUseId: number;
    customerLocation: number;
    customerLocationId: number;
    transactionDate: any;
    ebsTransactionNum: string;
    invoiceNum: string;
    salesOrderNum?: any;
    receiptNum: string;
    sourceRefNum: string;
    transactionCurrency?: any;
    soHeaderId?: any;
    salesOrderAmount?: any;
    transactionAmount: number;
    outstdTrxAmt: number;
    operatingUnit: string;
    dueDate: any;
    ebsId: number;
    cashReceiptId: number;
    custTrxId?: any;
    createdBy: number;
    creationDate: any;
    lastUpdatedBy: number;
    lastUpdateDate: any;
    userId?: any;
    respId?: any;
    respAppId?: any;
    cattribute3?: any;
    dattribute2?: any;
    dattribute3?: any;
    nattribute1?: any;
    nattribute2?: any;
    cattribute2?: any;
    cattribute1?: any;
    dattribute1?: any;
    nattribute3?: any;
}

export class CustomerAccountDetailsReceiptsLines {
    customerAccountDetailsLine: CustomerAccountDetailsLine2[];
}

export class Statement {
    statusCode: string;
    statusDescription: string;
    responseSource: string;
    customerAccountDetailsSalesLines: CustomerAccountDetailsSalesLines;
    customerAccountDetailsReceiptsLines: CustomerAccountDetailsReceiptsLines;
}

