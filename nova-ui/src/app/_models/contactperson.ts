import { ContactNumber } from './contactnumber';

export class ContactPerson {
    title: String;
	initials: String;
	firstname: String;
	lastname: String;
    contactNumbers: ContactNumber[] = [];
}