import { Audit } from "./audit";

export class Category {
    code: string;
    name: string;
    description: string;
    _id: string;
    organization: string;
    owners: string[];
    audit : Audit;
}