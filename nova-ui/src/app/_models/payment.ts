export class PaymentStatus {
    id: number;
    name: string;
}

export class Payment {
    id: number;
    clientId: number;
    paymentNumber: string;
    paymentDate: string;
    paymentTypeId: number;
    paymentType: string;
    amount: number;
    paymentStatusId: number;
    status: string;
    msisdn: string;
}

export class Cashier {
    id: number;
    name: string;
}

export class PaymentType {
    id: number;
    btbEnabled: number;
    btcEnabled: number;
    enabled: number;
    name: string;
    qtEnabled: number;
}

export class ReceiptMethod {
    id: number;
    bankBranchName: string;
    name: string;
}

export class TransactionType {
    id: number;
    erpTransactionType: string;
    name: string;
}

export class PaymentDetail {
    id: number;
    paymentDate: string;
    clientId: number;
    clientName: string;
    paymentNumber: string;
    unallocatedAmount?: any;
    amount: string;
    cashier: Cashier;
    paymentStatus: PaymentStatus;
    paymentType: PaymentType;
    receiptMethod: ReceiptMethod;
    transactionType: TransactionType;
    mobileMoneyPayments?: {id: number, statusId: number}
}
