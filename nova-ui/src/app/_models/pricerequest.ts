export class LineRequest {
    lineNumber : string;
	paymentTerm : string;
	posProductId : string;
	posWarehouseId : string;
	quantity : string;
	uom : string;
}

export class LineRequests {
	lineRequest : LineRequest[] = [];
}

export class PriceRequest {
	context : string;
	currencyCode : string;
	paymentType : string;
	posCustomerId : string;
	priceDate : string;
	lineRequests : LineRequests;
}
