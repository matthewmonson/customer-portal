import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SessionService } from '../session.service';
import { User } from '../_models/user';
import { HttpClient } from '@angular/common/http';
import { AlertService } from '../alert.service';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.scss'],
})
export class OtpComponent {

    otp: string;

    index: number = 0;
    cssClass: string = '';

    animation: boolean = true;
    keyboard: boolean = false;
    backdrop: string = 'static';
    css: boolean = false;
    
    currentUser : User;

    invalidpin : boolean = false;
    pinresendfailure : boolean = false;
    pinresent : boolean = false;

  constructor(private http : HttpClient,
    private session: SessionService,
    public dialogRef: MatDialogRef<OtpComponent>,
    private alertService: AlertService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    onCancel(): void {
      this.dialogRef.close(false);
    }

    requestNewOtp() {

      this.session.setloading(true);
      this.invalidpin=false;
      this.pinresent=false;
      this.pinresendfailure=false;
      this.currentUser = this.session.getSessionUser();

      // set the body - empty
      let body = JSON.stringify({  });

      // invoke Nova Service to request a new OTP URL
      this.http.patch(this.data.otpurl, body)
          .subscribe(
              (response: any) => {                                          
                  this.session.setloading(false);
                  this.pinresent = true;
                  this.alertService.success("New PIN requested");
              },
              error => {
                  this.alertService.error("Failed to request new PIN, please try again");
                  this.session.setloading(false);
                  this.pinresendfailure=true;
              }
          );
  }

  cancelOtp() {
      this.data.otpurl=null;       
      this.dialogRef.close(false);
  }

  submitOtp() {

      this.session.setloading(true);      
      this.pinresent=false;
      this.pinresendfailure=false;
      this.currentUser = this.session.getSessionUser();

      // set the body - empty
      let body = JSON.stringify({  });

      // invoke Nova Service to confirm the One-Time PIN
      this.http.post(this.data.otpurl, body, { headers: {'X-Casanova-OTP': this.otp } })
          .subscribe(
              (response: any) => {       
                  this.invalidpin=false; 
                  this.dialogRef.close(true);
                  this.session.setloading(false);
              },
              error => {
                  this.session.setloading(false);
                  this.invalidpin=true;
                  console.log("Invalid OTP, please try again");        
                  this.alertService.error("Invalid OTP, please try again");          
              }
          );
  }
}
