import { Injectable } from '@angular/core';
import { SessionService } from './session.service';
import { ConfigService } from './config.service';
import { AlertService } from './alert.service';
import { Payment, PaymentDetail, PaymentType } from './_models/payment';
import { UtilityService } from './utility.service';
import { Status } from './_models/status';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  NovaServiceApiUrl : string;

  PaymentAccountManagementPaymentsApiPath : string;
  PaymentAccountManagementStatusesApiPath : string;
  PaymentAccountManagementPaymentTypesApiPath : string;
  DocumentManagementReceiptsApiPath : string;
  PaymentsManagementPaymentsPath : string;
  HttpRequestTimeout : number;
  
  constructor(private session: SessionService, private config: ConfigService, private http: HttpClient, private alert : AlertService, private utlity : UtilityService) { 
      this.NovaServiceApiUrl                           = config.getConfig("novaApiUrl");
      this.PaymentAccountManagementPaymentsApiPath     = config.getConfig("novaApiPaymentManagementPaymentsPath");
      this.PaymentAccountManagementPaymentTypesApiPath = config.getConfig("novaApiPaymentManagementPaymentTypesPath");
      this.PaymentAccountManagementStatusesApiPath     = config.getConfig("novaApiPaymentManagementPaymentStatusesPath");
      this.HttpRequestTimeout                          = config.getConfig("httpRequestTimeout");
      this.DocumentManagementReceiptsApiPath           = config.getConfig("novaApiDocumentManagementReceiptsPath");
      this.PaymentsManagementPaymentsPath           = config.getConfig("novaApiPaymentManagementPaymentsPath");
      
      // clear cache when session destroyed
      session.getSessionStatus().subscribe((data) => {
          if(data=='destroyed'){
              this._getPayments=null;
              this._getPayment=null;
              this._getPaymentTypes=null;
              this._getStatuses=null;
          }
      });   
  }

  private _getPayments : Payment[] = [];
  getPayments(code : String) : Observable<Payment[]> {
      return Observable.create(observer => {
          this.http.get(this.NovaServiceApiUrl + this.PaymentAccountManagementPaymentsApiPath + "/" + code, {observe: 'response'}) //(this.HttpRequestTimeout)
              //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
              //.map((response: Response) => response.json())
              .subscribe((data:any) => {
                  this._getPayments = data.body;
                  observer.next(this._getPayments);
                  observer.complete();
              }, (err) => {
                  console.log("err: " + JSON.stringify(err));
                  observer.error(err);
              });
      });
  }
  getPaymentsFromCache() : Payment[] {
      return this._getPayments;
  }

  private _getPaymentTypes : PaymentType[] = [];
  getPaymentTypes(code : String) : Observable<PaymentType[]> {
    if(!this._getPaymentTypes || !this._getPaymentTypes.length)
      return Observable.create(observer => {
          this.http.get(this.NovaServiceApiUrl + this.PaymentAccountManagementPaymentTypesApiPath + "/" + code, {observe: 'response'}) //(this.HttpRequestTimeout)
              //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
              //.map((response: Response) => response.json())
              .subscribe((data:any) => {
                  this._getPaymentTypes = data.body;
                  observer.next(this._getPaymentTypes);
                  observer.complete();
              }, (err) => {
                  console.log("err: " + JSON.stringify(err));
                  observer.error(err);
              });
      });
    else
      return Observable.create(observer => { observer.next(this._getPaymentTypes); observer.complete(); });
  }

  private _getPayment : PaymentDetail;
  private currentPaymentNumber : String = "";
  getPayment(code : String, paymentNumber : String) : Observable<PaymentDetail> {
      return Observable.create(observer => {
          this.http.get(this.NovaServiceApiUrl + this.PaymentAccountManagementPaymentsApiPath + "/" + code + "/" + paymentNumber, {observe: 'response'}) //(this.HttpRequestTimeout)
              //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
              //.map((response: Response) => response.json())
              .subscribe((data:any) => {
                  this._getPayment = data.body;
                  this.currentPaymentNumber=paymentNumber;
                  observer.next(this._getPayment);
                  observer.complete();
              }, (err) => {
                  console.log("err: " + JSON.stringify(err));
                  observer.error(err);
              });
      });
  }
  getPaymentFromCache(paymentNumber : String): PaymentDetail {
      if(paymentNumber==this.currentPaymentNumber)
          return this._getPayment;
      else
          return new PaymentDetail();
  }

  getPaymentFromURL(url) : Observable<PaymentDetail> {
    return Observable.create(observer => {
        this.http.get(url, {observe: 'response'}) //(this.HttpRequestTimeout)
            //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
            //.map((response: Response) => response.json())
            .subscribe((data:any) => {
                observer.next(data.body);
                observer.complete();
            }, (err) => {
                console.log("err: " + JSON.stringify(err));
                observer.error(err);
            });
    });
  }

  private _getStatuses : Status[] = [];
  getStatuses(code : String) : Observable<Status[]> {
      if(!this._getStatuses || !this._getStatuses.length) {
          return Observable.create(observer => {
              this.http.get(this.NovaServiceApiUrl + this.PaymentAccountManagementStatusesApiPath + "/" + code, {observe: 'response'}) //(this.HttpRequestTimeout)
                  //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
                  //.map((response: Response) => response.json())
                  .subscribe((data:any) => {
                      this._getStatuses = data.body;
                      observer.next(this._getStatuses);
                      observer.complete();
                  }, (err) => {
                      console.log("err: " + JSON.stringify(err));
                      observer.error(err);
                  });
      });
      } else
          return Observable.create(observer => { observer.next(this._getStatuses); observer.complete(); });
  }

  downloadReceipt(code : String, paymentNumber : String) {
      // invoke Nova Service to request proforma invoice
      return this.http.get(this.NovaServiceApiUrl + this.DocumentManagementReceiptsApiPath + "/" + code + "/" + paymentNumber);
  }

  makePayment(code : String, payment : Payment) {

    // invoke Nova Service to request proforma invoice
    return this.http.post(this.NovaServiceApiUrl + this.PaymentsManagementPaymentsPath + "/" + code, payment, {observe: 'response'});
  }
}