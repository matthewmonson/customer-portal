import { Component, ViewChild, OnInit, AfterViewInit, AfterContentChecked, ElementRef } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { FormArray, FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService } from '../alert.service';
import { UserService } from '../user.service';
import { User } from '../_models/user';
import { SessionService } from '../session.service';
import { Location } from '@angular/common';
import { UtilityService } from '../utility.service';
import { TranslateService } from '@ngx-translate/core';

import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Observable } from 'rxjs';
import { MatChipInputEvent, MatAutocomplete, MatAutocompleteSelectedEvent, MatTableDataSource, MatDialog } from '@angular/material';
import { startWith, map } from 'rxjs/operators';
import { ConfirmDialogComponent } from '../shared-components/confirm-dialog/confirm-dialog.component';

export class UserRole {
  move: boolean;
  name: string;
}

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit, AfterViewInit {

  myForm: FormGroup;
  formsubmitted: boolean = false;
  model: User = null;
  mode: string = "";
  organization: string;
  username: string;
  usersDatasource;
  users: User[] = [];
  cols: any[];
  //columnOptions: SelectItem[];
  defaultLabel: string;
  defaultCols: any[];

  assignedUserRoles: UserRole[] = [];
  unassignedUserRoles: UserRole[] = [];
  rebuildingroleslist: boolean = false;
  currentuser: boolean = false;

  displayedColumns: string[] = ['username','firstname', 'lastname', 'mobile', 'email', 'organization', 'orgType', 'mustChangePassword'];

  private el: HTMLElement;
  private elInput;
  private phoneNum = "";

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  roleCtrl = new FormControl();
  filteredRoles: Observable<UserRole[]>;

  navigationSubscription;

  constructor(
    private router: Router,
    private userService: UserService,
    private alertService: AlertService,
    private route: ActivatedRoute,
    private session: SessionService,
    public translation: TranslateService,
    private _fb: FormBuilder,
    private location: Location,
    el: ElementRef,
    public utility: UtilityService,
    public dialog: MatDialog) {

    route.params.subscribe(params => {
      this.organization = params['organization'];
      this.username = params['username'];
    });

    this.el = el.nativeElement;

    this.mode = this.router.url.split("/")[2]; // list/view/edit/delete/add
    var iscurrentuser = this.router.url.split("/")[3];
    this.currentuser = (iscurrentuser && iscurrentuser === 'current');

    // build this components form group
    this.myForm = this._fb.group({
      'username': ['', Validators.required],
      'firstname': ['', Validators.required],
      'lastname': ['', Validators.required],
      'mobile': ['', Validators.required],
      'email': ['', [Validators.required, Validators.email]],
      'organization': [''],
      'mustChangePassword': ['']
    });

    // subscribe to the router events - storing the subscription so
    // we can unsubscribe later. 
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd && this.router.url.indexOf('user/list')>0) {
          this.updateListData();
      }
    });    
  }

  updateListData() {
    
    if (this.organization) {

      // get list of users for organisation from cache
      this.users = this.userService.getUsersForOrganizationFromCache(this.organization);
      this.usersDatasource = new MatTableDataSource(this.users);

      // get list of users for organisation from live          
      this.session.setloading(true);
      this.userService.getUsersForOrganization(this.organization)
        .subscribe(
          data => {
            this.users = data;
            this.usersDatasource = new MatTableDataSource(this.users);
            this.session.setloading(false);
          },
          error => {
            console.log("list error: " + JSON.stringify(error));
            this.alertService.error(this.translation.instant("  list of users")); //  + " [" + JSON.stringify(error) + "]");
            this.session.setloading(false);
          });
    } else {

      // get deep list of users from cache
      this.users = this.userService.getUsersFromCache();
      this.usersDatasource = new MatTableDataSource(this.users);
      
      // get deep list of users from live
      this.session.setloading(true);
      this.userService.getUsers()
        //.catch(error => { return Observable.throw(error) })
        .subscribe(
          data => {
            this.users = data;
            this.usersDatasource = new MatTableDataSource(this.users);
            this.session.setloading(false);
          },
          error => {
            console.log("list error: " + JSON.stringify(error));
            this.alertService.error(this.translation.instant("Failed to get list of users")); //  + " [" + JSON.stringify(error) + "]");
            this.session.setloading(false);
          });
    }
  }

  ngOnInit() {

    this.filteredRoles = this.roleCtrl.valueChanges.pipe(
      startWith(null),
      map((role: string | null) => role ? this._filter(role) : this.unassignedUserRoles.slice()));

    switch (this.mode) {

      case "list":

        
      //this.updateListData();

        /*
        // set column options
        this.translation.get("Select Columns").toPromise().then( translation => { this.defaultLabel = this.translation });

        // list column options
        this.cols = [
            {field: 'firstname', header: this.translation.instant("First Name"), selected: true},
            {field: 'lastname', header: this.translation.instant("Last Name"), selected: true},
            {field: 'mobile', header: this.translation.instant("Mobile"), selected: true},
            {field: 'email', header: this.translation.instant("Email"), selected: true},
            {field: 'organization', header: this.translation.instant("Organization"), selected: true},
            {field: 'roles', header: this.translation.instant("Roles"), selected: false},
            {field: 'orgType', header: this.translation.instant("OrgType"), selected: false},
            {field: 'mustChangePassword', header: this.translation.instant("Must Change Password"), selected: false},
        ];                    
        // list default columns
        this.defaultCols = [
            {field: 'firstname', header: this.translation.instant("First Name"), selected: true},
            {field: 'lastname', header: this.translation.instant("Last Name"), selected: true},
            {field: 'mobile', header: this.translation.instant("Mobile"), selected: true},
            {field: 'email', header: this.translation.instant("Email"), selected: true},
            {field: 'organization', header: this.translation.instant("Organization"), selected: true}                        
        ]; 
        this.columnOptions = [];
        for(let i = 0; i < this.defaultCols.length; i++) {
            this.columnOptions.push({label: this.defaultCols[i].header, value: this.defaultCols[i]});
        }                    
        */

        break;

      case "add":

        this.myForm.controls['organization'].disable();
        this.model = new User();
        this.model.organization = this.organization;

        // get list of valid roles (internally will get from cache first by default)
        this.session.setloading(true);
        this.userService.getUserRoles()
          //.catch(error => { return Observable.throw(error) })
          .subscribe(
            data => {
              for (let role of data) {
                let unassignedRole = new UserRole();
                unassignedRole.move = false;
                unassignedRole.name = role['name'];
                this.unassignedUserRoles.push(unassignedRole);
              }
              this.session.setloading(false);
            },
            error => {
              console.log("roles error: " + JSON.stringify(error));
              this.alertService.error(this.translation.instant("Failed to get list of roles")); //  + " [" + JSON.stringify(error) + "]");
              console.log("roles error json: " + JSON.stringify(error));
              this.session.setloading(false);
            });

        break;

      case "edit":

        this.myForm.controls['username'].disable();
        this.myForm.controls['organization'].disable();

        if(!this.utility.userHasRoles(['ROLE_User_Write'])) {
          this.myForm.disable();
        }

        if (!this.currentuser) {

          // get user detail from cache if the same user
          this.model = this.userService.getUserFromCache(this.organization, this.username);

          // get user detail from live
          this.session.setloading(true);
          this.userService.getUser(this.organization, this.username)
            //.catch(error => { return Observable.throw(error) })
            .subscribe(
              data => {
                this.model = data;                

                // get list of valid roles 
                this.session.setloading(true);
                this.userService.getUserRoles()
                  //.catch(error => { return Observable.throw(error) })
                  .subscribe(
                    data => {
                      for (let role of data) {
                        if (this.model.roles != null) {
                          var userHasRole = false;
                          for (let userrole of this.model.roles) {
                            if (userrole == role['name']) {
                              userHasRole = true;
                            }
                          }
                          if (!userHasRole) {
                            let unassignedRole = new UserRole();
                            unassignedRole.move = false;
                            unassignedRole.name = role['name'];
                            this.unassignedUserRoles.push(unassignedRole);
                          } else {
                            let assignedRole = new UserRole();
                            assignedRole.move = false;
                            assignedRole.name = role['name'];
                            this.assignedUserRoles.push(assignedRole);
                          }
                        } else {
                          let unassignedRole = new UserRole();
                          unassignedRole.move = false;
                          unassignedRole.name = role['name'];
                          this.unassignedUserRoles.push(unassignedRole);
                        }
                      }                      
                      this.isChecked=this.model.mustChangePassword;            
                      this.myForm.controls['mustChangePassword'].setValue(this.isChecked);
                      this.session.setloading(false);
                    },
                    error => {
                      console.log("roles error: " + JSON.stringify(error));
                      this.alertService.error(this.translation.instant("Failed to get list of roles")); //  + " [" + JSON.stringify(error) + "]");
                      console.log("roles error json: " + JSON.stringify(error));
                      this.session.setloading(false);
                      return;
                    });
              },
              error => {
                console.log("edit error: " + JSON.stringify(error));
                this.alertService.error(this.translation.instant("Failed to get user detail")); //  + " [" + JSON.stringify(error) + "]");
                console.log("edit error json: " + JSON.stringify(error));
                this.session.setloading(false);
                return;
              });
        } else {

          // get current user detail from cache if the same user
          this.model = this.userService.getCurrentUserFromCache();

          // get user detail from live
          this.session.setloading(true);
          this.userService.getCurrentUser()
            //.catch(error => { return Observable.throw(error) })
            .subscribe(
              data => {
                this.model = data;
                //this.session.setloading(false);

                // get list of valid roles 
                this.session.setloading(true);
                this.userService.getUserRoles()
                  //.catch(error => { return Observable.throw(error) })
                  .subscribe(
                    data => {
                      for (let role of data) {
                        if (this.model.roles != null) {
                          var userHasRole = false;
                          for (let userrole of this.model.roles) {
                            if (userrole == role['name']) {
                              userHasRole = true;
                            }
                          }
                          if (!userHasRole) {
                            let unassignedRole = new UserRole();
                            unassignedRole.move = false;
                            unassignedRole.name = role['name'];
                            this.unassignedUserRoles.push(unassignedRole);
                          } else {
                            let assignedRole = new UserRole();
                            assignedRole.move = false;
                            assignedRole.name = role['name'];
                            this.assignedUserRoles.push(assignedRole);
                          }
                        } else {
                          let unassignedRole = new UserRole();
                          unassignedRole.move = false;
                          unassignedRole.name = role['name'];
                          this.unassignedUserRoles.push(unassignedRole);
                        }
                      }
                      this.session.setloading(false);
                    },
                    error => {
                      console.log("roles error: " + JSON.stringify(error));
                      this.alertService.error(this.translation.instant("Failed to get list of roles")); //  + " [" + JSON.stringify(error) + "]");
                      console.log("roles error json: " + JSON.stringify(error));
                      this.session.setloading(false);
                      return;
                    });
              },
              error => {
                console.log("edit error: " + JSON.stringify(error));
                this.alertService.error(this.translation.instant("Failed to get user detail")); //  + " [" + JSON.stringify(error) + "]");
                console.log("edit error json: " + JSON.stringify(error));
                this.session.setloading(false);
                return;
              });
        }

        break;

      case "view":

        this.myForm.disable();

        if (!this.currentuser) {

          // get user detail from cache if the same user
          this.model = this.userService.getUserFromCache(this.organization, this.username);

          // get user detail from live
          this.session.setloading(true);
          this.userService.getUser(this.organization, this.username)
            //.catch(error => { return Observable.throw(error) })
            .subscribe(
              data => {
                this.model = data;
                this.session.setloading(false);

                // get list of valid roles 
                this.session.setloading(true);
                this.userService.getUserRoles()
                  //.catch(error => { return Observable.throw(error) })
                  .subscribe(
                    data => {
                      for (let role of data) {
                        if (this.model.roles != null) {
                          var userHasRole = false;
                          for (let userrole of this.model.roles) {
                            if (userrole == role['name']) {
                              userHasRole = true;
                            }
                          }
                          if (!userHasRole) {
                            let unassignedRole = new UserRole();
                            unassignedRole.move = false;
                            unassignedRole.name = role['name'];
                            this.unassignedUserRoles.push(unassignedRole);
                          } else {
                            let assignedRole = new UserRole();
                            assignedRole.move = false;
                            assignedRole.name = role['name'];
                            this.assignedUserRoles.push(assignedRole);
                          }
                        } else {
                          let unassignedRole = new UserRole();
                          unassignedRole.move = false;
                          unassignedRole.name = role['name'];
                          this.unassignedUserRoles.push(unassignedRole);
                        }
                      }
                      this.session.setloading(false);
                    },
                    error => {
                      console.log("roles error: " + JSON.stringify(error));
                      this.alertService.error(this.translation.instant("Failed to get list of roles")); //  + " [" + JSON.stringify(error) + "]");
                      console.log("roles error json: " + JSON.stringify(error));
                      this.session.setloading(false);
                      return;
                    });
              },
              error => {
                console.log("edit error: " + JSON.stringify(error));
                this.alertService.error(this.translation.instant("Failed to get user detail")); //  + " [" + JSON.stringify(error) + "]");
                console.log("edit error json: " + JSON.stringify(error));
                this.session.setloading(false);
                return;
              });
        } else {

          // get current user detail from cache if the same user
          this.model = this.userService.getCurrentUserFromCache();

          // get user detail from live
          this.session.setloading(true);
          this.userService.getCurrentUser()
            //.catch(error => { return Observable.throw(error) })
            .subscribe(
              data => {
                this.model = data;
                this.session.setloading(false);

                // get list of valid roles 
                this.session.setloading(true);
                this.userService.getUserRoles()
                  //.catch(error => { return Observable.throw(error) })
                  .subscribe(
                    data => {
                      for (let role of data) {
                        if (this.model.roles != null) {
                          var userHasRole = false;
                          for (let userrole of this.model.roles) {
                            if (userrole == role['name']) {
                              userHasRole = true;
                            }
                          }
                          if (!userHasRole) {
                            let unassignedRole = new UserRole();
                            unassignedRole.move = false;
                            unassignedRole.name = role['name'];
                            this.unassignedUserRoles.push(unassignedRole);
                          } else {
                            let assignedRole = new UserRole();
                            assignedRole.move = false;
                            assignedRole.name = role['name'];
                            this.assignedUserRoles.push(assignedRole);
                          }
                        } else {
                          let unassignedRole = new UserRole();
                          unassignedRole.move = false;
                          unassignedRole.name = role['name'];
                          this.unassignedUserRoles.push(unassignedRole);
                        }
                      }
                      this.session.setloading(false);
                    },
                    error => {
                      console.log("roles error: " + JSON.stringify(error));
                      this.alertService.error(this.translation.instant("Failed to get list of roles")); //  + " [" + JSON.stringify(error) + "]");
                      console.log("roles error json: " + JSON.stringify(error));
                      this.session.setloading(false);
                      return;
                    });
              },
              error => {
                console.log("edit error: " + JSON.stringify(error));
                this.alertService.error(this.translation.instant("Failed to get user detail")); //  + " [" + JSON.stringify(error) + "]");
                console.log("edit error json: " + JSON.stringify(error));
                this.session.setloading(false);
                return;
              });
        }

        break;

      case "delete":

        this.myForm.disable();

        // get user detail from cache if the same user
        this.model = this.userService.getUserFromCache(this.organization, this.username);

        // get user detail from live
        this.session.setloading(true);
        this.userService.getUser(this.organization, this.username)
          //.catch(error => { return Observable.throw(error) })
          .subscribe(
            data => {
              this.model = data;
              this.session.setloading(false);

              // get list of valid roles 
              this.session.setloading(true);
              this.userService.getUserRoles()
                //.catch(error => { return Observable.throw(error) })
                .subscribe(
                  data => {
                    for (let role of data) {
                      if (this.model.roles != null) {
                        var userHasRole = false;
                        for (let userrole of this.model.roles) {
                          if (userrole == role['name']) {
                            userHasRole = true;
                          }
                        }
                        if (!userHasRole) {
                          let unassignedRole = new UserRole();
                          unassignedRole.move = false;
                          unassignedRole.name = role['name'];
                          this.unassignedUserRoles.push(unassignedRole);
                        } else {
                          let assignedRole = new UserRole();
                          assignedRole.move = false;
                          assignedRole.name = role['name'];
                          this.assignedUserRoles.push(assignedRole);
                        }
                      } else {
                        let unassignedRole = new UserRole();
                        unassignedRole.move = false;
                        unassignedRole.name = role['name'];
                        this.unassignedUserRoles.push(unassignedRole);
                      }
                    }
                    this.session.setloading(false);
                  },
                  error => {
                    console.log("roles error: " + JSON.stringify(error));
                    this.alertService.error(this.translation.instant("Failed to get list of roles")); //  + " [" + JSON.stringify(error) + "]");
                    console.log("roles error json: " + JSON.stringify(error));
                    this.session.setloading(false);
                    return;
                  });
            },
            error => {
              console.log("delete error: " + JSON.stringify(error));
              this.alertService.error(this.translation.instant("Failed to get user detail")); //  + " [" + JSON.stringify(error) + "]");
              console.log("delete error json: " + JSON.stringify(error));
              this.session.setloading(false);
              return;
            });

        break;
    }
  }

  clickCancel() {
    this.router.navigate(['/user/list']);
  }

  clickDelete() {
    //confirm
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      data: { title: 'Confirm Delete', content: 'Delete User ' + this.model.username + '?' }
  });

  dialogRef.afterClosed().subscribe(result => {
      if (result === true) {

      this.session.setloading(true);
      // delete user
      this.userService.deleteUser(this.model.organization, this.model.username)
        .subscribe(
          data => {
            this.session.setloading(false);
            this.alertService.successKeepAfterNavigation(this.translation.instant("User deleted"));
            this.router.navigate(['/user/list']);
          },
          error => {
            this.session.setloading(false);
            console.log(error);
            this.alertService.error(this.translation.instant("Failed to delete User")); //  + " [" + JSON.stringify(error) + "]");
          });
      
        }});
  }

  clickView(event) {
    this.router.navigate(['/user/view', event.data['organization'], event.data['username']]);
  }

  clickEdit(organization, username) {
    this.router.navigate(['/user/edit', organization, username]);
  }

  clickAdd() {
    this.router.navigate(['/user/add', this.organization ? this.organization : this.session.getSessionUser().organization]);
  }

  toggleAssignedForMove(index: number) {
    if (this.assignedUserRoles[index].move == true)
      this.assignedUserRoles[index].move = false;
    else
      this.assignedUserRoles[index].move = true;
  }

  moveSelectedToUnassigned() {
    var newAssignedRoles = new Array<UserRole>();
    for (var _i = 0; _i < this.assignedUserRoles.length; _i++) {
      let role = this.assignedUserRoles[_i];
      if (role.move == true) {
        role.move = false;
        this.unassignedUserRoles.push(role);
      } else
        newAssignedRoles.push(role);
    }
    this.assignedUserRoles = newAssignedRoles;
  }

  toggleUnassignedForMove(index: number) {
    if (this.unassignedUserRoles[index].move == true)
      this.unassignedUserRoles[index].move = false;
    else
      this.unassignedUserRoles[index].move = true;
  }

  moveSelectedToAssigned() {
    var newUnassignedRoles = new Array<UserRole>();
    for (var _i = 0; _i < this.unassignedUserRoles.length; _i++) {
      let role = this.unassignedUserRoles[_i];
      if (role.move == true) {
        role.move = false;
        this.assignedUserRoles.push(role);
      } else
        newUnassignedRoles.push(role);
    }
    this.unassignedUserRoles = newUnassignedRoles;
  }

  submitUserCrudForm() {

    this.alertService.clearAlert();

    // if we aren't deleting then update model and validate....
    if (this.mode != 'delete') {

      // update the assigned roles on the user model
      this.model['roles'] = [];
      var i = 0;
      for (let role of this.assignedUserRoles) {
        this.model['roles'][i] = role.name;
        i++;
      }

      // validate the form               
      this.formsubmitted = true;
      this.myForm.updateValueAndValidity();
      if (!this.myForm.valid) {
        this.alertService.error(this.translation.instant("The form is not valid, please correct the values and try again"));
        return;
      }
      this.formsubmitted = false;
    }

    // if all good then proceed to posting via the service layer        

    switch (this.mode) {
      case "add":
        this.session.setloading(true);
        // create new user
        this.userService.createUser(this.model)
          .subscribe(
            data => {
              this.session.setloading(false);
              this.alertService.successKeepAfterNavigation(this.translation.instant("User created, an email will be sent to") + " " + this.model.email);
              this.router.navigate(['/user/list', this.organization]);
            },
            error => {
              this.session.setloading(false);
              console.log(error);
              this.alertService.error(this.translation.instant("Failed to add User")); //  + " [" + JSON.stringify(error) + "]");
            });
        break;

      case "edit":
        this.session.setloading(true);
        // update user
        this.userService.updateUser(this.model)
          .subscribe(
            data => {
              this.session.setloading(false);
              this.alertService.success(this.translation.instant("User updated"));
            },
            error => {
              this.session.setloading(false);
              console.log(error);
              this.alertService.error(this.translation.instant("Failed to update User")); //  + " [" + JSON.stringify(error) + "]");
            });
        break;
    }
  }

  ngAfterViewInit() {
    /*
      this.elInput = jQuery(this.el).find('.ng2intltelinput');
      this.elInput.intlTelInput();
*/
    if (this.utility.getUserAddress() != null && this.utility.getUserAddress().countryCode != null) {
      //this.setCountry(this.utility.getUserAddress().countryCode);
    } /*
      if(this.model && this.model.mobile>"")
          this.setNumber(this.model.mobile);*/
  }

  private _getNumber() {
    /*
      return this.elInput.intlTelInput('getNumber').replace(/[^\d]/, '');*/
  }
  /*
  getNumber() {
      var _phoneNum = this._getNumber();
      var _isValid = this.validator();
      if (_isValid) {
          this.phoneNum = _phoneNum;      
          this.myForm.controls['mobile'].setErrors(null);
          this.myForm.updateValueAndValidity();
          this.model.mobile = "+" + _phoneNum;
          this.setNumber(this.model.mobile);
      }
      else {        
          this.myForm.controls['mobile'].setErrors({"Invalid number": true});
          this.myForm.updateValueAndValidity();
      }
  }
  setNumber(sVal) {
      this.elInput.intlTelInput('setNumber', sVal);
  }
  setCountry(sVal) {
      this.elInput.intlTelInput('setCountry', sVal);
  }
  validator() {
      var _phoneNum = this._getNumber();
      if (_phoneNum.length > 0) {
          return this.elInput.intlTelInput("isValidNumber");
      } else {
          return false;
      }
  }
  */

  @ViewChild('roleInput', { static: false }) roleInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', { static: false }) matAutocomplete: MatAutocomplete;

  remove(role: UserRole): void {
    const index = this.assignedUserRoles.indexOf(role);

    if (index >= 0) {
      this.assignedUserRoles.splice(index, 1);
      this.unassignedUserRoles.push(role);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {    
    this.assignedUserRoles.push(event.option.value);    
    //this.unassignedUserRoles.splice(this.unassignedUserRoles.indexOf(event.option.value),1);    alert(JSON.stringify(this.unassignedUserRoles));
    let i=0;
            this.unassignedUserRoles.forEach(option => {
              if(option.name===event.option.value.name) {                
                this.unassignedUserRoles.splice(i,1);
              }
              i++;
            });
    this.roleInput.nativeElement.value = '';
    this.roleCtrl.setValue('');
  }

  isString(x: any): x is string {
    return typeof x === "string";
  }

  private _filter(value: String): UserRole[] {    
    if(this.isString(value)) {
      const filterValue = value.toLowerCase();    
      return this.unassignedUserRoles.filter(role => role.name.toLowerCase().indexOf(filterValue) >= 0);
    }
  }

  public isChecked : boolean = false;
  toggleChanged($event) {
    this.isChecked=!this.isChecked;
    this.myForm.controls['mustChangePassword'].setValue(this.isChecked ? true : false);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.usersDatasource.filter = filterValue.trim().toLowerCase();
  }
}
