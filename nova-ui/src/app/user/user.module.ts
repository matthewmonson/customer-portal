import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserPageRoutingModule } from './user-routing.module';

import { UserPage } from './user.page';
import { MaterialModule } from '../material/material.module';
import { TranslateModule } from '@ngx-translate/core';

import { NgxMatIntlTelInputModule } from 'ngx-mat-intl-tel-input';
import { SharedComponentsModule } from '../shared-components/shared-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaterialModule,
    UserPageRoutingModule,
    TranslateModule,
    ReactiveFormsModule,
    NgxMatIntlTelInputModule,
    SharedComponentsModule
  ],
  declarations: [UserPage]
})
export class UserPageModule {}
