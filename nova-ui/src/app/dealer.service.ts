import { Injectable } from '@angular/core';
import { Dealer, DealerAccountDetail } from './_models/dealer';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertService } from './alert.service';
import { UtilityService } from './utility.service';
import { ConfigService } from './config.service';
import { SessionService } from './session.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DealerService {

  NovaServiceApiUrl : string;
  DealerAccountManagementCurrentDealerApiPath : string;
  DealerAccountManagementDealersApiPath : string;
  DealerAccountManagementRolesApiPath : string;
  DealerAccountManagementAccountsApiPath : string;
  HttpRequestTimeout : number;

  constructor(private session: SessionService, private config: ConfigService, private http: HttpClient, private alert : AlertService, private utlity : UtilityService) {
      this.NovaServiceApiUrl                           = config.getConfig("novaApiUrl");
      this.DealerAccountManagementDealersApiPath       = config.getConfig("novaApiDealerManagementDealersPath");
      this.HttpRequestTimeout                          = config.getConfig("httpRequestTimeout");
      this.DealerAccountManagementCurrentDealerApiPath = config.getConfig("novaApiDealerManagementCurrentDealerPath");
      this.DealerAccountManagementAccountsApiPath      = config.getConfig("novaApiDealerManagementAccountsPath");

      // clear cache when session destroyed
      session.getSessionStatus().subscribe((data) => {
          if(data=='destroyed'){
              this._getAllDealers=null;
              this._getAllDealersForSupplier=null;
              this._getDealer=null;
              this._getCurrentDealer=null;
              this._getDealerAccountDetail=null;
              this.currentDealerCode=null;
              this.currentDealerSupplier=null;
              this.currentSupplierDealerCode=null;
          }
      });   
  }

  private _getAllDealers : Dealer[] = [];
  getAllDealers() : Observable<Dealer[]> {
      return Observable.create(observer => {
          this.http.get(this.NovaServiceApiUrl + this.DealerAccountManagementDealersApiPath, {observe: 'response'})//.timeout(this.HttpRequestTimeout)
              //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
              //.map((response: Response) => response.json())
              .subscribe((data:any) => {
                  this._getAllDealers = data.body;
                  observer.next(this._getAllDealers);
                  observer.complete();
              }, (err) => {
                  console.log("err: " + JSON.stringify(err));
                  observer.error(err);
              });
      });
  }
  getAllDealersFromCache() : Dealer[] {
      return this._getAllDealers;
  }

  private _getAllDealersForSupplier : Dealer[] = [];
  private currentSupplierDealerCode : String = "";
  getAllDealersForSupplier(parent : String) : Observable<Dealer[]> {
      return Observable.create(observer => {
          this.http.get(this.NovaServiceApiUrl + this.DealerAccountManagementDealersApiPath + "?where=%7B%22supplier%22:%22" + parent + "%22%7D", {observe: 'response'})//.timeout(this.HttpRequestTimeout)
              //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
              //.map((response: Response) => response.json())
              .subscribe((data:any) => {
                  this._getAllDealersForSupplier = data.body;
                  this.currentSupplierDealerCode = parent;
                  observer.next(this._getAllDealersForSupplier);
                  observer.complete();
              }, (err) => {
                  console.log("err: " + JSON.stringify(err));
                  observer.error(err);
              });
      });
  }
  getAllDealersForSupplierFromCache(parent : String) : Dealer[] {
      if(parent!=this.currentSupplierDealerCode)
          return [] as Dealer[];
      else
          return this._getAllDealersForSupplier;
  }

  private _getDealer : Dealer;
  private currentDealerSupplier : String = "";
  private currentDealerCode : String = "";
  getDealer(supplier : String, code : String) : Observable<Dealer> {
      return Observable.create(observer => {
          this.http.get(this.NovaServiceApiUrl + this.DealerAccountManagementDealersApiPath + "/" + supplier + "/" + code, {observe: 'response'})//.timeout(this.HttpRequestTimeout)
              //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
              //.map((response: Response) => response.json())
              .subscribe((data:any) => {
                  this._getDealer = data.body;
                  this.currentDealerCode=code;
                  this.currentDealerSupplier=supplier;
                  observer.next(this._getDealer);
                  observer.complete();
              }, (err) => {
                  console.log("err: " + JSON.stringify(err));
                  observer.error(err);
              });
      });
  }
  getDealerFromCache(supplier : String, code : String): Dealer {
      if(code==this.currentDealerCode && supplier==this.currentDealerSupplier)
          return this._getDealer;
      else
          return new Dealer();
  }

  private _getCurrentDealer : Dealer;    
  private currentDealerUsername : String = "";
  getCurrentDealer(username : String) : Observable<Dealer> {
      if(!this._getCurrentDealer)
          return Observable.create(observer => {
              this.http.get(this.NovaServiceApiUrl + this.DealerAccountManagementCurrentDealerApiPath, {observe: 'response'}) //.timeout(this.HttpRequestTimeout)
                  //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
                  //.map((response: Response) => response.json())
                  .subscribe((data:any) => {
                      this._getCurrentDealer = data.body;
                      this.currentDealerUsername=username;                    
                      observer.next(this._getCurrentDealer);
                      observer.complete();
                  }, (err) => {
                      console.log("err: " + JSON.stringify(err));
                      observer.error(err);
                  });
          });
      else {
          return Observable.create(observer => { observer.next(this._getCurrentDealer); observer.complete(); });
      }
  }

  private _getDealerAccountDetail : DealerAccountDetail;    
  getDealerAccountDetail(customerAccountNumber : String) : Observable<DealerAccountDetail> {
      return Observable.create(observer => {
          this.http.get(this.NovaServiceApiUrl + this.DealerAccountManagementAccountsApiPath + "/" + customerAccountNumber, {observe: 'response'}) //.timeout(this.HttpRequestTimeout)
              //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
              //.map((response: Response) => response.json())
              .subscribe((data:any) => {
                  this._getDealerAccountDetail = data.body;                    
                  observer.next(this._getDealerAccountDetail);
                  observer.complete();
              }, (err) => {
                  console.log("err: " + JSON.stringify(err));
                  observer.error(err);
              });
      });
  }

  createDealer(Dealer: Dealer) {
      // invoke Nova Service to create a new Dealer
      return this.http.post(this.NovaServiceApiUrl + this.DealerAccountManagementDealersApiPath + "/" + Dealer.supplier, Dealer, {observe: 'response'}); //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) });
  }

  updateDealer(Dealer: Dealer) {
      // invoke Nova Service to update an existing Dealer
      return this.http.put(this.NovaServiceApiUrl + this.DealerAccountManagementDealersApiPath + "/" + Dealer.supplier + "/" + Dealer.code, Dealer, {observe: 'response'}); //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) });
  }

  deleteDealer(supplier : String, code : String) {
      // invoke Nova Service to delete a Dealer
      return this.http.delete(this.NovaServiceApiUrl + this.DealerAccountManagementDealersApiPath + "/" + supplier + "/" + code, {observe: 'response'}); //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) });
  }
}

