import { Injectable } from '@angular/core';
import { SessionService } from './session.service';
import { ConfigService } from './config.service';
import { HttpClient } from '@angular/common/http';
import { AlertService } from './alert.service';
import { UtilityService } from './utility.service';
import { Order } from './_models/order';
import { Observable } from 'rxjs';
import { OrderDetail } from './_models/orderdetail';
import { Product } from './_models/product';
import { Warehouse } from './_models/warehouse';
import { Status } from './_models/status';
import { Payment } from './_models/payment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  NovaServiceApiUrl : string;
  OrderAccountManagementCurrentOrderApiPath : string;
  OrderAccountManagementOrdersApiPath : string;
  OrderAccountManagementRolesApiPath : string;
  OrderAccountManagementProductsApiPath : string;
  OrderAccountManagementWarehousesApiPath : string;
  OrderAccountManagementStatusesApiPath : string;
  OrderAccountManagementCancellationApiPath : string;
  HttpRequestTimeout : number;
  
  constructor(private session: SessionService, private config: ConfigService, private http: HttpClient, private alert : AlertService, private utlity : UtilityService) { 
      this.NovaServiceApiUrl                         = config.getConfig("novaApiUrl");
      this.OrderAccountManagementOrdersApiPath       = config.getConfig("novaApiOrderManagementOrdersPath");
      this.OrderAccountManagementProductsApiPath     = config.getConfig("novaApiOrderManagementProductsPath");
      this.OrderAccountManagementWarehousesApiPath   = config.getConfig("novaApiOrderManagementWarehousesPath");
      this.OrderAccountManagementStatusesApiPath     = config.getConfig("novaApiOrderManagementStatusesPath");
      this.OrderAccountManagementCancellationApiPath = config.getConfig("novaApiOrderManagementCancellationPath");
      this.HttpRequestTimeout                        = config.getConfig("httpRequestTimeout");

      // clear cache when session destroyed
      session.getSessionStatus().subscribe((data:any) => {
          if(data=='destroyed'){
              this._getOrders=null;
              this._getOrder=null;
              this._getProducts=null;
              this._getWarehouses=null;
              this._getStatuses=null;
          }
      });   
  }

  private _getOrders : Order[] = [];
  getOrders(code : String) : Observable<Order[]> {
      return Observable.create(observer => {
          this.http.get(this.NovaServiceApiUrl + this.OrderAccountManagementOrdersApiPath + "/" + code, {observe: 'response'})
              ////.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
              ////.map((response: Response) => response.json())
              .subscribe((data:any) => {
                  this._getOrders = data.body;
                  observer.next(this._getOrders);
                  observer.complete();
              }, (err) => {
                  console.log("err: " + JSON.stringify(err));
                  observer.error(err);
              });
      });
  }
  getOrdersFromCache() : Order[] {
      return this._getOrders;
  }

  private _getOrder : OrderDetail;
  private currentOrderNumber : String = "";
  getOrder(code : String, orderNumber : String) : Observable<OrderDetail> {
      return Observable.create(observer => {
          this.http.get(this.NovaServiceApiUrl + this.OrderAccountManagementOrdersApiPath + "/" + code + "/" + orderNumber, {observe: 'response'}) //.timeout(this.HttpRequestTimeout)
              //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
              //.map((response: Response) => response.json())
              .subscribe((data:any) => {
                  this._getOrder = data.body;
                  this.currentOrderNumber=orderNumber;
                  observer.next(this._getOrder);
                  observer.complete();
              }, (err) => {
                  console.log("err: " + JSON.stringify(err));
                  observer.error(err);
              });
      });
  }
  getOrderFromCache(orderNumber : String): OrderDetail {
      if(orderNumber==this.currentOrderNumber)
          return this._getOrder;
      else
          return new OrderDetail();
  }

  private _getProducts : Product[];
  private currentProductWarehouse: String = "";
  getProducts(code : String, productWarehouse: String) : Observable<Product[]> {
      return Observable.create(observer => {
          this.http.get(this.NovaServiceApiUrl + this.OrderAccountManagementProductsApiPath + "/" + code + "/" + productWarehouse, {observe: 'response'}) //.timeout(this.HttpRequestTimeout)
              //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
              //.map((response: Response) => response.json())
              .subscribe((data:any) => {
                  this._getProducts = data.body;
                  this.currentProductWarehouse=productWarehouse;
                  observer.next(this._getProducts);
                  observer.complete();
              }, (err) => {
                  console.log("err: " + JSON.stringify(err));
                  observer.error(err);
              });
      });
  }
  getProductsFromCache(currentProductWarehouse : String): Product[] {
      if(currentProductWarehouse==this.currentProductWarehouse)
          return this._getProducts;
      else
          return [] as Product[];
  }

  private _getWarehouses : Warehouse[] = [];
  getWarehouses(code : String) : Observable<Warehouse[]> {
      if(!this._getWarehouses || !this._getWarehouses.length) {
          return Observable.create(observer => {
              this.http.get(this.NovaServiceApiUrl + this.OrderAccountManagementWarehousesApiPath + "/" + code, {observe: 'response'}) //.timeout(this.HttpRequestTimeout)
                  //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
                  //.map((response: Response) => response.json())
                  .subscribe((data:any) => {
                      this._getWarehouses = data.body;
                      observer.next(this._getWarehouses);
                      observer.complete();
                  }, (err) => {
                      console.log("err: " + JSON.stringify(err));
                      observer.error(err);
                  });
      });
      } else
          return Observable.create(observer => { observer.next(this._getWarehouses); observer.complete(); });
  }  

  private _getStatuses : Status[] = [];
  getStatuses(code : String) : Observable<Status[]> {
      if(!this._getStatuses || !this._getStatuses.length) {
          return Observable.create(observer => {
              this.http.get(this.NovaServiceApiUrl + this.OrderAccountManagementStatusesApiPath + "/" + code, {observe: 'response'}) //.timeout(this.HttpRequestTimeout)
                  //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
                  //.map((response: Response) => response.json())
                  .subscribe((data:any) => {
                      this._getStatuses = data.body;
                      observer.next(this._getStatuses);
                      observer.complete();
                  }, (err) => {
                      console.log("err: " + JSON.stringify(err));
                      observer.error(err);
                  });
      });
      } else
          return Observable.create(observer => { observer.next(this._getStatuses); observer.complete(); });
  }

  getPayments(code : String, order : String) : Observable<Payment[]> {
          return Observable.create(observer => {
              this.http.get(this.NovaServiceApiUrl + this.OrderAccountManagementOrdersApiPath + "/" + code + "/" + order + "/payments", {observe: 'response'}) //.timeout(this.HttpRequestTimeout)
                  //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
                  //.map((response: Response) => response.json())
                  .subscribe((data:any) => {
                      observer.next(data.body);
                      observer.complete();
                  }, (err) => {
                      console.log("err: " + JSON.stringify(err));
                      observer.error(err);
                  });
      });
  }

  createOrder(code : String, Order: Order) {
      // invoke Nova Service to create a new Order
      return this.http.post(this.NovaServiceApiUrl + this.OrderAccountManagementOrdersApiPath + "/" + code, Order)//.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) });
  }

  updateOrder(Order: Order) {
      // invoke Nova Service to update an existing Order
      return this.http.put(this.NovaServiceApiUrl + this.OrderAccountManagementOrdersApiPath, Order)//.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) });
  }

  deleteOrder(orderNumber : String) {
      // invoke Nova Service to delete a Order
      return this.http.delete(this.NovaServiceApiUrl + this.OrderAccountManagementOrdersApiPath + "/" + orderNumber)//.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) });
  }

  cancelOrder(code : String, orderNumber : String) {
      // invoke Nova Service to cancel an Order
      return this.http.post(this.NovaServiceApiUrl + this.OrderAccountManagementCancellationApiPath + "/" + code + "/" + orderNumber, {})//.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) });
  }
}

