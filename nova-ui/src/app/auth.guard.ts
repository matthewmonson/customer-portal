import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SessionService } from './session.service';
import { AlertService } from './alert.service';

@Injectable()
export class AuthGuard implements CanActivate {

    private routeConfig;

    constructor(private session: SessionService, private router: Router, private alert : AlertService) {                 
        this.routeConfig = router.config;            
    }
    
    getRouteConfig() {
        return this.routeConfig;
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        
        let passwordResetToken = route.queryParams['passwordResetToken'];

        // check we have a session token
        if (this.session.getSessionToken()!=null) {

            let guardRoles = route.data['allowedRoles'];            
            if (guardRoles) {

                // check we have the correct role for the route being activated
                for (let userrole of this.session.getSessionUser().roles) {
                    for(let routerole of guardRoles) { //TODO: Extremely inefficient..
                        if(userrole===routerole) {
                            return true;
                        }
                    }
                }

                //this.alert.error(this.translation.translate('Permission denied'));

                return false;
                
            } else {

                return true;
            }
        } else {            
            
            // if we are coming from a password reset confirmation link then redirect to the forgotpassword component
            if(passwordResetToken && passwordResetToken>"")
                this.router.navigate(['/forgotpassword', passwordResetToken ]);
            else
                // else not logged in so redirect to login page with the return url
                this.router.navigate(['/login'], { queryParams: { returnurl: state.url }});

            return false;
        }
    }

    userAuthenticated() {
        return this.session.getSessionToken()!=null;
    }
}
