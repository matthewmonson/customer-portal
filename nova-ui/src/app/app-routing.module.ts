import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';

const routes: Routes = [

  // valid paths
  { path: '', redirectTo: 'home', pathMatch: 'full'                                                                                    },
  { path: 'home',           loadChildren: () => import('./home/home.module').then(m => m.HomePageModule),   canActivate: [AuthGuard]   },
  { path: 'login',          loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)                           },
  { path: 'login/logout',   loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)                           },
  
  { path: 'changepassword', loadChildren: () => import('./changepassword/changepassword.module').then( m => m.ChangepasswordPageModule), canActivate: [AuthGuard]},

  // User CRUD routes
  { path: 'user/list',                           loadChildren: () => import('./user/user.module').then(m => m.UserPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_User_Read']  } },
  { path: 'user/list/:organization',             loadChildren: () => import('./user/user.module').then(m => m.UserPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_User_Read']  } },
  { path: 'user/edit/:organization/:username',   loadChildren: () => import('./user/user.module').then(m => m.UserPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_User_Read'] } },
  { path: 'user/delete/:organization/:username', loadChildren: () => import('./user/user.module').then(m => m.UserPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_User_Write'] } },
  { path: 'user/add/:organization',              loadChildren: () => import('./user/user.module').then(m => m.UserPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_User_Write'] } },

  // Opco CRUD routes
  { path: 'opco/list',                           loadChildren: () => import('./opco/opco.module').then( m => m.OpcoPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Opco_Read'] } },
  { path: 'opco/list/:parentopco',               loadChildren: () => import('./opco/opco.module').then( m => m.OpcoPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Opco_Read'] } },  
  { path: 'opco/edit/:parentopco/:code',         loadChildren: () => import('./opco/opco.module').then( m => m.OpcoPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Opco_Read'] } },
  { path: 'opco/delete/:parentopco/:code',       loadChildren: () => import('./opco/opco.module').then( m => m.OpcoPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Opco_Write'] } },
  { path: 'opco/add/:parentopco',                loadChildren: () => import('./opco/opco.module').then( m => m.OpcoPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Opco_Write'] } },

  // Dealer CRUD routes
  { path: 'dealer/list',                         loadChildren: () => import('./dealer/dealer.module').then( m => m.DealerPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Dealer_Read'] } },
  { path: 'dealer/list/:supplier',               loadChildren: () => import('./dealer/dealer.module').then( m => m.DealerPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Dealer_Read'] } },  
  { path: 'dealer/edit/:supplier/:code',         loadChildren: () => import('./dealer/dealer.module').then( m => m.DealerPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Dealer_Read'] } },
  { path: 'dealer/delete/:supplier/:code',       loadChildren: () => import('./dealer/dealer.module').then( m => m.DealerPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Dealer_Write'] } },
  { path: 'dealer/add/:supplier',                loadChildren: () => import('./dealer/dealer.module').then( m => m.DealerPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Dealer_Write'] } },

  // Quote CRUD routes
  { path: 'quote/list',                          loadChildren: () => import('./quote/quote.module').then( m => m.QuotePageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Quotation_Read'] } },
  { path: 'quote/edit/:quotationnumber',         loadChildren: () => import('./quote/quote.module').then( m => m.QuotePageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Quotation_Read'] } },
  { path: 'quote/delete/:quotationnumber',       loadChildren: () => import('./quote/quote.module').then( m => m.QuotePageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Quotation_Write'] } },
  { path: 'quote/add',                           loadChildren: () => import('./quote/quote.module').then( m => m.QuotePageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Quotation_Write'] } },

  // Order CRUD routes
  { path: 'order/list',                          loadChildren: () => import('./order/order.module').then( m => m.OrderPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Order_Read'] } },
  { path: 'order/view/:ordernumber',             loadChildren: () => import('./order/order.module').then( m => m.OrderPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Order_Read'] } },
  { path: 'order/delete/:ordernumber',           loadChildren: () => import('./order/order.module').then( m => m.OrderPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Order_Write'] } },
  { path: 'order/add',                           loadChildren: () => import('./order/order.module').then( m => m.OrderPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Order_Write'] } },

  // Payment routes
  { path: 'payment/list',                        loadChildren: () => import('./payment/payment.module').then( m => m.PaymentPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Receipt_Read'] } },
  { path: 'payment/view/:paymentnumber',         loadChildren: () => import('./payment/payment.module').then( m => m.PaymentPageModule), canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_Receipt_Read'] } },

  { path: 'forgotpassword',                      loadChildren: () => import('./forgotpassword/forgotpassword.module').then( m => m.ForgotpasswordPageModule) },
  { path: 'forgotpassword/:passwordResetToken',  loadChildren: () => import('./forgotpassword/forgotpassword.module').then( m => m.ForgotpasswordPageModule) },

  // otherwise redirect back to home
  { path: '**', redirectTo: '' },   
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, onSameUrlNavigation: 'reload' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
