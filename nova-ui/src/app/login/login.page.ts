import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../_models/user';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { SessionService } from '../session.service';
import { AlertService } from '../alert.service';
import { TranslateService } from '@ngx-translate/core';
import { Dealer } from '../_models/dealer';
import { map, filter } from 'rxjs/operators';
import { DealerService } from '../dealer.service';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  model: any = {};
  returnUrl: string = null;
  currentDealer: Dealer;
  clearsession: boolean = false;

  constructor(
    private session: SessionService,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private config: ConfigService,
    private dealerService: DealerService,
    private http: HttpClient,
    public translation: TranslateService,
    private platform: Platform) {
      this.platform.ready().then(() => {
        this.initializeApp();
      });
  }

  ngOnInit() {
    
  }

  initializeApp() {    
      this.session.setloading(true);

      // get the return url
      this.route.queryParams
        //.filter(params => params.returnurl)
        .subscribe(params => {
          this.returnUrl = params.returnurl;
          console.log("returning back to: " + this.returnUrl);
          if (this.returnUrl == "/") this.returnUrl = null;
          this.clearsession = this.router.url.split("/")[2] && this.router.url.split("/")[2] == 'logout';

          this.session.setMenuActive(false);

          // reset login status
          this.logout();
        });
  }

  onKeydown(event) {
    if (event.key === "Enter") {
      this.login();
    }
  }

  help() {
    this.router.navigate(['/forgotpassword']);
  }
  
  login() {

    this.session.setloading(true);

    this.session.registerSession(this.model.username, this.model.password)
      .subscribe(
        (response: any) => {

          var token = response.headers.get('Location');

          // if we have a token then create a session user
          if (token) {

            var user: User = response.body;

            // if we have user detail return then update the session user
            if (user) {

              this.session.createSessionUser(user, token);
              this.startupRoute();

            } else {
              this.alertService.error(this.translation.instant("Failed to get user details"));
              this.session.destroySessionUser();
              this.session.setloading(false);
            }

          } else {
            console.error("No session token returned");
            this.alertService.error(this.translation.instant("Login failure"));
            this.session.setloading(false);
          }
        },
        error => {
          console.error(error);
          this.alertService.error(this.translation.instant("Login failure"));
          this.session.setloading(false);
        }
      );
  }

  startupRoute() {

    if (this.session.getSessionUser().mustChangePassword) {
      this.session.setMenuActive(false); // disable the menu      
      this.router.navigate(['/changepassword']);
    } else {

      if(this.returnUrl=='/changepassword') {
        this.returnUrl='/home';
      }

      // get current dealer
      this.dealerService.getCurrentDealer(this.session.getSessionUser().username)
        .subscribe(
          data => {
            console.log("current dealer: " + JSON.stringify(data));
            this.currentDealer = data;
            this.session.setloading(false);
            this.session.setMenuActive(true); // enable the menu                                        
            this.router.navigate([this.returnUrl ? this.returnUrl : '/quote/add']);
          },
          error => {
            this.session.setloading(false);
            this.session.setMenuActive(true); // enable the menu      
            this.router.navigate([this.returnUrl ? this.returnUrl : '']);
          });
    }
  }

  logout() {
    this.session.setloading(true);
    let existingSessionToken = localStorage.getItem('sessiontoken');
    let existingSessionUser = JSON.parse(localStorage.getItem('sessionuser'));
    if (existingSessionToken && !this.clearsession && this.returnUrl!='/changepassword') {
      console.log("session exists, refreshing with token: " + existingSessionToken);
      this.session.testSessionURI(existingSessionToken)
        .subscribe(
          (response: any) => {
            // no error so assume all  ok
            this.session.createSessionUser(existingSessionUser, existingSessionToken);
            this.startupRoute();
          },
          error => {
            this.session.setloading(false);
            console.error(error);
            this.alertService.error(this.translation.instant("Session has expired, please re-login"));
            this.session.destroySessionUser();
          });
    } else {
      console.log("clearing session.");
      this.session.destroySessionUser();
      this.session.setloading(false);
    }
  }
}
