import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import { SessionService } from './session.service';
import { UserService } from './user.service';
import { UtilityService } from './utility.service';
import { AuthGuard } from './auth.guard';
import { ConfigService } from './config.service';

import { MatSidenavModule, MatToolbarModule, MatIconModule, MatListModule, MatProgressSpinnerModule, MatSnackBarModule } from '@angular/material';
import { DealerService } from './dealer.service';
import { RequestInterceptorService } from './request-interceptor.service';

import { OtpComponent } from './otp/otp.component';
import { MaterialModule } from './material/material.module';
import { OpcoService } from './opco.service';

export function configService(configService: ConfigService) {
  return () => configService.load();  
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,  
    MaterialModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    })
  ],
  providers: [
    AuthGuard,
    StatusBar,
    SplashScreen,
    ConfigService,
    {   provide: APP_INITIALIZER,
        useFactory: configService,
        deps: [ConfigService],
        multi: true 
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptorService,
      multi: true
    },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    SessionService,
    UserService,
    UtilityService,
    OpcoService,
    DealerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
