import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChangepasswordPageRoutingModule } from './changepassword-routing.module';

import { ChangepasswordPage } from './changepassword.page';
import { MaterialModule } from '../material/material.module';
import { OtpComponent } from '../otp/otp.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaterialModule,
    ChangepasswordPageRoutingModule
  ],
  declarations: [ChangepasswordPage,OtpComponent],
  entryComponents: [OtpComponent]
})
export class ChangepasswordPageModule {}
