import { Component, OnInit } from '@angular/core';
import { PasswordChange } from '../_models/passwordchange';
import { User } from '../_models/user';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from '../alert.service';
import { ConfigService } from '../config.service';
import { SessionService } from '../session.service';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material';
import { OtpComponent } from '../otp/otp.component';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.page.html',
  styleUrls: ['./changepassword.page.scss'],
})
export class ChangepasswordPage {

    model: PasswordChange = new PasswordChange();
    currentUser: User;
    NovaServiceApiUrl : string;
    ChangePasswordApiPath : string;
    otptoken : string;
    otppin : string;
    returnUrl : string; // if the user click Back this is where we return to

    myForm;

    constructor( private router: Router,                 
                 private alertService: AlertService,
                 private route: ActivatedRoute,
                 private config: ConfigService,
                 private sessionService : SessionService,
                 private http: HttpClient,
                 private location: Location,
                 public translation: TranslateService,
                 public dialog: MatDialog,
                 private _fb : FormBuilder
                 ) {

            this.NovaServiceApiUrl = config.getConfig("novaApiUrl");
            this.ChangePasswordApiPath = config.getConfig("novaApiPasswordChangePath"); 

            this.currentUser = this.sessionService.getSessionUser();     
                     
            if (!this.sessionService.isMenuActive()) {
                this.returnUrl="/login";
            } else
                this.returnUrl="../";          
                
                // build this components form group
    this.myForm = this._fb.group({
      'currentpassword': ['', Validators.required],
      'oldpassword': ['', Validators.required],
      'confirmpassword': ['', Validators.required]
    });

            this.sessionService.setloading(false);
    }

    submitUserPasswordChangeForm() {

        if(! (this.model.oldpassword && this.model.newpassword && this.model.confirmnewpassword && (this.model.newpassword===this.model.confirmnewpassword)))
          return;

        this.sessionService.setloading(true);

        // set the body
        let body = JSON.stringify({ oldPassword: this.model.oldpassword, newPassword: this.model.newpassword, confirmPassword: this.model.confirmnewpassword });

        // invoke Nova Service to request a password change OTP URL
        this.http.post(this.NovaServiceApiUrl + this.ChangePasswordApiPath, body, {headers: {'Content-Type':'application/json'}, observe: 'response'})
            .subscribe(
                (response: any) => {
                    this.otptoken = response.headers.get('Location');
                    // if we have a token then prompt for OTP PIN
                    if (this.otptoken) {                       

                        this.sessionService.setloading(false);

                          // PROMPT
                          const dialogRef = this.dialog.open(OtpComponent, {
                            width: '320px',
                            data: {otpurl: this.otptoken}
                          });
                      
                          dialogRef.afterClosed().subscribe(result => {
                            if(result && result===true) {
                              if (!this.sessionService.isMenuActive()) {
                                        this.sessionService.setMenuActive(true);
                              }
                              this.alertService.successKeepAfterNavigation("Password successfully changed");

                              // update the locally stored session
                              let user = this.sessionService.getSessionUser();
                              user.mustChangePassword=false;
                              this.sessionService.createSessionUser(user, this.sessionService.getSessionToken());
                              
                              this.router.navigate(["../../"]);
                            } else
                              this.alertService.error("Password change failure");
                          }); 

                    } else {
                        console.log("No OTP token returned");
                        this.alertService.error("Password change failure");
                        this.sessionService.setloading(false);
                    }
                },
                error => {
                    console.log(error);
                    this.alertService.error("Password change failure");
                    this.sessionService.setloading(false);
                }
            );
    }

    clickBack() {
        this.location.back();
    }
}
