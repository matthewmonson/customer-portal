import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactpersonComponent } from './contactperson/contactperson.component';
import { ContactpersonsComponent } from './contactpersons/contactpersons.component';
import { AddressComponent } from './address/address.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { ContactNumberComponent } from './contactnumber/contactnumber.component';
import { ContactNumbersComponent } from './contactnumbers/contactnumbers.component';
import { NgxMatIntlTelInputModule } from 'ngx-mat-intl-tel-input';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';

@NgModule({
  declarations: [
    ContactNumberComponent,
    ContactNumbersComponent,
    ContactpersonComponent, 
    ContactpersonsComponent, 
    AddressComponent,
    ConfirmDialogComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    IonicModule,
    CommonModule,
    NgxMatIntlTelInputModule,
    TranslateModule
  ],
  exports: [
    ContactNumberComponent, 
    ContactNumbersComponent, 
    ContactpersonComponent, 
    ContactpersonsComponent, 
    AddressComponent,
    ConfirmDialogComponent
  ],
  entryComponents: [
    ConfirmDialogComponent
  ]
})
export class SharedComponentsModule { }
