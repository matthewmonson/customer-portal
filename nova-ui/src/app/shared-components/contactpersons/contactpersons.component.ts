import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ContactPerson } from 'src/app/_models/contactperson';

@Component({
  selector: 'contact-persons',
  templateUrl: './contactpersons.component.html',
  styleUrls: ['./contactpersons.component.scss']
})
export class ContactpersonsComponent {

  @Input('parentForm')
  contactPersonsParentForm : FormGroup;

  @Input('parentFormSubmitted')
  contactPersonsParentFormSubmitted : boolean = false;

  @Input('contacts')
  public contacts : ContactPerson[] = [];

  @Input('disabled')
  public isDisabled : boolean;
  
  constructor() {
  }

  addContact() { 
    this.contacts.push(new ContactPerson());
  }

  removeContact(i: number) {
    this.contacts.splice(i,1);      
  }
}
