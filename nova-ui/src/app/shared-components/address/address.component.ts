import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Address } from 'src/app/_models/address';
import { UtilityService } from 'src/app/utility.service';

@Component({
  selector: 'address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit, OnDestroy {

  @Input('parentForm')
  addressParentForm: FormGroup;

  @Input('parentFormSubmitted')
  addressParentFormSubmitted : boolean = false;

  @Input('index')
  index: any;

  @Input('address')
  public address: Address;
  
  @Input('disabled')
  public isDisabled : boolean;

  @Input('dealeraddress')
  public isDealerAddress : boolean = false;

  addressForm: FormGroup;

  constructor(private _fb: FormBuilder,        
        private utility : UtilityService) {            
  }

  ngOnInit() {    

        console.log("this.isDealerAddress: " + this.isDealerAddress);
        // define the form group
        if(!this.isDealerAddress)
            this.addressForm = this._fb.group({
                addresslatitude: ['',Validators.required],
                addresslongitude: ['',Validators.required],
                addressline1: ['',Validators.required], 
                addressline2: [''],
                addressline3: [''],
                city: ['',Validators.required],
                state: ['',Validators.required],
                zipcode: ['',Validators.required],
                countrycode: ['',Validators.required]
            });
        else // dealer address has extra inputs
            this.addressForm = this._fb.group({
                addresslatitude: ['',Validators.required],
                addresslongitude: ['',Validators.required],
                addressline1: ['',Validators.required], 
                addressline2: [''],
                addressline3: [''],
                city: ['',Validators.required],
                state: ['',Validators.required],
                zipcode: ['',Validators.required],
                countrycode: ['',Validators.required],                
                sitename: ['',Validators.required],
                contactperson: ['',Validators.required],
                contactmobile: ['',Validators.required],
                erpsitenumber: ['',Validators.required],
                erpsiteid: ['',Validators.required]
            });

      if(this.isDisabled)
          this.addressForm.disable();

      // if parent form provided then lets link to it
      if(this.addressParentForm)
        this.addressParentForm.addControl("address" + (this.index ? this.index : ""), this.addressForm);
  }

  ngOnDestroy() {
      // if parent form provided then lets unlink from it
      if(this.addressParentForm)
        this.addressParentForm.removeControl("address" + (this.index ? this.index : ""));
  }

  getPreciseLocation() {          
    if (this.utility.getUserAddress()!= null) {
        let control = this.addressForm.controls['addressline1'];
        control.setValue(this.utility.getUserAddress().addressLine1);
        control = this.addressForm.controls['addressline2'];
        control.setValue(this.utility.getUserAddress().addressLine2);
        control = this.addressForm.controls['addressline3'];
        control.setValue(this.utility.getUserAddress().addressLine3);
        control = this.addressForm.controls['city'];
        control.setValue(this.utility.getUserAddress().city);
        control = this.addressForm.controls['state'];
        control.setValue(this.utility.getUserAddress().state);
        control = this.addressForm.controls['countrycode'];
        control.setValue(this.utility.getUserAddress().countryCode);
        control = this.addressForm.controls['zipcode'];
        control.setValue(this.utility.getUserAddress().zipCode);
        control = this.addressForm.controls['addresslatitude'];
        control.setValue(this.utility.getGpsLatitude());
        control = this.addressForm.controls['addresslongitude'];
        control.setValue(this.utility.getGpsLongitude());
    } else {
        alert("No address available");
    }
  }
}
