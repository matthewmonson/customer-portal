import { Component, OnInit, AfterViewInit, Input, OnDestroy, ViewChild, Renderer, ElementRef } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ContactNumber } from 'src/app/_models/contactnumber';
import { UtilityService } from 'src/app/utility.service';

declare var jQuery: any, intlTelInput: any, intlTelInputUtils: any;

@Component({
  selector: 'contact-number',
  templateUrl: './contactnumber.component.html',
  styleUrls: ['./contactnumber.component.scss']
})
export class ContactNumberComponent implements OnInit, OnDestroy {

  @Input('parentForm')
  public numberParentForm: FormGroup;

  @Input('parentFormSubmitted')
  contactNumberParentFormSubmitted: boolean = false;

  @Input('number')
  public number: ContactNumber;

  @Input('index')
  public index: any;

  @Input('disabled')
  public isDisabled: boolean;

  private el: HTMLElement;
  private elInput;
  private phoneNum = "";

  numberForm: FormGroup;

  constructor(private _fb: FormBuilder, el: ElementRef, private utility : UtilityService) {

    // define the form group
    this.numberForm = this._fb.group({
      type: ['', Validators.required],
      number: ['', Validators.required]
    });

    this.el = el.nativeElement;
  }

  ngOnInit() {

    if (this.isDisabled)
      this.numberForm.disable();

    // if parent form provided then lets link to it
    if (this.numberParentForm)
      this.numberParentForm.addControl("number" + (this.index ? this.index : ""), this.numberForm);
  }

  ngOnDestroy() {
    // if parent form provided then lets unlink from it
    if (this.numberParentForm)
      this.numberParentForm.removeControl("number" + (this.index ? this.index : ""));
  }
}
