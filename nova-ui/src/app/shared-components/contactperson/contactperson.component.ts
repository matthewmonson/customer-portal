import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ContactPerson } from 'src/app/_models/contactperson';

@Component({
  selector: 'contact-person',
  templateUrl: './contactperson.component.html',
  styleUrls: ['./contactperson.component.scss']
})
export class ContactpersonComponent implements OnInit, OnDestroy {

  @Input('parentForm')
  contactPersonParentForm: FormGroup;

  @Input('parentFormSubmitted')
  contactPersonParentFormSubmitted : boolean = false;

  @Input('index')
  index: any;

  @Input('contact')
  public contact: ContactPerson;
  
  @Input('disabled')
  public isDisabled : boolean;

  contactForm: FormGroup;

  constructor(private _fb: FormBuilder) { 

        // define the form group
        this.contactForm = this._fb.group({
            title: ['',Validators.required], 
            initials: ['',Validators.required],
            firstname: ['',Validators.required],
            lastname: ['',Validators.required]
        });        
  }

  ngOnInit() {    
    
      if(this.isDisabled)
          this.contactForm.disable();

      // if parent form provided then lets link to it
      if(this.contactPersonParentForm)
        this.contactPersonParentForm.addControl("contactperson" + (this.index ? this.index : ""), this.contactForm);
  }

  ngOnDestroy() {
      // if parent form provided then lets unlink from it
      if(this.contactPersonParentForm)
        this.contactPersonParentForm.removeControl("contactperson" + (this.index ? this.index : ""));
  }
}
