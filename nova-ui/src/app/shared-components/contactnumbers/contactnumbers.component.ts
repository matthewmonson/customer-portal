import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ContactNumber } from 'src/app/_models/contactnumber';

@Component({
  selector: 'contact-numbers',
  templateUrl: './contactnumbers.component.html',
  styleUrls: ['./contactnumbers.component.scss']
})
export class ContactNumbersComponent {

  @Input('parentForm')
  public contactNumbersParentForm: FormGroup;

  @Input('parentFormSubmitted')
  contactNumbersParentFormSubmitted : boolean = false;

  @Input('numbers')
  public numbers : ContactNumber[] = [];

  @Input('disabled')
  public isDisabled : boolean;
  
  constructor() { }

  addNumber() {
      this.numbers.push(new ContactNumber());
  }

  removeNumber(i: number) {
      this.numbers.splice(i,1);
  }
}
