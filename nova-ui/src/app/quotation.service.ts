import { Injectable } from '@angular/core';
import { SessionService } from './session.service';
import { ConfigService } from './config.service';
import { HttpClient } from '@angular/common/http';
import { AlertService } from './alert.service';
import { UtilityService } from './utility.service';
import { Quotation, QuotationDetails } from './_models/quotation';
import { Observable } from 'rxjs';
import { PriceRequest } from './_models/pricerequest';
import { PriceResponse } from './_models/priceresponse';
import { Status } from './_models/status';
import { PaymentType } from './_models/orderdetail';

@Injectable({
  providedIn: 'root'
})
export class QuotationService {

  NovaServiceApiUrl : string;
  QuotationAccountManagementCurrentQuotationApiPath : string;
  QuotationAccountManagementQuotationsApiPath : string;
  QuotationAccountManagementPaymentTypesApiPath : string;
  QuotationAccountManagementWarehousesApiPath : string;
  QuotationAccountManagementStatusesApiPath : string;
  QuotationAccountManagementPricingApiPath : string;
  QuotationAccountManagementCancellationApiPath : string;
  DocumentManagementProformasApiPath : string;

  HttpRequestTimeout : number;
  
  constructor(private session: SessionService, 
  private config: ConfigService, 
  private http: HttpClient, 
  private alert : AlertService, 
  private utlity : UtilityService) { 
      this.NovaServiceApiUrl                             = config.getConfig("novaApiUrl");        
      this.QuotationAccountManagementQuotationsApiPath   = config.getConfig("novaApiQuoteManagementQuotationsPath"); 
      this.QuotationAccountManagementPaymentTypesApiPath = config.getConfig("novaApiQuoteManagementPaymentTypesPath"); 
      this.QuotationAccountManagementWarehousesApiPath   = config.getConfig("novaApiQuoteManagementWarehousesPath"); 
      this.QuotationAccountManagementStatusesApiPath     = config.getConfig("novaApiQuoteManagementStatusesPath");
      this.QuotationAccountManagementPricingApiPath      = config.getConfig("novaApiQuoteManagementPricingPath");
      this.QuotationAccountManagementCancellationApiPath = config.getConfig("novaApiQuoteManagementCancellationPath");
      this.DocumentManagementProformasApiPath            = config.getConfig("novaApiDocumentManagementProformasPath");
      this.HttpRequestTimeout                            = config.getConfig("httpRequestTimeout");

      // clear cache when session destroyed
      session.getSessionStatus().subscribe((data:any) => {
          if(data=='destroyed'){
              this._getQuotations=null;
              this._getQuotation=null;
              this._getPaymentTypes=null;
              this._getStatuses=null;
          }
      });   
  }

  private _getQuotations : Quotation[] = [];
  getQuotations(code : String) : Observable<Quotation[]> {
      return Observable.create(observer => {
          this.http.get(this.NovaServiceApiUrl + this.QuotationAccountManagementQuotationsApiPath + "/" + code, {observe: 'response'}) //.timeout(this.HttpRequestTimeout)
              // .catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
              // .map((response: Response) => response.json())
              .subscribe((data:any) => {
                  this._getQuotations = data.body;
                  observer.next(this._getQuotations);
                  observer.complete();
              }, (err) => {
                  console.log("err: " + JSON.stringify(err));
                  observer.error(err);
              });
      });
  }
  getQuotationsFromCache() : Quotation[] {
      return this._getQuotations;
  }

  private _getQuotation : Quotation;
  private currentQuotationNumber : String = "";
  getQuotation(code : String, quotationNumber : String) : Observable<Quotation> {
      return Observable.create(observer => {
          this.http.get(this.NovaServiceApiUrl + this.QuotationAccountManagementQuotationsApiPath + "/" + code + "/" + quotationNumber, {observe: 'response'}) //.timeout(this.HttpRequestTimeout)
              // .catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
              // .map((response: Response) => response.json())
              .subscribe((data:any) => {
                  this._getQuotation = data.body;
                  this.currentQuotationNumber=quotationNumber;
                  observer.next(this._getQuotation);
                  observer.complete();
              }, (err) => {
                  console.log("err: " + JSON.stringify(err));
                  observer.error(err);
              });
      });
  }
  getQuotationFromCache(quotationNumber : String): Quotation {
      if(quotationNumber==this.currentQuotationNumber)
          return this._getQuotation;
      else
          return new Quotation();
  }

  private _getPaymentTypes : PaymentType[] = [];
  getPaymentTypes(code : String) : Observable<PaymentType[]> {
      if(!this._getPaymentTypes || !this._getPaymentTypes.length) {
          return Observable.create(observer => {
              this.http.get(this.NovaServiceApiUrl + this.QuotationAccountManagementPaymentTypesApiPath + "/" + code, {observe: 'response'}) //.timeout(this.HttpRequestTimeout)
                  // .catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
                  // .map((response: Response) => response.json())
                  .subscribe((data:any) => {
                      this._getPaymentTypes = data.body;
                      observer.next(this._getPaymentTypes);
                      observer.complete();
                  }, (err) => {
                      console.log("err: " + JSON.stringify(err));
                      observer.error(err);
                  });
      });
      } else
          return Observable.create(observer => { observer.next(this._getPaymentTypes); observer.complete(); });
  }

  private _getStatuses : Status[] = [];
  getStatuses(code : String) : Observable<Status[]> {
      if(!this._getStatuses || !this._getStatuses.length) {
          return Observable.create(observer => {
              this.http.get(this.NovaServiceApiUrl + this.QuotationAccountManagementStatusesApiPath + "/" + code, {observe: 'response'}) //.timeout(this.HttpRequestTimeout)
                  // .catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
                  // .map((response: Response) => response.json())
                  .subscribe((data:any) => {
                      this._getStatuses = data.body;
                      observer.next(this._getStatuses);
                      observer.complete();
                  }, (err) => {
                      console.log("err: " + JSON.stringify(err));
                      observer.error(err);
                  });
      });
      } else
          return Observable.create(observer => { observer.next(this._getStatuses); observer.complete(); });
  }

  getQuotePrices(code : String, priceRequest : PriceRequest) : Observable<PriceResponse> {        
      return Observable.create(observer => {
          this.http.post(this.NovaServiceApiUrl + this.QuotationAccountManagementPricingApiPath + "/" + code, JSON.stringify(priceRequest), {headers: {'Content-Type':'application/json'}, observe: 'response'}) //.timeout(this.HttpRequestTimeout)
              // .catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
              // .map((response: Response) => response.json())
              .subscribe((data:any) => {                    
                  observer.next(data.body);
                  observer.complete();
              }, (err) => {
                  console.log("err: " + JSON.stringify(err));
                  observer.error(err);
              });
      });        
  }

  createQuotation(code : String, QuotationDetails: QuotationDetails) {
      // invoke Nova Service to create a new Quotation
      return this.http.post(this.NovaServiceApiUrl + this.QuotationAccountManagementQuotationsApiPath + "/" + code, QuotationDetails, {observe: 'response'})// .catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) });
  }

  acceptQuotation(code : String, quotationNumber : String) {
      // invoke Nova Service to accept a new Quotation
      return this.http.post(this.NovaServiceApiUrl + this.QuotationAccountManagementQuotationsApiPath + "/" + code + "/" + quotationNumber, {observe: 'response'})// .catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) });
  }

  cancelQuotation(code : String, quotationNumber : String) {
      // invoke Nova Service to cancel a Quotation
      return this.http.post(this.NovaServiceApiUrl + this.QuotationAccountManagementCancellationApiPath + "/" + code + "/" + quotationNumber, {observe: 'response'})// .catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) });
  }

  updateQuotation(QuotationDetails: QuotationDetails) {
      // invoke Nova Service to update an existing Quotation
      return this.http.put(this.NovaServiceApiUrl + this.QuotationAccountManagementQuotationsApiPath, Quotation,{observe: 'response'})// .catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) });
  }

  deleteQuotation(quotationNumber : String) {
      // invoke Nova Service to delete a Quotation
      return this.http.delete(this.NovaServiceApiUrl + this.QuotationAccountManagementQuotationsApiPath + "/" + quotationNumber, {observe: 'response'})// .catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) });
  }

  downloadProforma(code : String, quotationNumber : String) {
      let languageID=1; //TODO: Hardcode mapping to  language ID, must fix
      if(this.utlity.getLocale()==='fr')
          languageID=3;    
      // invoke Nova Service to request proforma invoice
      return this.http.get(this.NovaServiceApiUrl + this.DocumentManagementProformasApiPath + "/" + code + "/" + quotationNumber + "?languageId=" + languageID)// .catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) });
  }
}

