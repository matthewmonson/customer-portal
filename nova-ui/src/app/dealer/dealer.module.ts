import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '../material/material.module';
import { SharedComponentsModule } from '../shared-components/shared-components.module';
import { DealerPageRoutingModule } from './dealer-routing.module';
import { DealerPage } from './dealer.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DealerPageRoutingModule,
    TranslateModule,
    MaterialModule,
    ReactiveFormsModule,
    SharedComponentsModule
  ],
  declarations: [DealerPage],
  exports: [
    SharedComponentsModule
  ]
})
export class DealerPageModule {}