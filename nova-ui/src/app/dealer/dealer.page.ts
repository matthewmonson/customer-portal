import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Dealer } from '../_models/dealer';
import { Router, ActivatedRoute } from '@angular/router';
import { DealerService } from '../dealer.service';
import { AlertService } from '../alert.service';
import { SessionService } from '../session.service';
import { TranslateService } from '@ngx-translate/core';
import { SupplierSystemSpec } from '../_models/suppliersystemspec';
import { Location } from '@angular/common';
import { UtilityService } from '../utility.service';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../shared-components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-dealer',
  templateUrl: './dealer.page.html',
  styleUrls: ['./dealer.page.scss'],
})
export class DealerPage implements OnInit {

  myForm : FormGroup;
  formsubmitted : boolean = false;
  model: Dealer = null;    
  mode : string = "";
  supplier : string;
  code : string;
  dealersDatasource;
  dealers : Dealer[]; 

  displayedColumns: string[] = ['code', 'supplier', 'name'];

  constructor(
      private router: Router,
      private dealerService: DealerService,
      private alertService: AlertService,
      private route: ActivatedRoute,
      private session : SessionService,
      private _fb: FormBuilder,
      //private mapsAPILoader: MapsAPILoader,
      public translation: TranslateService,
      private location: Location,
      public utility: UtilityService,
      public dialog: MatDialog) { 

          route.params.subscribe(params => { 
              this.supplier = params['supplier'];
              this.code = params['code'];                
          });
          
          this.mode=this.router.url.split("/")[2]; // list/add/edit/delete

          // build this components form group
          this.myForm = this._fb.group({
              'code' : ['',Validators.required],
              'name' : ['',Validators.required],
              'supplier' : [''],
              'taxnumber' : ['',Validators.required],
              'registrationnumber' : ['',Validators.required],
              'posclientid' : [''],
              'logo' : [''],
              'proformatemplate':[''],
              'theme':[''],
              'serviceusername':[''],
              'servicepassword':[''],
              'repeatservicepassword':[''],
              'serviceurl':[''],
              'customername':[''],
              'erpcustomerid':[''],
              'erpcustomeraccountnumber':['', Validators.required],
              'blacklisted':[''],
              'issupplier':['']
          });  
      }

  ngOnInit() {

      switch(this.mode) {

              case "list" :

                  if (this.supplier) {

                      // get list of dealers from cache                    
                      this.dealers = this.dealerService.getAllDealersForSupplierFromCache(this.supplier);

                      // get list of dealers from live
                      this.session.setloading(true);
                      this.dealerService.getAllDealersForSupplier(this.supplier)
                          .subscribe(
                              data => {                     
                                  this.dealers = data;
                                  this.dealersDatasource = new MatTableDataSource(this.dealers);
                                  this.session.setloading(false);

                                  console.log("dealers for supplier: " + this.supplier);

                                
                              },
                              error => {
                                  console.log("list error: " + JSON.stringify(error));                                
                                  this.alertService.error(this.translation.instant("Failed to get list of dealers for supplier")); //  + " [" + JSON.stringify(error) + "]");
                                  this.session.setloading(false);
                              });  

                  } else {

                      // get deep list of dealers from cache                    
                      this.dealers = this.dealerService.getAllDealersFromCache()
                      this.dealersDatasource = new MatTableDataSource(this.dealers);

                      // get deep list of dealers from live
                      this.session.setloading(true);
                      this.dealerService.getAllDealers()
                          .subscribe(
                              data => {                     
                                  this.dealers = data;
                                  this.dealersDatasource = new MatTableDataSource(this.dealers);
                                  //this.dealers = JSON.parse(`[{"_id":"72624","shipToAddress":{"gpsPoint":{},"addressLine1":"BP 18342","addressLine2":null,"addressLine3":null,"city":"Douala","state":"Douala","zipCode":"8888","countryCode":"CAM","erpSiteId":6068,"siteName":"Coface","erpSiteNumber":"16678","contactPerson":"    Matthew","contactMobile":"0796958797","posClientId":760},"code":"72624","isSupplier":false,"taxNumber":"46282846392","owners":["root","mtn"],"supplierSystemSpec":{"logo":"","proformaTemplate":"","theme":"","serviceRegistration":{"serviceUsername":"","servicePassword":"","serviceUrl":""}},"accountNumber":"3622","billToAddress":{"gpsPoint":{},"addressLine1":"BP 18342","addressLine2":null,"addressLine3":"Douala","city":"Douala","state":"Douala","zipCode":"8888","countryCode":"CAM","erpSiteId":5972,"siteName":"Coface","erpSiteNumber":"16678","contactPerson":"    Matthew","contactMobile":"0796958797","posClientId":759},"registrationNumber":"1/293/18284645","audit":{"createdDate":1503317928174,"createdByUser":"mtn_opcoadmin","modifiedDate":1503317928174,"modifiedByUser":"mtn_opcoadmin"},"supplier":"mtn","contactPersons":[{"contactNumbers":[],"title":"MR","initials":"C","firstname":"Coface","lastname":"Coface"}],"name":"Coface","posClientId":760,"customerAccount":{"erpCustomerAccountNumber":"3622","blacklisted":false,"customerName":"Coface","erpCustomerId":72624}},{"_id":"IHS","shipToAddress":{"gpsPoint":{"latitude":"-34.1418189","longitude":"18.3860652"},"addressLine1":"Immeuble Ernst & Young Boulevard De La Liberte","addressLine2":null,"city":"Cape Town","state":"Western Cape","zipCode":"7975","countryCode":"ZA","erpSiteId":6049,"siteName":"IHSShipping","erpSiteNumber":"16659","contactPerson":"    Matthew","contactMobile":"0796958797","posClientId":742,"addressLine3":null},"code":"IHS","isSupplier":false,"taxNumber":"12345","owners":["root","mtn"],"supplierSystemSpec":{"logo":"","proformaTemplate":"","theme":"","serviceRegistration":{"serviceUsername":"","servicePassword":"","serviceUrl":""}},"accountNumber":"3603","billToAddress":{"gpsPoint":{"latitude":"-34.1418189","longitude":"18.3860652"},"addressLine1":"Immeuble Ernst & Young Boulevard De La Liberte","addressLine2":null,"city":"Cape Town","state":"Western Cape","zipCode":"7975","countryCode":"ZA","erpSiteId":5953,"siteName":"IHSBilling","erpSiteNumber":"16659","contactPerson":"    Matthew","contactMobile":"0796958797","posClientId":741,"addressLine3":"Douala"},"registrationNumber":"67890","audit":{"createdDate":1503313816948,"createdByUser":"mtn_opcoadmin","modifiedDate":1503313816948,"modifiedByUser":"mtn_opcoadmin"},"supplier":"mtn","contactPersons":[{"contactNumbers":[],"title":"Mr","initials":"MM","firstname":"Matthew","lastname":"Monson"}],"name":"IHS","posClientId":741,"customerAccount":{"erpCustomerAccountNumber":"3603","blacklisted":false,"customerName":"IHS Cameroon PLC","erpCustomerId":72605}}]`);
                                  
                                  this.session.setloading(false);

                                  let suppliers : String = "";
                                  // every dealer (child), determine unique list of suppliers across them
                                  for(let dealer of this.dealers) {
                                      if(suppliers.indexOf(dealer.supplier)===-1)
                                          suppliers=(suppliers > "" ? suppliers + ',' : '') + dealer.supplier;
                                  }
                                  console.log('suppliers: ' + suppliers);
                              },
                              error => {
                                  console.log("list error: " + JSON.stringify(error));                                
                                  this.alertService.error(this.translation.instant("Failed to get list of dealers")); //  + " [" + JSON.stringify(error) + "]");
                                  this.session.setloading(false);
                              });  
                  }

                  break;

              case "add" :
                  
                  this.myForm.controls['supplier'].disable();                    
                  this.model = new Dealer();
                  this.model.supplier=this.supplier;
                                      
                  break;

              case "edit" :

                  this.myForm.controls['code'].disable();
                  this.myForm.controls['supplier'].disable();
                  
                  this.model=this.dealerService.getDealerFromCache(this.supplier, this.code);
                  
                  // get dealer detail
                  this.session.setloading(true);
                  this.dealerService.getDealer(this.supplier, this.code)
                      .subscribe(
                          data => {
                              console.log("edit data: " + data);
                              console.log("edit data json: " + JSON.stringify(data));
                              this.model = data;           
                              console.log('model json: ' + JSON.stringify(this.model));     
                              if(!this.model.supplierSystemSpec) {
                                  this.model.supplierSystemSpec = new SupplierSystemSpec();
                              }                  
                              this.myForm.controls['repeatservicepassword'].setValue(this.model.supplierSystemSpec.serviceRegistration.servicePassword);
                              this.acceptedDealerAccountNumber=this.model.customerAccount.erpCustomerAccountNumber;
                              this.isChecked=this.model['blacklisted'];
                              this.myForm.controls['blacklisted'].setValue(this.isChecked);
                              this.session.setloading(false);
                          },
                          error => {
                              console.log("edit error: " + JSON.stringify(error));
                              this.alertService.error(this.translation.instant("Failed to get dealer detail")); //  + " [" + JSON.stringify(error) + "]");
                              console.log("edit error json: " + JSON.stringify(error));
                              this.session.setloading(false);
                              return;
                          });                                        
                  
                  break;

              case "view" :

                  this.myForm.disable();
                  
                  this.model=this.dealerService.getDealerFromCache(this.supplier, this.code);
                  
                  // get dealer detail
                  this.session.setloading(true);
                  this.dealerService.getDealer(this.supplier, this.code)
                      .subscribe(
                          data => {
                              console.log("edit data: " + data);
                              console.log("edit data json: " + JSON.stringify(data));
                              this.model = data;           
                              console.log('model json: ' + JSON.stringify(this.model));     
                              if(!this.model.supplierSystemSpec) {
                                  this.model.supplierSystemSpec = new SupplierSystemSpec();
                              }                       
                              this.myForm.controls['repeatservicepassword'].setValue(this.model.supplierSystemSpec.serviceRegistration.servicePassword);    
                              this.session.setloading(false);
                          },
                          error => {
                              console.log("edit error: " + JSON.stringify(error));
                              this.alertService.error(this.translation.instant("Failed to get dealer detail")); //  + " [" + JSON.stringify(error) + "]");
                              console.log("edit error json: " + JSON.stringify(error));
                              this.session.setloading(false);
                              return;
                          });                                        
                  
                  break;

              case "delete" :

                  this.myForm.disable();

                  // first attempt to get Dealer model from service cache
                  this.model=this.dealerService.getDealerFromCache(this.supplier, this.code);
                  
                  // get dealer detail
                  this.session.setloading(true);
                  this.dealerService.getDealer(this.supplier, this.code)
                      .subscribe(
                          data => {
                              console.log("delete data: " + data);
                              console.log("delete data json: " + JSON.stringify(data));
                              this.model = data;                                     
                              if(!this.model.supplierSystemSpec) {
                                  this.model.supplierSystemSpec = new SupplierSystemSpec();
                              }
                              this.myForm.controls['repeatservicepassword'].setValue(this.model.supplierSystemSpec.serviceRegistration.servicePassword);
                              this.session.setloading(false);            
                          },
                          error => {
                              console.log("delete error: " + JSON.stringify(error));
                              this.alertService.error(this.translation.instant("Failed to get dealer detail")); //  + " [" + JSON.stringify(error) + "]");
                              console.log("delete error json: " + JSON.stringify(error));
                              this.session.setloading(false);
                              return;
                          });
                                      
                  break;
          }            
  }

  clickDelete() {

    //confirm
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      data: { title: 'Confirm Delete' , content: 'Delete Dealer ' + this.model.code + '?' }
  });

  dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.session.setloading(true);
      // delete dealer
      this.dealerService.deleteDealer(this.model.supplier, this.model.code)
      .subscribe(
        data => {
          this.session.setloading(false);
          this.alertService.successKeepAfterNavigation(this.translation.instant("Dealer deleted"));
          this.router.navigate(['/dealer/list', this.supplier ? this.supplier : '']);
        },
        error => {
          this.session.setloading(false);
          console.log(error);
          this.alertService.error(this.translation.instant("Failed to delete Dealer")); //  + " [" + JSON.stringify(error) + "]");
      
        });
      }
    });
  }

  clickView(event) {
      this.router.navigate(['/dealer/view', event.data['supplier'], event.data['code']]);
  }

  clickEdit(supplier, code) {
      this.router.navigate(['/dealer/edit', supplier, code]);
  }

  clickAdd() {
      this.router.navigate(['/dealer/add', this.supplier ? this.supplier : this.session.getSessionUser().organization]);
  }

  clickCancel() {
    this.router.navigate(['/dealer/list']);
  }

  clickManageDealerUsers() {
      this.router.navigate(['/user/list', this.model.code]);
  }

  clickManageDealerSubDealers() {
      this.router.navigate(['/dealer/list', this.model.code]);
  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
  Object.keys(formGroup.controls).forEach(field => {  //{2}
    const control = formGroup.get(field);             //{3}
    if (control instanceof FormControl) {             //{4}
      control.markAsTouched({ onlySelf: true });
    } else if (control instanceof FormGroup) {        //{5}
      this.validateAllFormFields(control);            //{6}
    }
  });
}

  submitDealerCrudForm() {

    this.alertService.clearAlert();

    if (!this.myForm.valid) {
      this.validateAllFormFields(this.myForm);
      this.alertService.error(this.translation.instant("The form is not valid, please correct the values and try again"));
    } else {
      // validate form
      if (this.mode != 'delete') {
        this.formsubmitted = true;
        // if the user changed the dealer account number since last validation then check it
        if (this.acceptedDealerAccountNumber != this.model['customerAccount']['erpCustomerAccountNumber']) {
          this.validateDealerAccountNumber(true);
          return;
        }
        this.myForm.updateValueAndValidity();
        if (!this.myForm.valid || (this.model.isSupplier &&
          (this.myForm.controls['servicepassword'].value != this.myForm.controls['repeatservicepassword'].value ||
            this.myForm.controls['logo'].value <= '' ||
            this.myForm.controls['proformatemplate'].value <= '' ||
            this.myForm.controls['theme'].value <= '' ||
            this.myForm.controls['serviceusername'].value <= '' ||
            this.myForm.controls['serviceurl'].value <= ''))
        ) {
          this.alertService.error(this.translation.instant("The form is not valid, please correct the values and try again"));
          return;
        }
        this.formsubmitted = false;
      }

      this.session.setloading(true);

      // submit form        
      switch (this.mode) {
        case "add":

          this.model.accountNumber = this.acceptedDealerAccountNumber;

          // create new dealer
          this.dealerService.createDealer(this.model)
            .subscribe(
              data => {
                this.session.setloading(false);
                this.alertService.successKeepAfterNavigation(this.translation.instant("Dealer created, please create the Dealer Admin User"));
                this.router.navigate(['/user/add', this.model.code]);
              },
              error => {
                this.session.setloading(false);
                console.log(error);
                this.alertService.error(this.translation.instant("Failed to add Dealer")); //  + " [" + JSON.stringify(error) + "]");
              });
          break;

        case "edit":

          delete this.model['_id'];

          this.model.accountNumber = this.acceptedDealerAccountNumber;

          console.log("model updated as: " + JSON.stringify(this.model));
          // update dealer
          this.dealerService.updateDealer(this.model)
            .subscribe(
              data => {
                this.session.setloading(false);
                this.alertService.success(this.translation.instant("Dealer updated"));
              },
              error => {
                this.session.setloading(false);
                console.log(error);
                this.alertService.error(this.translation.instant("Failed to update Dealer")); //  + " [" + JSON.stringify(error) + "]");
              });
          break;

        case "delete":
          // delete dealer
          this.dealerService.deleteDealer(this.model.supplier, this.model.code)
            .subscribe(
              data => {
                this.session.setloading(false);
                this.alertService.successKeepAfterNavigation(this.translation.instant("Dealer deleted"));
                this.router.navigate(['/dealer/list', this.supplier ? this.supplier : '']);
              },
              error => {
                this.session.setloading(false);
                console.log(error);
                this.alertService.error(this.translation.instant("Failed to delete Dealer")); //  + " [" + JSON.stringify(error) + "]");
              });
          break;
      }
    }
  }

  private billingAddressContainerCompressed=true;
  toggleBillingAddressContainerCompressed() {
      this.billingAddressContainerCompressed=this.billingAddressContainerCompressed ? false : true;
  }

  private shippingAddressContainerCompressed=true;
  toggleShippingAddressContainerCompressed() {
      this.shippingAddressContainerCompressed=this.shippingAddressContainerCompressed ? false : true;
  }

  private acceptedDealerAccountNumber : string = "";
  lookupDealerAccountDetails() {        
      if(this.model['customerAccount']['erpCustomerAccountNumber']>"") {
          this.validateDealerAccountNumber();
      }
  }

  validateDealerAccountNumber(showUpdateMessage? : boolean) {
      this.alertService.clearAlert();
      this.session.setloading(true);
      this.dealerService.getDealerAccountDetail(this.model['customerAccount']['erpCustomerAccountNumber'])
          .subscribe(
              data => {
                  this.acceptedDealerAccountNumber = this.model['customerAccount']['erpCustomerAccountNumber'];
                  this.session.setloading(false);
                  this.model.customerAccount.blacklisted = data.blacklisted;
                  this.model.customerAccount.erpCustomerId = data.erpCustomerId;
                  for(let client of data.clientDetails) {

                      if(client.primaryBillTo===true) {           
                          this.model.billToAddress.posClientId    = client.id;
                          this.model.billToAddress.addressLine1   = client.address1;
                          this.model.billToAddress.addressLine2   = client.address2;
                          this.model.billToAddress.addressLine3   = client.address3;
                          this.model.billToAddress.erpSiteNumber  = client.erpSiteNumber;
                          this.model.billToAddress.erpSiteId      = client.erpSiteId;
                          this.model.billToAddress.contactMobile  = client.contactMsisdn;
                          this.model.billToAddress.contactPerson  = client.contactPerson;
                      }

                      if(client.primaryShipTo===true) {                              
                          this.model.shipToAddress.posClientId    = client.id;                        
                          this.model.registrationNumber           = client.registrationNumber;
                          this.model.taxNumber                    = client.taxNumber;
                          this.model.customerAccount.customerName = client.name;                                 
                          this.model.shipToAddress.addressLine1   = client.address1;
                          this.model.shipToAddress.addressLine2   = client.address2;
                          this.model.shipToAddress.addressLine3   = client.address3;
                          this.model.shipToAddress.erpSiteNumber  = client.erpSiteNumber;
                          this.model.shipToAddress.erpSiteId      = client.erpSiteId;
                          this.model.shipToAddress.contactMobile  = client.contactMsisdn;
                          this.model.shipToAddress.contactPerson  = client.contactPerson;
                      }
                  }
                  if(showUpdateMessage)
                      this.alertService.info(this.translation.instant("Dealer account data updated, please check the values and re-submit the form"));
              },
              error => {
                  this.acceptedDealerAccountNumber = "";
                  this.session.setloading(false);
                  console.log(error);
                  this.alertService.error(this.translation.instant("Failed to retrieve Dealer Account Detail")); //  + " [" + JSON.stringify(error) + "]");
                  this.myForm.controls['erpcustomeraccountnumber'].setErrors({"failed": true});
              });
  }

  _shippingSameAsBillingAddress : boolean = false;
  shippingSameAsBillingAddress() {
    this._shippingSameAsBillingAddress=!this._shippingSameAsBillingAddress;
    if(this._shippingSameAsBillingAddress && (this.mode==='add' || this.mode==='edit')) {
        this.model.shipToAddress.gpsPoint=this.model.billToAddress.gpsPoint;
        this.model.shipToAddress.addressLine1=this.model.billToAddress.addressLine1;
        this.model.shipToAddress.addressLine2=this.model.billToAddress.addressLine2;
        this.model.shipToAddress.addressLine3=this.model.billToAddress.addressLine3;
        this.model.shipToAddress.city=this.model.billToAddress.city;
        this.model.shipToAddress.state=this.model.billToAddress.state;
        this.model.shipToAddress.zipCode=this.model.billToAddress.zipCode;            
    }
  }

  nodeDealerTreeNodeSelect(event) {
      if(event.node.label==='ADD')
          this.router.navigate(['/dealer/add', event.node.data]);
      else
          this.router.navigate(['/dealer/view', event.node.data, event.node.label]);
  }

  public isChecked : boolean = false;
  toggleChanged($event) {
      this.isChecked=!this.isChecked;
      this.myForm.controls['blacklisted'].setValue(this.isChecked ? true : false);
  }

  public isSupplierChecked : boolean = false;
  toggleSupplierChanged($event) {
      this.isSupplierChecked=!this.isSupplierChecked;
      this.myForm.controls['issupplier'].setValue(this.isSupplierChecked ? true : false);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dealersDatasource.filter = filterValue.trim().toLowerCase();
  }
}
