import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { SessionService } from './session.service';
import { Address } from './_models/address';
import { Router } from '@angular/router';
import { AlertService } from './alert.service';
import { TranslateService } from '@ngx-translate/core';
import { Observable, NEVER } from 'rxjs';

import numeral from 'numeral';

var google:any;

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  private gpsLatitude : String = "";
  private gpsLongitude : String = "";
  private googleAddress : any = null;
  
  private address : Address;

  private locale : string = "";
  setLocale(locale : string) {
      this.locale = locale;
  }
  getLocale() {
      return this.locale;
  }

  getGpsLatitude() { return this.gpsLatitude }
  getGpsLongitude() { return this.gpsLongitude }
  getUserAddress() { return this.address }  

  private amountFormat : String = "";

  public formatAmount(amount : number, currencySymbol?: string, prefixCurrencySymbol? : boolean) : string {
    var formatted : string = "";
    if(this.amountFormat==="") {
        this.amountFormat = this.config.getConfig("uiAmountFormat");
    }
    if(this.amountFormat && this.amountFormat!="") {
        formatted = numeral(amount).format(this.amountFormat);                
    } else
        formatted = amount.toString();

    if(currencySymbol) {
        if(prefixCurrencySymbol)
            formatted = currencySymbol + " " + formatted;
        else
            formatted = formatted + " " + currencySymbol;
    }

    return formatted;
  }

  constructor(public translation: TranslateService, public config : ConfigService, public session : SessionService, private router: Router, private alertService: AlertService) {
      /*
      // get geolocation of the user
      if ("geolocation" in navigator) {
          navigator.geolocation.getCurrentPosition((position) => {
              this.gpsLatitude = position.coords.latitude.toString();
              this.gpsLongitude = position.coords.longitude.toString();
              let geocoder = new google.maps.Geocoder();
              let latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
              let request = {
                  location: latlng
              };
              geocoder.geocode(request, (results, status) => {
                  if (status == google.maps.GeocoderStatus.OK) {                                            
                      this.setUserStreetAddress(results);
                      console.log("User location: " + JSON.stringify(this.getUserAddress()));
                  }
              });
          });
      }
      */
  }

  // extract street address from google geocode API result
  private setUserStreetAddress(results) {    
    this.address = new Address();
    for (let addr of results) {        
        if (addr.types[0] == "street_address") {
            for(let line of addr.address_components) {
                if(line.types[0]=="street_number")
                    this.address.addressLine1=line.long_name;
                if(line.types[0]=="route")
                    this.address.addressLine2=line.long_name;
                if(line.types[0]=="sublocality")
                    this.address.addressLine3=line.long_name;                
                if(line.types[0]=="locality")
                    this.address.city=line.long_name;
                if(line.types[0]=="administrative_area_level_1")
                    this.address.state=line.long_name;
                if(line.types[0]=="country")
                    this.address.countryCode=line.short_name;
                if(line.types[0]=="postal_code")
                    this.address.zipCode=line.short_name;
            }
        }
    }
  }

  parseHTTPErrorResponse(error : Response) { 

    console.log("HTTP error response JSON: " + JSON.stringify(error));

    var errorMessage = "";
    if(error.status===401) { // session has most likely expired, route back to login        
        this.router.navigate(["/login"], { queryParams: { returnurl: this.router.url} });
        //this.alertService.error(this.translation.translate("Your session has expired, please log in again"));
        return NEVER; // never return to the caller
    } else {
        if(error['message']) { // do we have a message element
            this.translation.get(error['message']).toPromise().then(translation => { errorMessage=translation });
        }
        else {
            if(error.status===0) { // deal with connection errors, status 0
                this.translation.get("Connection error, please try again").toPromise().then(translation => { errorMessage=translation });
            } else {
                if(error['_body']['message']) { // sometimes we get a 'message' element inside '_body'
                    this.translation.get(error['_body']['message']).toPromise().then(translation => { errorMessage=translation });
                }
                if(error['_body'] && errorMessage==="") { // sometimes we get '_body' as a string
                    try { // ...first try and parse _body string to JSON
                        var errorJSON = JSON.parse(error['_body']);
                        if(errorJSON != null && errorJSON['message']) {
                            this.translation.get(errorJSON['message']).toPromise().then(translation => { errorMessage=translation });
                        }
                    } catch(err) { }
                    if (errorMessage==="") { // ...if still nothing then use _body as is
                        this.translation.get(error['_body']).toPromise().then(translation => { errorMessage=translation });
                    }
                }
                if(errorMessage==="") // if all else fails barf the error json string
                    errorMessage=JSON.stringify(error);
            }
        }      

        return Observable.throw(new Error(errorMessage));
    }
  }    

    userHasRoles(roles: string[]) {
        let hasroles : boolean = false;
        for(let role of roles) {
            for (let userrole of this.session.getSessionUser().roles) {
                if(userrole===role) {
                    hasroles=true;
                    break;
                }
            }
        }
        return hasroles;
    }
}
