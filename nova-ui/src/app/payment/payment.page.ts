import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../alert.service';
import { QuotationService } from '../quotation.service';
import { DealerService } from '../dealer.service';
import { SessionService } from '../session.service';
import { UtilityService } from '../utility.service';
import { PaymentService } from '../payment.service';
import { ConfigService } from '../config.service';
import { Payment, PaymentDetail, PaymentType } from '../_models/payment';
import { Product } from '../_models/product';
import { Status } from '../_models/status';
import { Dealer } from '../_models/dealer';
import { FormArray, FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import  moment from 'moment/moment';
import { TranslateService } from '@ngx-translate/core';

import filesaver from 'filesaver.js';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {

  myForm : FormGroup;
  formsubmitted : boolean = false;
  model: Payment = null;    
  mode : string = "";
  paymentNumber : string;
  payments : Payment[];

  paymentsDatasource;

  selectedPayment : Payment;
  payment : Payment;    
  paymentDetail : PaymentDetail;
  displayDialog : boolean = false;
  
  statuses : Status[] = []; 

  dateFilter : any;

  paymentTypes : PaymentType[];
  
  selectedWarehouse : string;
  products : Product[] = [];

  productSelectAvailable : number;

  selectedProduct : Product;
  selectedProductQuantity : number;

  notenoughstock : boolean = false;

  currentDealer : Dealer;

  pricingpayment = false;

  //countries        = require('country-data').countries;
  //currencies       = require('country-data').currencies;
  selectedCurrency  : string;
  selectedCurrencySymbol : string;
  
  defaultCurrencyCode: string;
  prefixCurrencySymbol: string;

  acceptingPayment : boolean = false;

  returnurl : string = "";

  displayedColumns: string[] = ['paymentNumber', 'paymentDate', 'paymentType', 'amount', 'status'];

  constructor(
      private router: Router,        
      private quotationService: QuotationService,
      private dealerService: DealerService,
      private alertService: AlertService,
      private route: ActivatedRoute,
      private session : SessionService,
      private paymentService : PaymentService,
      private utilityService : UtilityService,
      private config : ConfigService,
      private _fb: FormBuilder,
      public translation: TranslateService,        
      private location: Location) {

          route.params.subscribe(params => { 
              this.paymentNumber = params['paymentnumber'];                
          });  
          this.mode=this.router.url.split("/")[2]; // list/add/edit/delete/view

          this.returnurl="";
          this.defaultCurrencyCode = this.config.getConfig("uiCurrencyCode");
          this.prefixCurrencySymbol = this.config.getConfig("uiAmountCurrencySymbolPrefix");          
          
          // build this components form group
          this.myForm = this._fb.group({
          });
      }

  ngOnInit() {

      switch(this.mode) {

              case "list" :

                  this.session.setloading(true);
                                      
                  // get current dealer
                  this.dealerService.getCurrentDealer(this.session.getSessionUser().username)
                      .subscribe(
                          data => {                     
                              console.log("current dealer: " + JSON.stringify(data));
                              this.currentDealer = data;         

                              // get paymenttypes
                              this.paymentTypes = [];
                              this.paymentService.getPaymentTypes(this.currentDealer.code)
                                  .subscribe(
                                      data => {                                            
                                          console.log("data json: " + JSON.stringify(data));
                                          this.paymentTypes = data;
                                          
                                          // get statuses
                                          this.paymentService.getStatuses(this.currentDealer.code)
                                              .subscribe(
                                                  data => {                                            
                                                      console.log("data json: " + JSON.stringify(data));
                                                      this.statuses = data;
                                          
                                                      // get list of payments from cache                    
                                                      this.payments = this.paymentService.getPaymentsFromCache();
                                                      this.paymentsDatasource = new MatTableDataSource(this.payments);

                                                      // get list of payments from live                                                        
                                                      this.paymentService.getPayments(this.currentDealer.code).subscribe(
                                                              data => {                     
                                                                  console.log("Payments JSON: " + JSON.stringify(data));
                                                                  this.payments = data;

                                                                  // format SQL timestamp to date
                                                                  for(let payment of this.payments) {
                                                                      if(payment.paymentDate) {                                        
                                                                          payment.paymentDate = moment(Number.parseInt(payment.paymentDate)).format("YYYY-MM-DD");
                                                                          //payment.amount = this.formatAmount(Number.parseInt(payment.amount), true);
                                                                      }         
                                                                  }
                                                                  this.paymentsDatasource = new MatTableDataSource(this.payments);

                                                                  // successful chain of API calls
                                                                  this.session.setloading(false);                                  
                                                              },
                                                              error => {                                    
                                                                  this.alertService.error(this.translation.instant("Failed to get list of payments")); //  + " [" + JSON.stringify(error) + "]");
                                                                  this.session.setloading(false);
                                                                  console.log("list error: " + JSON.stringify(error));                                
                                                              });
                                                      
                                                  },
                                                  error => {
                                                      console.log("edit error: " + JSON.stringify(error));
                                                      this.alertService.error(this.translation.instant("Failed to get list of statuses")); //  + " [" + JSON.stringify(error) + "]");
                                                      console.log("edit error json: " + JSON.stringify(error));
                                                      this.session.setloading(false);
                                                      return;
                                                  });                                 
                                      },
                                      error => {
                                          console.log("edit error: " + JSON.stringify(error));
                                          this.alertService.error(this.translation.instant("Failed to get list of payment types")); //  + " [" + JSON.stringify(error) + "]");
                                          console.log("edit error json: " + JSON.stringify(error));
                                          this.session.setloading(false);
                                          return;
                                      });
                          },
                          error => {                                                        
                              this.alertService.error(this.translation.instant("Failed to get current dealer detail")); //  + " [" + JSON.stringify(error) + "]");
                              console.log("error json: " + JSON.stringify(error));
                              this.session.setloading(false);
                              return;
                          });

                  break;

              case "view" :

                  this.session.setloading(true);

                  this.myForm.disable();
                  
                  // get current dealer
                  this.dealerService.getCurrentDealer(this.session.getSessionUser().username)
                      .subscribe(
                          data => {                     
                              console.log("current dealer: " + JSON.stringify(data));
                              this.currentDealer = data;                                                                                             

                              // get payment detail
                              this.paymentService.getPayment(this.currentDealer.code, this.paymentNumber)
                                  .subscribe(
                                      data => {
                                          console.log("view data: " + data);
                                          console.log("view data json: " + JSON.stringify(data));
                                          this.paymentDetail = data;                                           
                                          
                                          if(this.paymentDetail.paymentDate)
                                              this.paymentDetail.paymentDate=moment(Number.parseInt(this.paymentDetail.paymentDate)).format("YYYY-MM-DD");

                                          // TODO: Setting to current dealer customer name until such time as name is returned from API
                                          if(!this.paymentDetail.clientName)
                                              this.paymentDetail.clientName = this.currentDealer.customerAccount.customerName;

                                          this.paymentDetail.amount = this.formatAmount(Number.parseInt(this.paymentDetail.amount), true);
                                          console.log('model json: ' + JSON.stringify(this.paymentDetail)); 

                                          this.session.setloading(false);
                                      },
                                      error => {
                                          console.log("edit error: " + JSON.stringify(error));
                                          this.alertService.error(this.translation.instant("Failed to get payment detail")); //  + " [" + JSON.stringify(error) + "]");
                                          console.log("edit error json: " + JSON.stringify(error));
                                          this.session.setloading(false);
                                          return;
                                      });
                          },
                          error => {                                                        
                              this.alertService.error(this.translation.instant("Failed to get current dealer detail")); //  + " [" + JSON.stringify(error) + "]");
                              console.log("error json: " + JSON.stringify(error));
                              this.session.setloading(false);
                              return;
                          });
                  
                  break;
      }
  }

  clickDelete(payment : String) {
      this.router.navigate(['/payment/delete', payment]);
  }

  clickEdit() {
      
  }

  clickAdd() {
      this.router.navigate(['/payment/add']);
  }

  clickBack() {
    this.router.navigate(['/payment/list']);
  }

  onPaymentSelect(paymentNumber) {        
      this.router.navigate(['/payment/view', paymentNumber]);
  }    
  
  private billingAddressContainerCompressed=true;
  toggleBillingAddressContainerCompressed() {
      this.billingAddressContainerCompressed=this.billingAddressContainerCompressed ? false : true;
  }

  private shippingAddressContainerCompressed=true;
  toggleShippingAddressContainerCompressed() {
      this.shippingAddressContainerCompressed=this.shippingAddressContainerCompressed ? false : true;
  }

  /*
  updateSelectedCurrency(event) {        
      for(let currency of this.currencies['all'] ) {
          if(currency['code']===event.value)
              this.selectedCurrencySymbol==currency['symbol'];
      }
      if(this.model.paymentLines.length)
          this.recalculatePayment();
  }*/

  formatAmount(amount : number, addCurrencySymbol? : boolean) : string {
      if(addCurrencySymbol)        
          return this.utilityService.formatAmount(amount, this.defaultCurrencyCode, (this.prefixCurrencySymbol==='true' || this.prefixCurrencySymbol==='yes') ? true : false);
      else
          return this.utilityService.formatAmount(amount);
  }

  private downloadingReceipt=false;
  downloadReceipt() {
      this.session.setloading(true);
      this.paymentService.downloadReceipt(this.currentDealer.code, this.paymentDetail.paymentNumber).subscribe
          ((response:any) => {
              this.session.setloading(true);
              let file = response.blob();
              filesaver.saveAs(file,this.paymentDetail.paymentNumber+'_rec.pdf');
          },
          error => {              
              this.alertService.error(this.translation.instant("Failed to download receipt")); //  + " [" + JSON.stringify(error) + "]");
              console.log("error json: " + JSON.stringify(error));
              this.session.setloading(true);
              return;
          });
      return false;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.paymentsDatasource.filter = filterValue.trim().toLowerCase();
  }
}
