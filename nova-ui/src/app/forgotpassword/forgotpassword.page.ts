import { Component, OnInit } from '@angular/core';
import { AlertService } from '../alert.service';
import { ConfigService } from '../config.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpBackend } from '@angular/common/http';
import { SessionService } from '../session.service';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.page.html',
  styleUrls: ['./forgotpassword.page.scss'],
})
export class ForgotpasswordPage {

  NovaServiceApiUrl: string;
  ResetPasswordApiPath: string;

  loading: boolean = false;

  model: any = {};

  passwordResetToken = undefined;

  private http: HttpClient;

  constructor(private alertService: AlertService,
              private config: ConfigService,              
              private router: Router,
              private route: ActivatedRoute,
              public translation: TranslateService,
              private backEndHttp: HttpBackend,
              private session: SessionService) {
    
    this.NovaServiceApiUrl = config.getConfig("novaApiUrl");
    this.ResetPasswordApiPath = config.getConfig("novaApiPasswordResetPath");    
    route.params.subscribe(params => { this.passwordResetToken = params['passwordResetToken']; });    
    this.http = new HttpClient(backEndHttp);
  }

  clickCancel() {
    this.router.navigate(['/login']);
  }
  resetPassword() {

    if(this.model.username<='') return;

    this.session.setloading(true);

    // set the body
    let body = JSON.stringify({ username: this.model.username });

    // invoke Nova Service to request a password reset
    this.http.post(this.NovaServiceApiUrl + this.ResetPasswordApiPath, body, { headers: {'Content-Type':'application/json'} })
      .subscribe((response) => {
          this.session.setloading(false);
          this.alertService.successKeepAfterNavigation("Password reset received, a password reset link will be emailed to your registered address");
          this.router.navigate(['/login']);
        },
        error => {          
          this.alertService.error("Password reset request failed");
          this.session.setloading(false);
        }
      );
  }

  confirmResetPassword() {
    this.session.setloading(true);

    // set the body
    let body = JSON.stringify({ username: this.model.username });

    // invoke Nova Service to confirm a password reset
    this.http.post(this.NovaServiceApiUrl + this.ResetPasswordApiPath + "/" + this.passwordResetToken, body, { headers: {'Content-Type':'application/json'} })
      .subscribe((response: Response) => {
        this.session.setloading(false);
          this.alertService.successKeepAfterNavigation("Your password has been reset, a new password has been generated and emailed to your registered address");
          this.router.navigate(['/login']);
        },
        error => {
          this.alertService.error("Password reset confirmation failed");
          this.session.setloading(false);
        }
      );
  }
}

