import { Injectable } from '@angular/core';
import { SessionService } from './session.service';
import { ConfigService } from './config.service';
import { AlertService } from './alert.service';
import { UtilityService } from './utility.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from './_models/user';
import { Observable } from 'rxjs';
import { Role } from './_models/role';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  NovaServiceApiUrl: string;
  UserAccountManagementCurrentUserApiPath: string;
  UserAccountManagementUsersApiPath: string;
  UserAccountManagementRolesApiPath: string;
  HttpRequestTimeout: number;

  constructor(private session: SessionService, private config: ConfigService, private http: HttpClient, private alert: AlertService, private utlity: UtilityService) {
    this.NovaServiceApiUrl = config.getConfig("novaApiUrl");
    this.UserAccountManagementCurrentUserApiPath = config.getConfig("novaApiUserManagementCurrentUserPath");
    this.UserAccountManagementUsersApiPath = config.getConfig("novaApiUserManagementUsersPath");
    this.UserAccountManagementRolesApiPath = config.getConfig("novaApiUserManagementRolesPath");
    this.HttpRequestTimeout = config.getConfig("httpRequestTimeout");

    // clear cache when session destroyed
    session.getSessionStatus().subscribe((data) => {
      if (data == 'destroyed') {
        this._getCurrentUser = null;
        this._getUser = null;
        this._getUserRoles = null;
        this._getUsers = null;
        this._getUsersForOrganization = null;
        this.currentOrganization = null;
        this.currentUserName = null;
        this.currentUserOrganization = null;
      }
    });
  }

  private _getUsersForOrganization: User[] = [];
  private currentOrganization: String = "";
  getUsersForOrganization(organization: string): Observable<User[]> {
    return Observable.create(observer => {
      this.http.get(this.NovaServiceApiUrl + this.UserAccountManagementUsersApiPath + "?where=%7B%22organization%22:%22" + organization + "%22%7D", { observe: 'response' })//.timeout(this.HttpRequestTimeout)
        //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
        //.map((response: Response) => response.json())
        .subscribe((data: any) => {
          this._getUsersForOrganization = data.body;
          this.currentOrganization = organization;
          observer.next(this._getUsersForOrganization);
          observer.complete();
        }, (err) => {
          console.log("err: " + JSON.stringify(err));
          observer.error(err);
        })
    });
  }
  getUsersForOrganizationFromCache(organization: string): User[] {
    if (organization == this.currentOrganization)
      return this._getUsersForOrganization;
    else
      return [] as User[];
  }

  private _getUsers: User[] = [];
  getUsers(): Observable<User[]> {
    return Observable.create(observer => {
      this.http.get(this.NovaServiceApiUrl + this.UserAccountManagementUsersApiPath, { observe: 'response' })//.timeout(this.HttpRequestTimeout)
        //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
        //.map((response: Response) => response.json())
        .subscribe(
          (data: any) => {
            this._getUsers = data.body;
            observer.next(this._getUsers);
            observer.complete();
          },
          (err) => {
            console.log("err: " + JSON.stringify(err));
            observer.error(err);
          })
    });
  }
  getUsersFromCache(): User[] {
    if (this._getUsers)
      return this._getUsers;
    else
      return [] as User[];
  }

  private currentUserName: String = "";
  private currentUserOrganization: String = "";
  private _getUser: User;
  getUser(organization: String, username: String): Observable<User> {
    return Observable.create(observer => {
      this.http.get(this.NovaServiceApiUrl + this.UserAccountManagementUsersApiPath + "/" + organization + "/" + username, { observe: 'response' })//.timeout(this.HttpRequestTimeout)
        //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
        //.map((response: Response) => response.json())
        .subscribe(
          (data: any) => {
            this._getUser = data.body
            this.currentUserName = username;
            this.currentUserOrganization = organization;
            observer.next(this._getUser);
            observer.complete();
          }, (err) => {
            console.log("err: " + JSON.stringify(err));
            observer.error(err);
          })
    })
  }

  getUserFromCache(organization: String, username: String): User {
    if (organization == this.currentUserOrganization && username == this.currentUserName)
      return this._getUser;
    else
      return new User();
  }

  private _getUserRoles: Role[] = [];
  getUserRoles() {
    // always return cache if available
    if (!this._getUserRoles || !this._getUserRoles.length) {
      return Observable.create(observer => {
        // invoke Nova Service to retrieve user roles
        this.http.get(this.NovaServiceApiUrl + this.UserAccountManagementRolesApiPath, { observe: 'response' })//.timeout(this.HttpRequestTimeout)
          //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
          //.map((response: Response) => response.json())
          .subscribe((data: any) => {
            this._getUserRoles = data.body;
            observer.next(this._getUserRoles);
            observer.complete();
          }, (err) => {
            console.log("err: " + JSON.stringify(err));
            observer.error(err);
          })
      });
    } else {
      return Observable.create(observer => { observer.next(this._getUserRoles); observer.complete(); });
    }
  }

  private _getCurrentUser: User;
  getCurrentUser(): Observable<User> {
    return Observable.create(observer => {
      this.http.get(this.NovaServiceApiUrl + this.UserAccountManagementCurrentUserApiPath, { observe: 'response' })//.timeout(this.HttpRequestTimeout)
        //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
        //.map((response: Response) => response.json())
        .subscribe((data: any) => {
          this._getCurrentUser = data.body
          observer.next(this._getCurrentUser);
          observer.complete();
        }, (err) => {
          console.log("err: " + JSON.stringify(err));
          observer.error(err);
        })
    });
  }
  getCurrentUserFromCache(): User {
    if (this._getCurrentUser)
      return this._getCurrentUser;
    else
      return new User();
  }

  createUser(user: User) {
    // invoke Nova Service to create a new user
    return this.http.post(this.NovaServiceApiUrl + this.UserAccountManagementUsersApiPath + "/" + user.organization, user, { observe: 'response' })//.timeout(this.HttpRequestTimeout).catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) });
  }

  updateUser(user: User) {
    // invoke Nova Service to update an existing user
    return this.http.put(this.NovaServiceApiUrl + this.UserAccountManagementUsersApiPath + "/" + user.organization + "/" + user.username, user, { observe: 'response' })//.timeout(this.HttpRequestTimeout).catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) });
  }

  deleteUser(organization: String, username: String) {
    // invoke Nova Service to delete a user
    return this.http.delete(this.NovaServiceApiUrl + this.UserAccountManagementUsersApiPath + "/" + organization + "/" + username, { observe: 'response' })//.timeout(this.HttpRequestTimeout).catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) });
  }
}