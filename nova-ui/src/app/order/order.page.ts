import { Component, OnInit, Inject } from '@angular/core';

import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { AlertService } from '../alert.service';
import { OrderService } from '../order.service';
import { QuotationService } from '../quotation.service';
import { DealerService } from '../dealer.service';
import { SessionService } from '../session.service';
import { UtilityService } from '../utility.service';
import { ConfigService } from '../config.service';
import { Order } from '../_models/order';
import { OrderDetail } from '../_models/orderdetail';
import { PaymentType } from '../_models/paymenttype';
import { Warehouse } from '../_models/warehouse';
import { Product } from '../_models/product';
import { Status } from '../_models/status';
import { PriceRequest, LineRequests, LineRequest } from '../_models/pricerequest';
import { PriceResponse, PriceResponseDetails, LineResponses, LineResponse } from '../_models/priceresponse';
import { Dealer } from '../_models/dealer';
import { FormArray, FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Location } from '@angular/common';

import moment, { now } from 'moment/moment';
import { TranslateService } from '@ngx-translate/core';

import filesaver from 'filesaver.js';

import currencies from 'country-data';
import { ConfirmDialogComponent } from '../shared-components/confirm-dialog/confirm-dialog.component';
import { MatDialog, MatTableDataSource, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Payment } from '../_models/payment';
import { inject } from '@angular/core/testing';
import { PaymentService } from '../payment.service';
import { Observable, Subscription } from 'rxjs';
import { interval } from 'rxjs';

@Component({
  selector: 'app-order',
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.scss'],
})
export class OrderPage implements OnInit {

  myForm: FormGroup;
  formsubmitted: boolean = false;
  model: Order = null;
  mode: string = "";
  orderNumber: string;
  orders: Order[];
  ordersDatasource;
  order: Order;
  orderDetail: OrderDetail;

  displayDialog: boolean = false;

  statuses: Status[] = [];

  warehouses: Warehouse[];

  dateFilter: any;

  paymentTypes: PaymentType[];

  issuingWarehouses: Warehouse[];

  selectedWarehouse: string;
  products: Product[] = [];

  productSelectAvailable: number;

  selectedProduct: Product;
  selectedProductQuantity: number;

  notenoughstock: boolean = false;

  currentDealer: Dealer;

  pricingorder = false;

  defaultCurrencyCode: string;
  prefixCurrencySymbol: string;

  acceptingOrder: boolean = false;

  returnurl: string = "";

  displayedColumns: string[] = ['customerName', 'orderNumber', 'quotationNumber', 'createdDate', 'dispatchedDate', 'warehouse', 'status'];

  acceptedDisplayedColumns = ['product', 'serialStart', 'serialEnd', 'quantity', 'amount'];

  navigationSubscription;

  displayedPaymentsColumns: string[] = ['paymentNumber', 'paymentDate', 'paymentType', 'amount'];
  payments: Payment[] = [];
  paymentsDatasource;

  totalPayments = 0;
  mobileMoneyPaymentTimeoutMS;

  countryCodes = [];

  constructor(
    private router: Router,
    private orderService: OrderService,
    private quotationService: QuotationService,
    private dealerService: DealerService,
    private alertService: AlertService,
    private route: ActivatedRoute,
    private session: SessionService,
    private utilityService: UtilityService,
    private config: ConfigService,
    private _fb: FormBuilder,
    public translation: TranslateService,
    private location: Location,
    public dialog: MatDialog) {

    route.params.subscribe(params => {
      this.orderNumber = params['ordernumber'];
    });
    this.mode = this.router.url.split("/")[2]; // list/add/edit/delete/view

    this.returnurl = "";
    this.defaultCurrencyCode = this.config.getConfig("uiCurrencyCode");
    this.prefixCurrencySymbol = this.config.getConfig("uiAmountCurrencySymbolPrefix");
    this.mobileMoneyPaymentTimeoutMS = this.config.getConfig("mobileMoneyPaymentTimeoutMS");
    this.countryCodes = this.config.getConfig("uiCountryCodes").split(",");

    // subscribe to the router events - storing the subscription so
    // we can unsubscribe later. 
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        if (this.mode === 'list' && this.router.url.indexOf('order/list') > 0)
          this.updateListData();
      }
    });
  }

  updateListData() {
    this.session.setloading(true);

    // get current dealer    
    this.dealerService.getCurrentDealer(this.session.getSessionUser().username)
      .subscribe(
        data => {
          console.log("current dealer: " + JSON.stringify(data));
          this.currentDealer = data;

          // get warehouses
          this.warehouses = [];
          //this.warehouses.push({label: this.translation.instant("All"), value: null});                                
          this.orderService.getWarehouses(this.currentDealer.code)
            .subscribe(
              data => {

                this.warehouses = data;

                /*
                console.log('model json: ' + JSON.stringify(this.issuingWarehouses));                                                
                for(let warehouse of this.issuingWarehouses) {
                    this.warehouses.push({label: warehouse.name, value: warehouse.name});
                }
                */
                // get statuses
                /*
                this.statusesSelect = [];
                this.statusesSelect.push({label: this.translation.instant("All"), value: null}); */
                this.statuses = [];
                this.orderService.getStatuses(this.currentDealer.code)
                  .subscribe(
                    data => {

                      this.statuses = data;

                      /*
                      for(let status of this.statuses) {
                          this.statusesSelect.push({label: status.name, value: status.name});
                      }
                      */

                      // get list of orders from cache                    
                      this.orders = this.orderService.getOrdersFromCache();
                      this.ordersDatasource = new MatTableDataSource(this.orders);

                      // get list of orders from live                                                        
                      this.orderService.getOrders(this.currentDealer.code).subscribe(
                        data => {
                          console.log("Orders JSON: " + JSON.stringify(data));
                          this.orders = data;

                          // format SQL timestamp to date
                          for (let order of this.orders) {
                            if (order.createdDate) {
                              order.createdDate = moment(Number.parseInt(order.createdDate)).format("YYYY-MM-DD");
                            }
                            if (order.dispatchedDate) {
                              order.dispatchedDate = moment(Number.parseInt(order.dispatchedDate)).format("YYYY-MM-DD");
                            }
                          }
                          this.ordersDatasource = new MatTableDataSource(this.orders);

                          // successful chain of API calls
                          this.session.setloading(false);
                        },
                        error => {
                          this.alertService.error(this.translation.instant("Failed to get list of orders")); //  + " [" + JSON.stringify(error) + "]");
                          this.session.setloading(false);
                          console.log("list error: " + JSON.stringify(error));
                        });

                    },
                    error => {
                      console.log("edit error: " + JSON.stringify(error));
                      this.alertService.error(this.translation.instant("Failed to get list of statuses")); //  + " [" + JSON.stringify(error) + "]");
                      console.log("edit error json: " + JSON.stringify(error));
                      this.session.setloading(false);
                      return;
                    });
              },
              error => {
                console.log("edit error: " + JSON.stringify(error));
                this.alertService.error(this.translation.instant("Failed to get list of warehouses")); //  + " [" + JSON.stringify(error) + "]");
                console.log("edit error json: " + JSON.stringify(error));
                this.session.setloading(false);
                return;
              });
        },
        error => {
          this.alertService.error(this.translation.instant("Failed to get current dealer detail")); //  + " [" + JSON.stringify(error) + "]");
          console.log("error json: " + JSON.stringify(error));
        });
  }

  ngOnInit() {

    switch (this.mode) {

      case "list":

        this.updateListData();

        break;

      case "view":
        this.session.setloading(true);
        // get current dealer    
        this.dealerService.getCurrentDealer(this.session.getSessionUser().username)
          .subscribe(
            data => {
              console.log("current dealer: " + JSON.stringify(data));
              this.currentDealer = data;

              // get order detail
              this.orderService.getOrder(this.currentDealer.code, this.orderNumber)
                .subscribe(
                  data => {
                    console.log("view data: " + data);
                    console.log("view data json: " + JSON.stringify(data));
                    this.orderDetail = data;
                    console.log('model json: ' + JSON.stringify(this.model));

                    this.orderDetail.createdDate = moment(Number.parseInt(this.orderDetail.createdDate)).format("YYYY-MM-DD");
                    if (this.orderDetail.dispatchedDate)
                      this.orderDetail.dispatchedDate = moment(Number.parseInt(this.orderDetail.dispatchedDate)).format("YYYY-MM-DD");


                    this.orderService.getPayments(this.currentDealer.code, this.orderNumber)
                      .subscribe(
                        data => {
                          this.payments = data;
                          this.totalPayments = 0;
                          for (let payment of this.payments) {
                            payment.paymentDate=moment(Number.parseInt(payment.paymentDate)).format("YYYY-MM-DD");
                            this.totalPayments = this.totalPayments + payment.amount;
                          }
                          this.paymentsDatasource = new MatTableDataSource(this.payments);
                          this.session.setloading(false);
                        },
                        error => {
                          this.session.setloading(false);
                        }
                      );                    
                  },
                  error => {
                    console.log("edit error: " + JSON.stringify(error));
                    this.alertService.error(this.translation.instant("Failed to get order detail")); //  + " [" + JSON.stringify(error) + "]");
                    console.log("edit error json: " + JSON.stringify(error));
                    this.session.setloading(false);
                    return;
                  });
            },
            error => {
              this.session.setloading(false);
              this.alertService.error(this.translation.instant("Failed to get current dealer detail")); //  + " [" + JSON.stringify(error) + "]");
              console.log("error json: " + JSON.stringify(error));
            });

        break;
    }

  }

  clickDelete(order: String) {
    this.router.navigate(['/order/delete', order]);
  }

  clickEdit() {
  }

  clickAdd() {
    this.router.navigate(['/order/add']);
  }

  clickBack() {
    this.router.navigate(['/order/list']);
  }

  onOrderSelect(orderNumber) {
    this.router.navigate(['/order/view', orderNumber]);
  }

  private billingAddressContainerCompressed = true;
  toggleBillingAddressContainerCompressed() {
    this.billingAddressContainerCompressed = this.billingAddressContainerCompressed ? false : true;
  }

  private shippingAddressContainerCompressed = true;
  toggleShippingAddressContainerCompressed() {
    this.shippingAddressContainerCompressed = this.shippingAddressContainerCompressed ? false : true;
  }

  /*
  updateSelectedCurrency(event) {        
      for(let currency of this.currencies['all'] ) {
          if(currency['code']===event.value)
              this.selectedCurrencySymbol==currency['symbol'];
      }
      if(this.model.orderLines.length)
          this.recalculateOrder();
  }*/

  formatAmount(amount: number, addCurrencySymbol?: boolean): string {
    if (addCurrencySymbol)
      return this.utilityService.formatAmount(amount, this.defaultCurrencyCode, (this.prefixCurrencySymbol === 'true' || this.prefixCurrencySymbol === 'yes') ? true : false);
    else
      return this.utilityService.formatAmount(amount);
  }

  private downloadingProforma = false;
  downloadProforma(event) {
    this.session.setloading(true);
    this.quotationService.downloadProforma(this.currentDealer.code, this.orderDetail.quotationNumber).subscribe
      ((response: any) => {
        this.session.setloading(false);
        let file = response.blob();
        filesaver.saveAs(file, this.orderDetail.quotationNumber + '_profinv.pdf');
      },
        error => {
          this.session.setloading(false);
          console.log("error: " + JSON.stringify(error));
          this.alertService.error(this.translation.instant("Failed to download proforma invoice")); //  + " [" + JSON.stringify(error) + "]");
          console.log("error json: " + JSON.stringify(error));
        });
    event.stopPropagation();
  }

  refreshView() {

    console.log("MODE: " + this.mode);
    this.session.setloading(true);

    if (this.mode === 'view') {
      // get order detail
      this.orderService.getOrder(this.currentDealer.code, this.orderNumber)
        .subscribe(
          data => {
            console.log("view data: " + data);
            console.log("view data json: " + JSON.stringify(data));
            this.orderDetail = data;
            console.log('model json: ' + JSON.stringify(this.model));
            this.orderService.getPayments(this.currentDealer.code, this.orderNumber)
                      .subscribe(
                        data => {
                          this.payments = data;
                          this.session.setloading(false);
                        },
                        error => {
                          this.session.setloading(false);
                        }
                      );    
          },
          error => {
            console.log("edit error: " + JSON.stringify(error));
            this.alertService.error(this.translation.instant("Failed to get order detail")); //  + " [" + JSON.stringify(error) + "]");
            console.log("edit error json: " + JSON.stringify(error));
            this.session.setloading(false);
            return;
          });
    }
    else
      // get list of orders from live                                                        
      this.orderService.getOrders(this.currentDealer.code).subscribe(
        data => {
          console.log("Orders JSON: " + JSON.stringify(data));
          this.orders = data;

          // format SQL timestamp to date
          for (let order of this.orders) {
            if (order.createdDate) {
              order.createdDate = moment(Number.parseInt(order.createdDate)).format("YYYY-MM-DD");
            }
            if (order.dispatchedDate) {
              order.dispatchedDate = moment(Number.parseInt(order.dispatchedDate)).format("YYYY-MM-DD");
            }
          }

          this.orderService.getPayments(this.currentDealer.code, this.orderNumber)
                      .subscribe(
                        data => {
                          this.payments = data;
                          this.session.setloading(false);
                        },
                        error => {
                          this.session.setloading(false);
                        }
                      );    
        },
        error => {
          this.alertService.error(this.translation.instant("Failed to get list of orders")); //  + " [" + JSON.stringify(error) + "]");
          this.session.setloading(false);
          console.log("list error: " + JSON.stringify(error));
        });

  }

  clickCancel(salesOrderNumber?: string, event?) {

    if (event) event.stopPropagation();

    //confirm
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',      
      data: { title: 'Cancel', content: 'Cancel this Order?' }
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result === true) {

        // cancel order    
        this.session.setloading(true);
        this.orderService.cancelOrder(this.currentDealer.code, salesOrderNumber ? salesOrderNumber : this.orderNumber)
          .subscribe(
            data => {
              this.session.setloading(false);
              this.alertService.success(this.translation.instant("Order has been cancelled"));
              this.refreshView();
              this.returnurl = "/account/order/list"; // return to listing if back clicked
            },
            error => {
              console.log("edit error: " + JSON.stringify(error));
              this.alertService.error(this.translation.instant("Failed to cancel order")); //  + " [" + JSON.stringify(error) + "]");
              console.log("edit error json: " + JSON.stringify(error));
              this.session.setloading(false);
            });

      }
    })
  }

  viewDocuments(orderNumber: string) {
    this.router.navigate(['/documents', orderNumber]);
    return false;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.ordersDatasource.filter = filterValue.trim().toLowerCase();
  }

  addMobileMoneyPayment() {
    const dialogRef = this.dialog.open(AddPaymentDialog, {
      width: '450px !important',
      closeOnNavigation: false,
      disableClose: true,
      data: { currentDealer: this.currentDealer, timeout: this.mobileMoneyPaymentTimeoutMS, countryCodes: this.countryCodes, payment: { currencyCode: this.defaultCurrencyCode, orderNumber: this.orderNumber } }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.session.setloading(false);
        this.orderService.getPayments(this.currentDealer.code, this.orderNumber)
          .subscribe(
            data => {
              this.payments = data;
              this.totalPayments = 0;
              for (let payment of this.payments) {
                payment.paymentDate=moment(Number.parseInt(payment.paymentDate)).format("YYYY-MM-DD");
                this.totalPayments = this.totalPayments + payment.amount;
              }
              this.paymentsDatasource = new MatTableDataSource(this.payments);
              this.session.setloading(false);
            },
            error => {
              this.session.setloading(false);
            }
          ); 
      }
    });
  }
}

@Component({
  selector: 'add-payment-dialog',
  templateUrl: 'add-payment-dialog.html',
})
export class AddPaymentDialog {

  public processing = false;
  poller : Subscription;
  countryCodes;
  amountOptions = {};

  constructor(
    public session: SessionService,
    public alertService: AlertService,
    public paymentService: PaymentService,
    public dialogRef: MatDialogRef<AddPaymentDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
      this.countryCodes = data.countryCodes;
      this.amountOptions = { prefix: data.payment.currencyCode + ' ', thousands: ',', decimal: '.' };
    }

  onNoClick(): void {
    if(this.poller) {
      this.poller.unsubscribe();
    }
    this.dialogRef.close(null);
  }

  onSubmitClick(): void {
    if (this.data.payment.amount > 0 && this.data.payment.msisdn > '') {
      this.data.payment.msisdn=this.data.payment.msisdn.replace('+','');
      this.processing = true;
      this.paymentService.makePayment(this.data.currentDealer.code, this.data.payment).subscribe(
        (success: any) => {
          let pollUrl = success.headers.get('Location');
          let start = now();
          this.poller = interval(10000).subscribe(tick => {
            if ((now() - start) > this.data.timeout) { // timed out
              console.error("Timeout");
              this.processing = false;
              this.alertService.error("Request timed out, please try again");
              this.dialogRef.close();
              this.poller.unsubscribe();
            }
            this.paymentService.getPaymentFromURL(pollUrl).subscribe(
              response => {
                if(response.mobileMoneyPayments) {
                  if(response.mobileMoneyPayments.statusId===2) {
                    this.poller.unsubscribe();
                    this.processing = false;
                    this.alertService.error("Mobile Money Payment Failed");
                    this.dialogRef.close();
                  } else if(response.mobileMoneyPayments.statusId===1) {
                    this.poller.unsubscribe();
                    this.alertService.success("Mobile Money Payment Successful");
                    this.processing = false;
                    this.dialogRef.close(true);
                  }
                }
              },
              error => {
                this.poller.unsubscribe();
                console.error(error);
                this.processing = false;
                this.alertService.error(JSON.stringify(error));
                this.dialogRef.close();
              }
            )
          })
        },
        error => {
          this.processing = false;
          this.alertService.error(JSON.stringify(error));
          console.error(error);
        }
      )
    }
  }
}
