import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderPageRoutingModule } from './order-routing.module';

import { OrderPage, AddPaymentDialog } from './order.page';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '../material/material.module';
import { SharedComponentsModule } from '../shared-components/shared-components.module';
import { NgxMatIntlTelInputModule } from 'ngx-mat-intl-tel-input';
import { CurrencyMaskModule } from "ng2-currency-mask";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,    
    TranslateModule,
    MaterialModule,
    ReactiveFormsModule,
    SharedComponentsModule,
    OrderPageRoutingModule,
    NgxMatIntlTelInputModule,
    CurrencyMaskModule,
  ],
  declarations: [OrderPage, AddPaymentDialog],
  entryComponents: [AddPaymentDialog]
})
export class OrderPageModule {}
