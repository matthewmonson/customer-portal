import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  private config: Object = null;

  constructor(private http: HttpClient) {        
  }

  load() {
      /*if(environment.production) {*/
        return new Promise((resolve, reject) => {
          this.http.get(environment.production ? "./assets/production.config.json" : "./assets/development.config.json")
            .subscribe( response => {
              this.config = response;
              resolve(true);
          });
        });
        /*
        return new Promise((resolve, reject) => {
          this.http.get("./config")
            .subscribe( (response : HttpResponse<string>) => {                                
                this.config = JSON.parse(response.body);
                resolve(true);
            });
        });
      } else {
        return new Promise((resolve, reject) => {
          this.http.get("./assets/development.config.json")
            .subscribe( response => {
              this.config = response;
              resolve(true);
          });
        });
      }*/
  }

  /**
   * Use to get a configuration parameter in the environment config file
   */
  public getConfig(key: any) {
      return this.config[key];
  }

  /**
   * Use to return whether running production or development
   */
  public getEnv() {
      if(environment.production)
        return "production";
      else
        return "development";
  }
}