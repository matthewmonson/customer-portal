import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse, HttpEvent } from "@angular/common/http";
import { Observable, throwError, defer, from } from 'rxjs';
import { catchError, mergeMap, takeUntil  } from 'rxjs/operators';
import { SessionService } from './session.service';
import { AlertService } from './alert.service';
import { Router, ActivatedRouteSnapshot } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class RequestInterceptorService implements HttpInterceptor {

  constructor(private session: SessionService, private alertService : AlertService, private router : Router, private translation : TranslateService) {
  }

  requestAddToken(req) {
    return this.session.getSessionToken() ? req.clone({ setHeaders: { 'X-Casanova': this.session.getSessionToken() } }) : req.clone();
  }

  /**
   * Intercept request to authorize request
   * @param req original request
   * @param next next
   */
  intercept<T>(req: HttpRequest<T>, next: HttpHandler): Observable<HttpEvent<T>> {
      // Authorization handler observable
      const authHandle = defer(() => {
        // Add token to request
        const authorizedReq = this.requestAddToken(req);
        // Execute
        return next.handle(authorizedReq).pipe(takeUntil(this.session.onCancelPendingRequests()));
      });

      return authHandle.pipe(
        catchError((requestError, retryRequest) => {
          if (requestError instanceof HttpErrorResponse && requestError.status === 401 && this.session.getSessionToken()) {
            // test the session token
            this.session.testSessionURI(this.session.getSessionToken())
            .subscribe(
              (response: any) => {
                // session is still valid so throw the error
                return throwError(requestError);
              },
              error => {
                // session is not valid anymore so route back to login
                console.error(error);
                this.session.destroySessionUser();
                this.alertService.error(this.translation.instant("Session has expired, please re-login"));
                this.router.navigate(['/login'] /*, { queryParams: { returnurl: this.router.routerState.snapshot.url }}*/ );
              });
          } else
            return throwError(requestError);
        })
      );
  }
}
