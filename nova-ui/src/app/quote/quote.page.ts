import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { AlertService } from '../alert.service';
import { QuotationService } from '../quotation.service';
import { OrderService } from '../order.service';
import { DealerService } from '../dealer.service';
import { SessionService } from '../session.service';
import { UtilityService } from '../utility.service';
import { ConfigService } from '../config.service';
import { QuotationUI, QuotationUILine, Quotation, QuotationDetails, QuotationDetailLine } from '../_models/quotation';

import { Warehouse } from '../_models/warehouse';
import { Product } from '../_models/product';
import { Status } from '../_models/status';
import { PriceRequest, LineRequests, LineRequest } from '../_models/pricerequest';
import { PriceResponse, PriceResponseDetails, LineResponses, LineResponse } from '../_models/priceresponse';
import { Dealer } from '../_models/dealer';
import { FormArray, FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import moment from 'moment/moment';
import { TranslateService } from '@ngx-translate/core';
import { PaymentType } from '../_models/orderdetail';

import filesaver from 'filesaver.js';

//var countries        = require('country-data');
import currencies from 'country-data';
import { Observable } from 'rxjs';
import { startWith, map, filter } from 'rxjs/operators';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { ConfirmDialogComponent } from '../shared-components/confirm-dialog/confirm-dialog.component';

@Component({
    selector: 'quotation',
    templateUrl: './quote.page.html',
    styleUrls: ['./quote.page.scss']
})
export class QuotePage implements OnInit {

    @ViewChild('productsearch', { static: false }) productSearchElement: ElementRef;

    @ViewChild('productselectquantity', { static: false }) productSelectQuantityElement: ElementRef;

    myForm: FormGroup;
    formsubmitted: boolean = false;
    model: QuotationUI = null;
    mode: string = "";
    quotationNumber: string;
    quotations: Quotation[];
    quotationsDatasource: any;
    quotation: Quotation;

    quotationDetails: QuotationDetails;

    displayDialog: boolean = false;

    statuses: Status[] = [];

    statusesSelect: any[];

    warehouses: any[];

    dateFilter: any;

    paymentTypes: PaymentType[];
    paymentTypeSelect: any[];

    issuingWarehouses: Warehouse[] = [];
    issuingWarehouseSelect: any[];

    selectedWarehouse: string;
    products: Product[] = [];

    productSelect: any[];
    productSelectAvailable: number;

    selectedProduct: Product;
    selectedProductQuantity: number;

    notenoughstock: boolean = false;

    currentDealer: Dealer;

    pricingquote = false;

    currencySelect: any[];
    selectedCurrency: string;
    selectedCurrencySymbol: string;

    defaultCurrencyCode: string;
    prefixCurrencySymbol: string;

    acceptingQuote: boolean = false;

    returnurl: string = "";

    displayedColumns: string[] = ['quotationNumber', 'createdDate', 'warehouse', 'status', 'orderNumber'];

    quoteDisplayedColumns: string[] = ['product', 'available', 'quantity', 'grossAmount', 'taxAmount', 'discountAmount', 'netAmount'];

    filteredWarehouses: Observable<Warehouse[]>;

    filteredProducts: Observable<Product[]>;

    navigationSubscription;

    constructor(
        private router: Router,
        private quotationService: QuotationService,
        private orderService: OrderService,
        private dealerService: DealerService,
        private alertService: AlertService,
        private route: ActivatedRoute,
        private session: SessionService,
        private utility: UtilityService,
        private config: ConfigService,
        private _fb: FormBuilder,
        public translation: TranslateService,
        private location: Location,
        public dialog: MatDialog) {

        route.params.subscribe(params => {
            this.quotationNumber = params['quotationnumber'];
        });
        this.mode = this.router.url.split("/")[2]; // list/add/edit/delete/view

        // build this components form group
        this.myForm = this._fb.group({
            'paymenttype': ['', Validators.required],
            'issuingwarehouse': ['', Validators.required],
            'currency': ['', Validators.required],
            'product': [''],
            'quantity': ['']
        });

        this.returnurl = "";
        this.defaultCurrencyCode = this.config.getConfig("uiCurrencyCode");
        this.prefixCurrencySymbol = this.config.getConfig("uiAmountCurrencySymbolPrefix");

        this.filteredWarehouses = this.myForm.controls['issuingwarehouse'].valueChanges
            .pipe(
                startWith(''),
                map((value: String) => this._filterWarehouses(value))
            );

        this.filteredProducts = this.myForm.controls['product'].valueChanges
            .pipe(
                startWith(''),
                map(value => this._filterProducts(value))
            );

        // subscribe to the router events - storing the subscription so
        // we can unsubscribe later. 
        this.navigationSubscription = this.router.events.subscribe((e: any) => {
            // If it is a NavigationEnd event re-initalise the component
            if (e instanceof NavigationEnd) {
                if (this.mode === 'list' && this.router.url.indexOf('quote/list') > 0)
                    this.updateListData();
            }
        });
    }

    updateListData() {
        this.session.setloading(true);

        // get current dealer
        this.dealerService.getCurrentDealer(this.session.getSessionUser().username)
            .subscribe(
                data => {
                    console.log("current dealer: " + JSON.stringify(data));
                    this.currentDealer = data;

                    // get warehouses
                    this.warehouses = [];
                    this.warehouses.push({ label: this.translation.instant("All"), value: null });
                    this.orderService.getWarehouses(this.currentDealer.code)
                        .subscribe(
                            data => {
                                console.log("data json: " + JSON.stringify(data));
                                this.issuingWarehouses = data;
                                console.log('model json: ' + JSON.stringify(this.issuingWarehouses));
                                for (let warehouse of this.issuingWarehouses) {
                                    this.warehouses.push({ label: warehouse.name, value: warehouse.name });
                                }

                                // get statuses
                                this.statusesSelect = [];
                                this.statusesSelect.push({ label: this.translation.instant("All"), value: null });
                                this.quotationService.getStatuses(this.currentDealer.code)
                                    .subscribe(
                                        data => {
                                            console.log("data json: " + JSON.stringify(data));
                                            this.statuses = data;
                                            console.log('model json: ' + JSON.stringify(this.statuses));
                                            for (let status of this.statuses) {
                                                this.statusesSelect.push({ label: status.name, value: status.name });
                                            }

                                            // get list of quotations from cache                    
                                            this.quotations = this.quotationService.getQuotationsFromCache();
                                            this.quotationsDatasource = new MatTableDataSource(this.quotations);

                                            // get list of quotations from live                                                        
                                            this.quotationService.getQuotations(this.currentDealer.code).subscribe(
                                                data => {
                                                    console.log("Quotations JSON: " + JSON.stringify(data));
                                                    this.quotations = data;

                                                    // format SQL timestamp to date
                                                    for (let quotation of this.quotations) {
                                                        if (quotation.createdDate) {
                                                            quotation.createdDate = moment(Number.parseInt(quotation.createdDate)).format("YYYY-MM-DD");
                                                        }
                                                    }
                                                    this.quotationsDatasource = new MatTableDataSource(this.quotations);
                                                    // successful chain of API calls
                                                    this.session.setloading(false);
                                                },
                                                error => {
                                                    this.alertService.error(this.translation.instant("Failed to get list of quotations")); //  + " [" + JSON.stringify(error) + "]");
                                                    this.session.setloading(false);
                                                    console.log("list error: " + JSON.stringify(error));
                                                });

                                        },
                                        error => {
                                            console.log("edit error: " + JSON.stringify(error));
                                            this.alertService.error(this.translation.instant("Failed to get list of statuses")); //  + " [" + JSON.stringify(error) + "]");
                                            console.log("edit error json: " + JSON.stringify(error));
                                            this.session.setloading(false);
                                            return;
                                        });
                            },
                            error => {
                                console.log("edit error: " + JSON.stringify(error));
                                this.alertService.error(this.translation.instant("Failed to get list of warehouses")); //  + " [" + JSON.stringify(error) + "]");
                                console.log("edit error json: " + JSON.stringify(error));
                                this.session.setloading(false);
                                return;
                            });
                },
                error => {
                    this.alertService.error(this.translation.instant("Failed to get current dealer detail")); //  + " [" + JSON.stringify(error) + "]");
                    console.log("error json: " + JSON.stringify(error));
                    this.session.setloading(false);
                    return;
                });
    }

    ngOnInit() {

        switch (this.mode) {

            case "list":

                this.updateListData();

                break;

            case "add":

                // build list of currency codes
                this.currencySelect = [];
                this.currencySelect.push({ label: this.translation.instant("Currency"), value: null });
                for (let currency of currencies.currencies['all']) {
                    this.currencySelect.push({ label: currency['code'] + " - " + currency['name'], value: currency['code'] });
                }

                // default to the first currency for the users current location
                /* ======= REMOVED UNTIL SUCH TIME THAT WE ALLOW CHANGING THE CURRENCY
                if(this.utilityService.getUserAddress() != null && this.utilityService.getUserAddress().countryCode != null)
                for(let country of this.countries['all']) {                        
                    if(country['alpha2']===this.utilityService.getUserAddress().countryCode) {
                        this.selectedCurrency=country['currencies'][0];
                        for(let currency of this.currencies['all'] ) {
                            if(currency['code']===this.selectedCurrency)
                                this.selectedCurrencySymbol=currency['symbol'];
                        }
                    }
                } */

                // set currency code    
                this.selectedCurrency = this.defaultCurrencyCode;
                // get currency symbol
                for (let currency of currencies.currencies['all']) {
                    if (currency['code'] === this.defaultCurrencyCode) {
                        this.selectedCurrencySymbol = currency['symbol'];
                    }
                }
                // disable the currency dropdown
                this.myForm.controls['currency'].disable();

                this.model = new QuotationUI();

                this.session.setloading(true);

                // get current dealer
                this.dealerService.getCurrentDealer(this.session.getSessionUser().username)
                    .subscribe(
                        data => {
                            console.log("current dealer: " + JSON.stringify(data));
                            this.currentDealer = data;

                            // get payment types                            
                            this.quotationService.getPaymentTypes(this.currentDealer.code)
                                .subscribe(
                                    data => {
                                        console.log("edit data: " + data);
                                        console.log("edit data json: " + JSON.stringify(data));
                                        this.paymentTypes = data;
                                        console.log('model json: ' + JSON.stringify(this.paymentTypes));
                                        this.paymentTypeSelect = [];
                                        this.paymentTypeSelect.push({ label: this.translation.instant("Payment Method"), value: null });
                                        for (let paymenttype of this.paymentTypes) {
                                            this.paymentTypeSelect.push({ label: paymenttype.name, value: paymenttype.id });
                                        }

                                        // get issuing warehouses
                                        this.orderService.getWarehouses(this.currentDealer.code)
                                            .subscribe(
                                                data => {
                                                    console.log("edit data: " + data);
                                                    console.log("edit data json: " + JSON.stringify(data));
                                                    this.issuingWarehouses = data;
                                                    console.log('issuing warehouses json: ' + JSON.stringify(this.issuingWarehouses));

                                                    this.myForm.controls['issuingwarehouse'].setValue('');

                                                    this.session.setloading(false);
                                                },
                                                error => {
                                                    console.log("edit error: " + JSON.stringify(error));
                                                    this.alertService.error(this.translation.instant("Failed to get list of warehouses")); //  + " [" + JSON.stringify(error) + "]");
                                                    console.log("edit error json: " + JSON.stringify(error));
                                                    this.session.setloading(false);
                                                    return;
                                                });
                                    },
                                    error => {
                                        console.log("edit error: " + JSON.stringify(error));
                                        this.alertService.error(this.translation.instant("Failed to get list of payment types")); //  + " [" + JSON.stringify(error) + "]");
                                        console.log("edit error json: " + JSON.stringify(error));
                                        this.session.setloading(false);
                                        return;
                                    });
                        },
                        error => {
                            this.alertService.error(this.translation.instant("Failed to get current dealer detail")); //  + " [" + JSON.stringify(error) + "]");
                            console.log("error json: " + JSON.stringify(error));
                            this.session.setloading(false);
                            return;
                        });

                break;

            /*
        case "edit" :
            
            this.quotation=this.quotationService.getQuotationFromCache(this.quotationNumber);
            
            // get quotation detail
            this.session.setloading(true);

            // get current dealer
            this.dealerService.getCurrentDealer(this.session.getSessionUser().username)
                .subscribe(
                    data => {                     
                        console.log("current dealer: " + JSON.stringify(data));
                        this.currentDealer = data;        

                        this.quotationService.getQuotation(this.currentDealer.code, this.quotationNumber)
                            .subscribe(
                                data => {
                                    console.log("edit data: " + data);
                                    console.log("edit data json: " + JSON.stringify(data));
                                    this.quotation = data;
                                    console.log('model json: ' + JSON.stringify(this.model));                                
                                    this.session.setloading(false);
                                },
                                error => {
                                    console.log("edit error: " + JSON.stringify(error));
                                    this.alertService.error(this.translation.instant("Failed to get quotation detail")); //  + " [" + JSON.stringify(error) + "]");
                                    console.log("edit error json: " + JSON.stringify(error));
                                    this.session.setloading(false);
                                    return;
                                });                                        
                    },
                    error => {                                                        
                        this.alertService.error(this.translation.instant("Failed to get current dealer detail")); //  + " [" + JSON.stringify(error) + "]");
                        console.log("error json: " + JSON.stringify(error));
                        this.session.setloading(false);
                        return;
                    });

            break;
*/
            case "edit":

                // build list of currency codes
                this.currencySelect = [];
                this.currencySelect.push({ label: this.translation.instant("Currency"), value: null });
                for (let currency of currencies.currencies['all']) {
                    this.currencySelect.push({ label: currency['code'] + " - " + currency['name'], value: currency['code'] });
                }

                // default to the first currency for the users current location
                /* ======= REMOVED UNTIL SUCH TIME THAT WE ALLOW CHANGING THE CURRENCY
                if(this.utilityService.getUserAddress() != null && this.utilityService.getUserAddress().countryCode != null)
                for(let country of this.countries['all']) {                        
                    if(country['alpha2']===this.utilityService.getUserAddress().countryCode) {
                        this.selectedCurrency=country['currencies'][0];
                        for(let currency of this.currencies['all'] ) {
                            if(currency['code']===this.selectedCurrency)
                                this.selectedCurrencySymbol=currency['symbol'];
                        }
                    }
                } */

                // set currency code    
                this.selectedCurrency = this.defaultCurrencyCode;
                // get currency symbol
                for (let currency of currencies.currencies['all']) {
                    if (currency['code'] === this.defaultCurrencyCode) {
                        this.selectedCurrencySymbol = currency['symbol'];
                    }
                }
                // disable the currency dropdown
                this.myForm.controls['currency'].disable();

                this.model = new QuotationUI();

                this.session.setloading(true);

                // get current dealer
                this.dealerService.getCurrentDealer(this.session.getSessionUser().username)
                    .subscribe(
                        data => {
                            console.log("current dealer: " + JSON.stringify(data));
                            this.currentDealer = data;

                            // get payment types                            
                            this.quotationService.getPaymentTypes(this.currentDealer.code)
                                .subscribe(
                                    data => {
                                        console.log("edit data: " + data);
                                        console.log("edit data json: " + JSON.stringify(data));
                                        this.paymentTypes = data;
                                        console.log('model json: ' + JSON.stringify(this.paymentTypes));
                                        this.paymentTypeSelect = [];
                                        this.paymentTypeSelect.push({ label: this.translation.instant("Payment Method"), value: null });
                                        for (let paymenttype of this.paymentTypes) {
                                            this.paymentTypeSelect.push({ label: paymenttype.name, value: paymenttype.id });
                                        }

                                        // get issuing warehouses
                                        this.orderService.getWarehouses(this.currentDealer.code)
                                            .subscribe(
                                                data => {
                                                    console.log("edit data: " + data);
                                                    console.log("edit data json: " + JSON.stringify(data));
                                                    this.issuingWarehouses = data;
                                                    console.log('model json: ' + JSON.stringify(this.issuingWarehouses));
                                                    this.issuingWarehouseSelect = [];
                                                    this.issuingWarehouseSelect.push({ label: this.translation.instant("Issuing Warehouse"), value: null });
                                                    for (let warehouse of this.issuingWarehouses) {
                                                        this.issuingWarehouseSelect.push({ label: warehouse.name, value: warehouse.id.toString() });
                                                    }

                                                    this.myForm.disable();

                                                    // get quotation detail
                                                    this.quotationService.getQuotation(this.currentDealer.code, this.quotationNumber)
                                                        .subscribe(
                                                            data => {
                                                                console.log("view data: " + data);
                                                                console.log("view data json: " + JSON.stringify(data));
                                                                this.quotation = data;


                                                                this.mapQuoteResponseToUI(data);

                                                                this.model.createdDate = moment(Number.parseInt(this.model.createdDate)).format("YYYY-MM-DD");

                                                                if (this.quotation.createdDate) {
                                                                    this.quotation.createdDate = moment(Number.parseInt(this.quotation.createdDate)).format("YYYY-MM-DD");
                                                                }

                                                                //this.model.acceptedDate=moment(Number.parseInt(this.model.acceptedDate)).format("YYYY-MM-DD");
                                                                console.log('model json: ' + JSON.stringify(this.model));
                                                                this.session.setloading(false);
                                                            },
                                                            error => {
                                                                console.log("edit error: " + JSON.stringify(error));
                                                                this.alertService.error(this.translation.instant("Failed to get quotation detail")); //  + " [" + JSON.stringify(error) + "]");
                                                                console.log("edit error json: " + JSON.stringify(error));
                                                                this.session.setloading(false);
                                                                return;
                                                            });
                                                },
                                                error => {
                                                    console.log("edit error: " + JSON.stringify(error));
                                                    this.alertService.error(this.translation.instant("Failed to get list of warehouses")); //  + " [" + JSON.stringify(error) + "]");
                                                    console.log("edit error json: " + JSON.stringify(error));
                                                    this.session.setloading(false);
                                                    return;
                                                });
                                    },
                                    error => {
                                        console.log("edit error: " + JSON.stringify(error));
                                        this.alertService.error(this.translation.instant("Failed to get list of payment types")); //  + " [" + JSON.stringify(error) + "]");
                                        console.log("edit error json: " + JSON.stringify(error));
                                        this.session.setloading(false);
                                        return;
                                    });
                        },
                        error => {
                            this.alertService.error(this.translation.instant("Failed to get current dealer detail")); //  + " [" + JSON.stringify(error) + "]");
                            console.log("error json: " + JSON.stringify(error));
                            this.session.setloading(false);
                            return;
                        });

                break;
        }
    }

    clickDelete(quotation: String) {
        this.router.navigate(['/quote/delete', quotation]);
    }

    clickEdit() {

    }

    clickAdd() {
        this.router.navigate(['/quote/add']);
    }

    clickBack() {
        this.router.navigate(['/quote/list']);
    }

    submitQuoteCrudForm() {

        this.alertService.clearAlert();

        // validate form
        if (this.mode != 'delete') {
            this.formsubmitted = true;
            this.validateAllFormFields(this.myForm);
            this.myForm.updateValueAndValidity();
            if (!this.myForm.valid) {
                this.alertService.error(this.translation.instant("The form is not valid, please correct the values and try again"));
                return;
            }
            this.formsubmitted = false;
        }

        if (!this.model.quoteLines.length) return;

        //confirm
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            width: '250px',
            data: { title: 'Confirm', content: 'Accept this quote?' }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result === true) {

                // create new quotation
                this.session.setloading(true);
                this.quotationService.createQuotation(this.currentDealer.code, this.mapUItoQuoteRequest())
                    .subscribe(
                        (data: any) => {
                            this.session.setloading(false);
                            var location: string = data.headers.get('Location');
                            console.log("location: " + location);
                            console.log("location length: " + location.split("/").length);
                            this.alertService.successKeepAfterNavigation(this.translation.instant("Quotation created"));
                            this.router.navigate(['/quote/edit', location.split("/").pop()]); // redirect to view
                        },
                        error => {
                            this.session.setloading(false);
                            console.log(error);
                            this.alertService.error(this.translation.instant("Failed to accept quotation")); //  + " [" + JSON.stringify(error) + "]");
                        });
            }
        });
    }

    onQuoteSelect(quote) {
        this.router.navigate(['/quote/edit', quote]);
    }
    /*
    close() {
        this.displayDialog = false;
    }
    */
    quoteFooterControls = [];
    onIssuingWarehouseSelect(event) {

        this.alertService.clearAlert();

        this.myForm.controls['issuingwarehouse'].setValue(event.option.value.name);
        this.selectedWarehouse = '' + event.option.value.id;
        this.model.issuedWarehouseId = this.selectedWarehouse;
        this.quoteFooterControls = [];

        // get products for warehouse
        this.session.setloading(true);
        this.orderService.getProducts(this.currentDealer.code, this.model.issuedWarehouseId)
            .subscribe(
                data => {
                    /*
                    console.log("edit data: " + data);
                    console.log("edit data json: " + JSON.stringify(data));*/
                    this.products = data; /*
                    console.log('model json: ' + JSON.stringify(this.issuingWarehouses));
                    this.productSelect = [];
                    this.productSelect.push({ label: this.translation.instant("Select Product"), value: null });
                    this.selectedProduct = null;
                    for (let product of this.products) {
                        for (let line of this.model.quoteLines) {
                            if (line.erpItemCode === product.erpItemCode)
                                product.quantity = Number(product.quantity) - Number(line.quantity);
                        }
                        this.productSelect.push({ label: product.description, value: product });
                    }*/
                    this.session.setloading(false);

                    if (this.products.length > 0) {
                        this.quoteFooterControls = ['productSelectFooter', 'productSelectAvailableFooter', 'productSelectQuantityInputFooter'];
                    } else {
                        this.quoteFooterControls = [];
                    }
                },
                error => {
                    console.log("edit error: " + JSON.stringify(error));
                    this.alertService.error(this.translation.instant("Failed to get list of products for issuing warehouse")); //  + " [" + JSON.stringify(error) + "]");
                    console.log("edit error json: " + JSON.stringify(error));
                    this.session.setloading(false);
                    return;
                });

    }

    updateQuoteLineQuantity(line: QuotationUILine) {

        if (line.quantity === line.acceptedquantity) return;

        this.alertService.clearAlert();

        // revert quantity if invalid
        if (line.quantity < 1 || line.quantity > line.available) {
            console.log("invalid amount, reverting");
            line.quantity = line.acceptedquantity;
            return;
        }

        // buffer copy existing quote lines for calcing
        var quoteLines: QuotationUILine[] = [];
        for (let currentQuoteLine of this.model.quoteLines) {
            var quoteLine = new QuotationUILine();
            quoteLine.available = currentQuoteLine.available;
            quoteLine.description = currentQuoteLine.description;
            quoteLine.discountAmount = currentQuoteLine.discountAmount;
            quoteLine.erpItemCode = currentQuoteLine.erpItemCode;
            quoteLine.grossAmount = currentQuoteLine.grossAmount;
            quoteLine.line = currentQuoteLine.line;
            quoteLine.netAmount = currentQuoteLine.netAmount;
            quoteLine.posProductId = currentQuoteLine.posProductId;
            quoteLine.quantity = line.line === currentQuoteLine.line ? line.quantity : currentQuoteLine.quantity; // update line with new quantity
            quoteLine.taxAmount = currentQuoteLine.taxAmount;
            quoteLine.uom = currentQuoteLine.uom;
            quoteLine.warehouseId = currentQuoteLine.warehouseId;
            quoteLine.origavailable = currentQuoteLine.origavailable;
            quoteLine.product = currentQuoteLine.product;
            quoteLines.push(quoteLine);
        }

        this.submitQuoteForPricing(quoteLines);
    }

    addQuoteLine() {

        if (this.selectedProduct == null) return;

        this.alertService.clearAlert();
        this.notenoughstock = false;

        // buffer copy existing quote lines for calcing
        var quoteLines: QuotationUILine[] = [];
        for (let currentQuoteLine of this.model.quoteLines) {
            var quoteLine = new QuotationUILine();
            quoteLine.available = currentQuoteLine.available;
            quoteLine.description = currentQuoteLine.description;
            quoteLine.discountAmount = currentQuoteLine.discountAmount;
            quoteLine.erpItemCode = currentQuoteLine.erpItemCode;
            quoteLine.grossAmount = currentQuoteLine.grossAmount;
            quoteLine.line = currentQuoteLine.line;
            quoteLine.netAmount = currentQuoteLine.netAmount;
            quoteLine.posProductId = currentQuoteLine.posProductId;
            quoteLine.quantity = currentQuoteLine.quantity;
            quoteLine.taxAmount = currentQuoteLine.taxAmount;
            quoteLine.uom = currentQuoteLine.uom;
            quoteLine.warehouseId = currentQuoteLine.warehouseId;
            quoteLine.origavailable = currentQuoteLine.origavailable;
            quoteLine.product = currentQuoteLine.product;
            quoteLines.push(quoteLine);
        }

        if (this.selectedProductQuantity > 0) {
            if (this.selectedProductQuantity <= this.selectedProduct.quantity) {
                let line = new QuotationUILine();
                line.erpItemCode = this.selectedProduct.erpItemCode;
                line.posProductId = this.selectedProduct.id;
                line.description = this.selectedProduct.description;
                line.quantity = this.selectedProductQuantity;
                line.line = quoteLines.length;
                line.uom = this.selectedProduct.uom;
                line.warehouseId = this.selectedWarehouse;
                line.origavailable = Number(this.productSelectAvailable);
                line.product = this.selectedProduct;
                line.available = this.selectedProduct.quantity;
                quoteLines.push(line);
                this.submitQuoteForPricing(quoteLines);
            } else {
                this.notenoughstock = true;
                this.alertService.error("Insufficient stock");
            }
        }
    }

    recalculateQuote() {

        this.alertService.clearAlert();

        // buffer copy existing quote lines for calcing
        var quoteLines: QuotationUILine[] = [];
        for (let currentQuoteLine of this.model.quoteLines) {
            var quoteLine = new QuotationUILine();
            quoteLine.available = currentQuoteLine.available;
            quoteLine.description = currentQuoteLine.description;
            quoteLine.discountAmount = currentQuoteLine.discountAmount;
            quoteLine.erpItemCode = currentQuoteLine.erpItemCode;
            quoteLine.grossAmount = currentQuoteLine.grossAmount;
            quoteLine.line = currentQuoteLine.line;
            quoteLine.netAmount = currentQuoteLine.netAmount;
            quoteLine.posProductId = currentQuoteLine.posProductId;
            quoteLine.quantity = currentQuoteLine.quantity;
            quoteLine.taxAmount = currentQuoteLine.taxAmount;
            quoteLine.uom = currentQuoteLine.uom;
            quoteLine.warehouseId = currentQuoteLine.warehouseId;
            quoteLine.origavailable = currentQuoteLine.origavailable;
            quoteLine.product = currentQuoteLine.product;
            quoteLines.push(quoteLine);
        }

        this.submitQuoteForPricing(quoteLines);
    }

    submitQuoteForPricing(quoteLines: QuotationUILine[], addProductBack?: Product) {

        // validate form
        this.formsubmitted = true;
        this.validateAllFormFields(this.myForm);
        this.myForm.updateValueAndValidity();
        if (!this.myForm.valid) {
            this.alertService.error(this.translation.instant("The form is not valid, please correct the values and try again"));
            return;
        }
        this.formsubmitted = false;

        this.session.setloading(true);

        this.pricingquote = true;
        console.log("Pricing quote..");

        // build pricing request
        let priceRequest = new PriceRequest();
        priceRequest.context = "B2B"; //TODO: Will this ever be different
        priceRequest.currencyCode = this.selectedCurrency;
        priceRequest.paymentType = this.model.paymentType.toString();
        priceRequest.posCustomerId = this.currentDealer.billToAddress.posClientId.toString();
        priceRequest.priceDate = moment(new Date()).format();

        let priceLineRequests = new LineRequests();
        for (let line of quoteLines) {
            let priceLine = new LineRequest();
            priceLine.lineNumber = line.line.toString();
            priceLine.paymentTerm = "IMMEDIATE"; //TODO: Will this ever be different?
            priceLine.posProductId = line.posProductId.toString();
            priceLine.posWarehouseId = line.warehouseId;
            priceLine.quantity = line.quantity.toString();
            priceLine.uom = line.uom;
            priceLineRequests.lineRequest.push(priceLine);
        }
        priceRequest.lineRequests = priceLineRequests;

        console.log("pricing request: " + JSON.stringify(priceRequest));

        // request prices
        this.quotationService.getQuotePrices(this.currentDealer.code, priceRequest)
            .subscribe(
                priceResponse => {

                    console.log("pricing response: " + JSON.stringify(priceResponse));

                    if (priceResponse.statusCode.trim() != "0") { // error returned back from pricing
                        this.session.setloading(false);
                        this.alertService.error(this.translation.instant("Failed to get pricing") + " [" + priceResponse.statusDescription + "]");
                        this.pricingquote = false;

                    } else {

                        /**********************************
                        var priceResponse : PriceResponse = new PriceResponse();
                        var priceResponseDetails : PriceResponseDetails = new PriceResponseDetails();                        
                        var lineResponses = new LineResponses();
                        var lineResponse : LineResponse = new LineResponse();
                        lineResponse.lineNumber=0;
                        lineResponse.discountValue=1;
                        lineResponse.grossValue=12;
                        lineResponse.taxValue=1;
                        lineResponse.netValue=10;
                        lineResponses.lineResponse.push(lineResponse);
                        priceResponseDetails.totalDiscountValue=1;
                        priceResponseDetails.totalGrossValue=12;
                        priceResponseDetails.totalTaxValue=1;
                        priceResponseDetails.totalValue=10;
                        priceResponseDetails.lineResponses=lineResponses;
                        priceResponse.priceResponseDetails=priceResponseDetails;
                        *******************************/

                        // update quote lines with pricing response
                        for (let priceResponseLine of priceResponse.priceResponseDetails.lineResponses.lineResponse) {
                            let i = 0;
                            for (let quoteLine of quoteLines) {
                                if (quoteLine.line === priceResponseLine.lineNumber) {
                                    quoteLine.grossAmount = priceResponseLine.grossValue;
                                    quoteLine.taxAmount = priceResponseLine.taxValue;
                                    quoteLine.discountAmount = priceResponseLine.discountValue;
                                    quoteLine.netAmount = priceResponseLine.value;
                                    quoteLine.acceptedquantity = quoteLine.quantity; // we store this so we can revert back on invalid inputs
                                    // remove any products in response from the product list, so we only have unique product lines on the quote
                                    let j = 0;
                                    for (let product of this.products) {
                                        if (product['erpItemCode'] === quoteLine.erpItemCode) {
                                            this.products.splice(j, 1);
                                        }
                                        j = j + 1;
                                    }
                                }
                                i = i + 1;
                            }
                        }

                        // only at this point do we update the model lines and therefore the UI
                        this.model.quoteLines = quoteLines;

                        // ..update model totals
                        this.model.grossAmount = priceResponse.priceResponseDetails.totalGrossValue;
                        this.model.discountAmount = priceResponse.priceResponseDetails.totalDiscountValue;
                        this.model.taxAmount = priceResponse.priceResponseDetails.totalTaxValue;
                        this.model.netAmount = priceResponse.priceResponseDetails.totalValue;

                        this.pricingquote = false;
                        this.selectedProduct = null;
                        this.selectedProductQuantity = 0;
                        this.productSelectAvailable = null;

                        // disable changing the warehouse once a product from a warehouse had been added
                        if (this.model.quoteLines.length > 0) {
                            this.myForm.controls['issuingwarehouse'].disable();
                        } else {
                            this.myForm.controls['issuingwarehouse'].enable();
                        }

                        // we removed the line so add the product back to product list
                        if (addProductBack) {
                            this.products.push(addProductBack);
                        }

                        this.myForm.controls['product'].setValue('');
                        this.myForm.controls['quantity'].setValue('');

                        setTimeout(() => { this.productSearchElement.nativeElement.focus(); }, 0);

                        this.session.setloading(false);
                    }
                },
                error => {
                    console.log(error);
                    this.pricingquote = false;
                    this.session.setloading(false);
                    this.alertService.error(this.translation.instant("Failed to get pricing")); //  + " [" + JSON.stringify(error) + "]");
                });
    }

    removeQuoteLine(line: QuotationUILine) {

        this.alertService.clearAlert();
        this.session.setloading(true);

        // buffer copy existing quote lines for calcing
        var quoteLines: QuotationUILine[] = [];
        for (let currentQuoteLine of this.model.quoteLines) {
            if (line.line != currentQuoteLine.line) { // skip line being removed
                var quoteLine = new QuotationUILine();
                quoteLine.available = currentQuoteLine.available;
                quoteLine.description = currentQuoteLine.description;
                quoteLine.discountAmount = currentQuoteLine.discountAmount;
                quoteLine.erpItemCode = currentQuoteLine.erpItemCode;
                quoteLine.grossAmount = currentQuoteLine.grossAmount;
                quoteLine.line = currentQuoteLine.line;
                quoteLine.netAmount = currentQuoteLine.netAmount;
                quoteLine.posProductId = currentQuoteLine.posProductId;
                quoteLine.quantity = currentQuoteLine.quantity;
                quoteLine.taxAmount = currentQuoteLine.taxAmount;
                quoteLine.uom = currentQuoteLine.uom;
                quoteLine.warehouseId = currentQuoteLine.warehouseId;
                quoteLine.origavailable = currentQuoteLine.origavailable;
                quoteLine.product = currentQuoteLine.product;
                quoteLines.push(quoteLine);
            }
        }

        this.submitQuoteForPricing(quoteLines, line.product);
    }

    onQuoteLineProductSelect(event) {
        console.log(JSON.stringify(event.value));

        this.myForm.controls['product'].setValue(event.option.value.description);
        this.selectedProduct = event.option.value;
        this.productSelectAvailable = this.selectedProduct.quantity;
        this.selectedProductQuantity = 0;

        setTimeout(() => { this.productSelectQuantityElement.nativeElement.focus(); }, 0);
    }

    mapUItoQuoteRequest(): QuotationDetails {
        var quotationDetails = new QuotationDetails();
        quotationDetails.context = "B2B"; //TODO: Will this ever be different?
        quotationDetails.paymentTerm = "IMMEDIATE"; //TODO: Will this ever be different?
        quotationDetails.currencyCode = this.selectedCurrency;
        quotationDetails.paymentTypeId = this.model.paymentType;
        quotationDetails.billToId = Number.parseInt(this.currentDealer.billToAddress.posClientId.toString());
        quotationDetails.shipToId = Number.parseInt(this.currentDealer.shipToAddress.posClientId.toString());
        quotationDetails.warehouseId = Number.parseInt(this.model.issuedWarehouseId);
        quotationDetails.createdByUser = this.session.getSessionUser().lastname + ", " + this.session.getSessionUser().firstname;
        for (let quoteline of this.model.quoteLines) {
            var detailLine = new QuotationDetailLine();
            detailLine.lineNumber = quoteline.line;
            detailLine.quantity = Number.parseInt(quoteline.quantity.toString());
            detailLine.stockDetailId = quoteline.posProductId;
            detailLine.uom = quoteline.uom;
            quotationDetails.quotationDetails.push(detailLine);
        }
        console.log("QUOTE REQUEST: " + JSON.stringify(quotationDetails));
        return quotationDetails;
    }

    mapQuoteResponseToUI(quotation: any) {

        this.model = new QuotationUI();

        this.model.createdDate = quotation['audit']['createdDate'];
        this.model.createdByUser = quotation['audit']['createdByUser'];
        this.model.quotationNumber = quotation['quotationNumber'];
        this.model.status = quotation['quotationStatus']['name'];
        this.model.paymentType = quotation['paymentType']['id'];
        this.model.issuedWarehouseId = quotation['warehouse']['id'];
        this.model.orderNumber = quotation['orderNumber'];
        //this.model.acceptedDate=quotation['acceptedDate'];
        //this.model.acceptedByUser=quotation['acceptedByUser'];

        this.model.grossAmount = quotation['quotationPrice']['totalGrossValue'];
        this.model.discountAmount = quotation['quotationPrice']['totalDiscountValue'];
        this.model.taxAmount = quotation['quotationPrice']['totalTaxValue'];
        this.model.netAmount = quotation['quotationPrice']['totalValue'];

        for (let warehouse of this.issuingWarehouses) {
            if (warehouse.id === this.model.issuedWarehouseId) {
                this.myForm.controls['issuingwarehouse'].setValue(warehouse.name);
            }
        }

        for (let quotationDetail of quotation['quotationDetails']) {

            let quoteUILine = new QuotationUILine();
            quoteUILine.description = quotationDetail['stockDetail']['label'];
            quoteUILine.line = quotationDetail['lineNumber'];
            quoteUILine.quantity = quotationDetail['quantity'];
            quoteUILine.grossAmount = quotationDetail['quotationDetailPrice']['grossValue'];
            quoteUILine.discountAmount = quotationDetail['quotationDetailPrice']['discountValue'];
            quoteUILine.netAmount = quotationDetail['quotationDetailPrice']['value'];
            quoteUILine.taxAmount = quotationDetail['quotationDetailPrice']['taxValue'];
            this.model.quoteLines.push(quoteUILine);
        }
    }

    private billingAddressContainerCompressed = true;
    toggleBillingAddressContainerCompressed() {
        this.billingAddressContainerCompressed = this.billingAddressContainerCompressed ? false : true;
    }

    private shippingAddressContainerCompressed = true;
    toggleShippingAddressContainerCompressed() {
        this.shippingAddressContainerCompressed = this.shippingAddressContainerCompressed ? false : true;
    }

    /*
    updateSelectedCurrency(event) {        
        for(let currency of this.currencies['all'] ) {
            if(currency['code']===event.value)
                this.selectedCurrencySymbol==currency['symbol'];
        }
        if(this.model.quoteLines.length)
            this.recalculateQuote();
    }*/

    formatAmount(amount: number, addCurrencySymbol?: boolean): string {
        if (addCurrencySymbol)
            return this.utility.formatAmount(amount, this.selectedCurrencySymbol, (this.prefixCurrencySymbol === 'true' || this.prefixCurrencySymbol === 'yes') ? true : false);
        else
            return this.utility.formatAmount(amount);
    }

    placeOrder(quotationNumber: string) {

        //confirm
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            width: '250px',
            data: { title: 'Confirm Order', content: 'Place Order?' }
        });

        dialogRef.afterClosed().subscribe(result => {

            if (result === true) {

                // places order for the quote      
                this.session.setloading(true);
                this.quotationService.acceptQuotation(this.currentDealer.code, quotationNumber)
                    .subscribe(
                        data => {
                            this.session.setloading(false);
                            this.alertService.success(this.translation.instant("Order has been placed"));
                            this.refreshView();
                            this.returnurl = "/quote/list"; // return to listing if back clicked
                        },
                        error => {
                            console.log("edit error: " + JSON.stringify(error));
                            this.alertService.error(this.translation.instant("Failed to place order")); //  + " [" + JSON.stringify(error) + "]");
                            console.log("edit error json: " + JSON.stringify(error));
                            this.session.setloading(false);
                            return;
                        }); /*
              }
          });*/
            }
        })
    }

    refreshView() {
        this.model = new QuotationUI();
        this.session.setloading(true);
        // get quotation detail
        this.quotationService.getQuotation(this.currentDealer.code, this.quotationNumber)
            .subscribe(
                data => {
                    console.log("view data: " + data);
                    console.log("view data json: " + JSON.stringify(data));
                    this.quotation = data;
                    console.log('model json: ' + JSON.stringify(this.model));
                    this.quotation.createdDate = moment(Number.parseInt(this.quotation.createdDate)).format("YYYY-MM-DD");
                    this.mapQuoteResponseToUI(data);

                    this.session.setloading(false);
                },
                error => {
                    console.log("edit error: " + JSON.stringify(error));
                    this.alertService.error(this.translation.instant("Failed to get quotation detail")); //  + " [" + JSON.stringify(error) + "]");
                    console.log("edit error json: " + JSON.stringify(error));
                    this.session.setloading(false);
                    return;
                });
    }

    private downloadingProforma = false;
    downloadProforma() {
        this.session.setloading(true);
        this.quotationService.downloadProforma(this.currentDealer.code, this.model.quotationNumber).subscribe
            ((response: any) => {
                this.session.setloading(false);
                let file = response.blob();
                filesaver.saveAs(file, this.model.quotationNumber + '_profinv.pdf');
            },
                error => {
                    this.session.setloading(false);
                    console.log("error: " + JSON.stringify(error));
                    this.alertService.error(this.translation.instant("Failed to download proforma invoice")); //  + " [" + JSON.stringify(error) + "]");
                    console.log("error json: " + JSON.stringify(error));
                    return;
                });
        return false;
    }

    clickCancel() {

        //confirm
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            width: '250px',
            data: { title: 'Cancel', content: 'Are you certain you want to Cancel this quote?' }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result === true) {

                // cancel quote    
                this.session.setloading(true);
                this.quotationService.cancelQuotation(this.currentDealer.code, this.model.quotationNumber)
                    .subscribe(
                        data => {
                            this.session.setloading(false);
                            this.alertService.success(this.translation.instant("Quote has been cancelled"));
                            this.refreshView();
                            this.returnurl = "/quote/list"; // return to listing if back clicked
                        },
                        error => {
                            console.log("edit error: " + JSON.stringify(error));
                            this.alertService.error(this.translation.instant("Failed to cancel quote")); //  + " [" + JSON.stringify(error) + "]");
                            console.log("edit error json: " + JSON.stringify(error));
                            this.session.setloading(false);
                            return;
                        }); /*
              } 
          });
          */
            }
        })
    }

    isString(x: any): x is string {
        return typeof x === "string";
    }

    private _filterWarehouses(value: String): Warehouse[] {
        if (this.isString(value)) {
            const filterValue = value.toLowerCase();
            return this.issuingWarehouses.filter((warehouse: Warehouse) => warehouse.name.toLowerCase().indexOf(filterValue) > -1);
        }
    }

    private _filterProducts(value: String): Product[] {
        if (this.isString(value)) {
            const filterValue = value.toLowerCase();
            console.log(filterValue);
            return this.products.filter((product: Product) => product.description.toLowerCase().indexOf(filterValue) > -1);
        }
    }

    validateAllFormFields(formGroup: FormGroup) {         //{1}
        Object.keys(formGroup.controls).forEach(field => {  //{2}
            const control = formGroup.get(field);             //{3}
            if (control instanceof FormControl) {             //{4}
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {        //{5}
                this.validateAllFormFields(control);            //{6}
            }
        });
    }

    onInputFocus(event) {
        const target = event.currentTarget;
        target.type = 'text';
        target.setSelectionRange(0, target.value.length);
        target.type = 'number';
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.quotationsDatasource.filter = filterValue.trim().toLowerCase();
    }
}
