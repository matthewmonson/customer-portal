import { Component } from '@angular/core';
import { SessionService } from '../session.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  session;
  constructor(private sessionService: SessionService) {
    this.session=sessionService;
    this.session.setloading(false);
  }
}
