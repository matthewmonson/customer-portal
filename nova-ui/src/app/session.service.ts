import { Injectable } from '@angular/core';
import { User } from './_models/user';
import { Subject, Observable} from 'rxjs';

import { HttpClient, HttpHeaders, HttpBackend } from '@angular/common/http';

import { ConfigService } from './config.service';
import { UtilityService } from './utility.service';

@Injectable()
export class SessionService {
  
  private menuActive = new Subject<boolean>();
  
  private currentUser : User;

  private sessionToken : string = null;

  private sessionStatus = new Subject<string>();

  NovaServiceApiUrl : string = null;
  AuthenticationApiPath : string = null;
  UserManagementCurrentUserApiPath : string = null;
  HttpRequestTimeout : number = null;

  private backendHttpClient: HttpClient;

  constructor(private config: ConfigService, private http: HttpClient, backEndHttp: HttpBackend) {
    this.backendHttpClient = new HttpClient(backEndHttp);
  }

  isMenuActive() : Observable<boolean> {    
    return this.menuActive.asObservable();
  }

  setMenuActive(active : boolean) {    
    this.menuActive.next(active);
  }
  
  // loading indicator
  private loader = new Subject<boolean>();

  setloading(toggle) {
    this.loader.next(toggle);
  }

  loading() : Observable<boolean> {
    return this.loader.asObservable();
  }

  testSessionURI(token?)  {

      // we simply do a get on the session uri to see if its still valid
      return this.backendHttpClient.get(token ? token : this.sessionToken, { headers: {'Content-Type':  'application/json','Accept': 'application/json'}, observe: 'response' }); //.timeout(this.HttpRequestTimeout);
  }
  
  registerSession(username : string, password : string) {

        // check config, might not be initialised yet
        if(!this.NovaServiceApiUrl)
            this.NovaServiceApiUrl                = this.config.getConfig("novaApiUrl");
        if(!this.AuthenticationApiPath)
            this.AuthenticationApiPath            = this.config.getConfig("novaApiAuthenticationPath");
        if(!this.UserManagementCurrentUserApiPath)
            this.UserManagementCurrentUserApiPath = this.config.getConfig("novaApiUserManagementCurrentUserPath");
        if(!this.HttpRequestTimeout)
            this.HttpRequestTimeout               = this.config.getConfig("httpRequestTimeout");

        // set the headers
        const 
          headers = new HttpHeaders({
            'Content-Type':  'application/json',
            'Accept': 'application/json'
          });

        // set the body
        let body = JSON.stringify({ username: username, password: password });

        // invoke Nova Service to register a new session
        return this.http.post(this.NovaServiceApiUrl + this.AuthenticationApiPath, body, {headers: headers, observe: 'response'} ); //.timeout(this.HttpRequestTimeout);
  }

  createSessionUser(user : User, token : string) : boolean {     
    localStorage.setItem('sessiontoken', token);
    localStorage.setItem('sessionuser', JSON.stringify(user));
    this.sessionToken=token;    
    this.currentUser=user;
    console.log("User session created");

    //build menu based on permissions
    let menu = [];
    menu.push({
      title : "Home",
      url   : "/home",
      icon  : "home"
    });
    for(let role of this.currentUser.roles) {
      role=role.toLowerCase().trim();
      console.log("role: [" + role + "]");
      if( role==='role_user_read' ) {
        menu.push({
          title : "Users",
          url   : "/user/list",
          icon  : "people"
        },  );
      }
      if( role==='role_dealer_read' ) {
        menu.push({
          title : "Dealers",
          url   : "/dealer/list",
          icon  : "person"
        });
      }
      if( role==='role_opco_read' ) {
        menu.push({
          title : "OpCos",
          url   : "/opco/list",
          icon  : "business"
        });
      }
      if( role==='role_quotation_read' ) {
        menu.push({
          title : "Quotations",
          url   : "/quote/list",
          icon  : "add_shopping_cart"
        });
      }
      if( role==='role_order_read' ) {
        menu.push({
          title : "Orders",
          url   : "/order/list",
          icon  : "assignment"
        });
      }
      if( role==='role_receipt_read') {
        menu.push({
          title : "Payments",
          url   : "/payment/list",
          icon  : "attach_money"
        });
      }
    }
    menu.push({
      title : "Logout",
      url   : "/login/logout",
      icon  : "lock"
    });
    console.log("user menu: " + JSON.stringify(menu));
    localStorage.setItem('sessionmenu', JSON.stringify(menu));

    this.setMenu(menu);

    return true;
  }

  private menu = new Subject<any[]>();

  setMenu(menu) {
    this.menu.next(menu);
  }

  getMenu() : Observable<any[]> {
    return this.menu.asObservable();
  }

  getSessionUser() : User {    
    return this.currentUser;
  }

  updateSessionUser(user : User) {       
    this.currentUser=user;
  }

  destroySessionUser() {    
    localStorage.removeItem('sessiontoken');
    localStorage.removeItem('sessionuser');    
    this.currentUser=null;
    this.sessionToken=null;    
    this.sessionStatus.next("destroyed");    
  }

  getSessionToken() : string {
    return this.sessionToken;
  }

  getSessionStatus() : Observable<string> {
    return this.sessionStatus.asObservable();
  }

  private cancelPendingRequests$ = new Subject<void>();

  // cancel pending HTTP requests, this gets set on route change inside app.component.ts
  public cancelPendingRequests() {
    this.cancelPendingRequests$.next()
  }
  public onCancelPendingRequests() {
    return this.cancelPendingRequests$.asObservable();
  }
}
