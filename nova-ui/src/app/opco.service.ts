import { Injectable } from '@angular/core';
import { SessionService } from './session.service';
import { ConfigService } from './config.service';
import { AlertService } from './alert.service';
import { Opco } from './_models/opco';

import { UtilityService } from './utility.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class OpcoService {

    NovaServiceApiUrl : string;
    OpcoAccountManagementCurrentOpcoApiPath : string;
    OpcoAccountManagementOpcosApiPath : string;
    OpcoAccountManagementRolesApiPath : string;
    HttpRequestTimeout : number;
    
    constructor(private session: SessionService, private config: ConfigService, private http: HttpClient, private alert : AlertService, private utlity : UtilityService) { 
        this.NovaServiceApiUrl                       = config.getConfig("novaApiUrl");        
        this.OpcoAccountManagementOpcosApiPath       = config.getConfig("novaApiOpcoManagementOpcosPath"); 
        this.HttpRequestTimeout                      = config.getConfig("httpRequestTimeout");

        // clear cache when session destroyed
        session.getSessionStatus().subscribe((data) => {
            if(data=='destroyed'){
                this._getAllOpcos=null;
                this._getAllOpcosForParent=null;
                this._getOpco=null;
                this.currentOpcoCode=null;
                this.currentOpcoParent=null;
                this.currentParentOpcoCode=null;
            }
        });   
    }

    private _getAllOpcos : Opco[] = [];
    getAllOpcos() : Observable<Opco[]> {
        return Observable.create(observer => {
            this.http.get(this.NovaServiceApiUrl + this.OpcoAccountManagementOpcosApiPath, {observe: 'response'}) ////.timeout(this.HttpRequestTimeout)
                //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
                //.map((response: Response) => response.json())
                .subscribe((data: any) => {
                    this._getAllOpcos = data.body;
                    observer.next(this._getAllOpcos);
                    observer.complete();
                }, (err) => {
                    console.log("err: " + JSON.stringify(err));
                    observer.error(err);
                });
        });
    }
    getAllOpcosFromCache() : Opco[] {
        return this._getAllOpcos;
    }

    private _getAllOpcosForParent : Opco[] = [];
    private currentParentOpcoCode : String = "";
    getAllOpcosForParent(parent : String) : Observable<Opco[]> {
        return Observable.create(observer => {
            this.http.get(this.NovaServiceApiUrl + this.OpcoAccountManagementOpcosApiPath + "?where=%7B%22parentOpCo%22:%22" + parent +"%22%7D", {observe: 'response'})//.timeout(this.HttpRequestTimeout)
                //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
                //.map((response: Response) => response.json())
                .subscribe((data:any) => {
                    this._getAllOpcosForParent = data.body;
                    this.currentParentOpcoCode = parent;
                    observer.next(this._getAllOpcosForParent);
                    observer.complete();
                }, (err) => {
                    console.log("err: " + JSON.stringify(err));
                    observer.error(err);
                });
        });
    }
    getAllOpcosForParentFromCache(parent : String) : Opco[] {
        if(parent!=this.currentParentOpcoCode)
            return [] as Opco[];
        else
            return this._getAllOpcosForParent;
    }

    private _getOpco : Opco;
    private currentOpcoParent : String = "";
    private currentOpcoCode : String = "";
    getOpco(parentOpco : String, code : String) : Observable<Opco> {
        return Observable.create(observer => {
            this.http.get(this.NovaServiceApiUrl + this.OpcoAccountManagementOpcosApiPath + "/" + parentOpco + "/" + code, {observe: 'response'})//.timeout(this.HttpRequestTimeout)
                //.catch((error : any) => { return this.utlity.parseHTTPErrorResponse(error) })
                //.map((response: Response) => response.json())
                .subscribe((data:any) => {
                    this._getOpco = data.body;
                    this.currentOpcoCode=code;
                    this.currentOpcoParent=parentOpco;
                    observer.next(this._getOpco);
                    observer.complete();
                }, (err) => {
                    console.log("err: " + JSON.stringify(err));
                    observer.error(err);
                });
        });
    }
    getOpcoFromCache(parentOpco : String, code : String): Opco {
        if(code==this.currentOpcoCode && parentOpco==this.currentOpcoParent)
            return this._getOpco;
        else
            return new Opco();
    }

    createOpco(Opco: Opco) {
        // invoke Nova Service to create a new Opco
        return this.http.post(this.NovaServiceApiUrl + this.OpcoAccountManagementOpcosApiPath + "/" + Opco.parentOpCo, Opco)
    }

    updateOpco(Opco: Opco) {
        // invoke Nova Service to update an existing Opco
        return this.http.put(this.NovaServiceApiUrl + this.OpcoAccountManagementOpcosApiPath + "/" + Opco.parentOpCo + "/" + Opco.code, Opco);
    }

    deleteOpco(parentOpco : String, code : String) {
        // invoke Nova Service to delete a Opco
        return this.http.delete(this.NovaServiceApiUrl + this.OpcoAccountManagementOpcosApiPath + "/" + parentOpco + "/" + code, {observe: 'response'});
    }
}

