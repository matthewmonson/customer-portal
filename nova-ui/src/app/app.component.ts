import { Component, Inject } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SessionService } from './session.service';
import { ConfigService } from './config.service';
import { UtilityService } from './utility.service';
import { TranslateService } from '@ngx-translate/core';

import defaultLanguage from "../assets/i18n/en.json";
import { AuthGuard } from './auth.guard';
import { Observable } from 'rxjs';

import { delay } from 'rxjs/operators';
import { AlertService } from './alert.service';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from './shared-components/confirm-dialog/confirm-dialog.component';
import { Location } from '@angular/common';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {


  color = 'primary';
  mode = 'indeterminate';

  error = false;
  errormessage: string;
  itracReturnUrl: string;

  environment: string;
  currentsession: SessionService;
  userroles: string[];
  accountsfocused: boolean = false;
  inventoryfocused: boolean = false;

  menuactive$: Observable<boolean> = this.session.isMenuActive().
    pipe(
      delay(0) // trick to overcome the annoying ExpressionChangedAfterItHasBeenCheckedError
    );

  loading$: Observable<boolean> = this.session.loading().
    pipe(
      delay(0) // trick to overcome the annoying ExpressionChangedAfterItHasBeenCheckedError
    );

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private session: SessionService,
    private config: ConfigService,
    public utility: UtilityService,
    public translate: TranslateService,
    public authService: AuthGuard,
    private alertService: AlertService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
    public location: Location,
    private router: Router
  ) {
    this.sideMenu();
    this.initializeApp();
  }

  navigate: any;
  sideMenu() {
    this.navigate = [];
  }

  prevUrl = undefined;
  haveHistory = false;

  initializeApp() {
    this.platform.ready().then(() => {

      this.environment = this.config.getEnv();
      this.currentsession = this.session;

      var supportedLanguageCodes: string = this.config.getConfig("supportedLanguageCodes");

      var languageCode = navigator.language;  // try to use browser language
      if (supportedLanguageCodes.indexOf(languageCode) < 0) {
        if (languageCode.split("-").length > 0) {
          languageCode = languageCode.split("-")[0];
          if (supportedLanguageCodes.indexOf(languageCode) < 0)
            languageCode = "en";
        } else
          languageCode = "en";
      }
      this.translate.setTranslation(languageCode, defaultLanguage);
      this.translate.setDefaultLang(languageCode);
      this.utility.setLocale(languageCode);

      this.statusBar.styleDefault();

      this.alertService.getMessage().
        subscribe(message => {
          this.openSnackBar(message);
        });

      this.splashScreen.hide();
      this.session.setloading(false);

      this.session.getMenu().subscribe(menu => {
        this.navigate = menu;
      });

      // cancel any pending HTTP requests on route change
      this.router.events.pipe(filter(event => event instanceof NavigationStart)).forEach((event : NavigationStart) => {
        this.session.cancelPendingRequests();
      });
    });
  }

  setaccountfocusedclick(event: Event) {
    if (this.accountsfocused)
      this.accountsfocused = false;
    else
      this.accountsfocused = true;
    return false;
  }

  setaccountfocusedtap(event: Event) {
    if (this.accountsfocused)
      this.accountsfocused = false;
    else
      this.accountsfocused = true;
    return false;
  }

  openSnackBar(message) {
    if (message) {
      this.snackBar.open(message.text, null, {
        duration: 5000,
      });
    }
  }

  showBackButton(): boolean {
    return this.authService.userAuthenticated() && (this.router.url != '/home');
  }
}


