import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OpcoPageRoutingModule } from './opco-routing.module';

import { OpcoPage } from './opco.page';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '../material/material.module';
import { SharedComponentsModule } from '../shared-components/shared-components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OpcoPageRoutingModule,
    TranslateModule,
    MaterialModule,
    ReactiveFormsModule,
    SharedComponentsModule
  ],
  declarations: [OpcoPage],
  exports: [
    SharedComponentsModule
  ]
})
export class OpcoPageModule {}
