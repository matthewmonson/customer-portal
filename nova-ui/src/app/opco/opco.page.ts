import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Opco } from '../_models/opco';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { OpcoService } from '../opco.service';
import { AlertService } from '../alert.service';
import { SessionService } from '../session.service';
import { TranslateService } from '@ngx-translate/core';
import { SupplierSystemSpec } from '../_models/suppliersystemspec';
import { Location } from '@angular/common';
import { UtilityService } from '../utility.service';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../shared-components/confirm-dialog/confirm-dialog.component';

@Component({
    selector: 'app-opco',
    templateUrl: './opco.page.html',
    styleUrls: ['./opco.page.scss'],
})
export class OpcoPage implements OnInit {

    myForm: FormGroup;
    formsubmitted: boolean = false;
    model: Opco = null;
    mode: string = "";
    parentOpCo: string;
    code: string;
    opcosDatasource;
    opcos: Opco[] = [];
    repeatservicepassword: string = "";
    passwordsmatch: boolean = false;

    private _opcosObservable: Observable<Opco[]>;
    //dataupdating : boolean = false;


    displayedColumns: string[] = ['code', 'parentOpCo', 'name'];

    constructor(
        private router: Router,
        private opcoService: OpcoService,
        private alertService: AlertService,
        private route: ActivatedRoute,
        private session: SessionService,
        private _fb: FormBuilder,
        public translation: TranslateService,
        public utility: UtilityService,
        private location: Location,
        public dialog: MatDialog) {

        route.params.subscribe(params => {
            this.parentOpCo = params['parentopco'];
            this.code = params['code'];
        });
        this.mode = this.router.url.split("/")[2]; // list/add/edit/delete

        // build this components form group
        this.myForm = this._fb.group({
            'code': ['', Validators.required],
            'name': ['', Validators.required],
            'parentopco': [''],
            'taxnumber': ['', Validators.required],
            'registrationnumber': ['', Validators.required],
            'logo': [''],
            'proformatemplate': [''],
            'theme': [''],
            'serviceusername': [''],
            'servicepassword': [''],
            'repeatservicepassword': [''],
            'serviceurl': ['']
        });
    }

    ngOnInit() {

        switch (this.mode) {

            case "list":

                if (this.parentOpCo) {

                    // get list of opcos from cache                    
                    this.opcos = this.opcoService.getAllOpcosForParentFromCache(this.parentOpCo);
                    this.opcosDatasource = new MatTableDataSource(this.opcos);

                    // get list of opcos from live
                    this.session.setloading(true);
                    this._opcosObservable = this.opcoService.getAllOpcosForParent(this.parentOpCo)
                    this._opcosObservable.subscribe(
                        data => {
                            this.opcos = data;
                            this.opcosDatasource = new MatTableDataSource(this.opcos);
                            this.session.setloading(false);
                        },
                        error => {
                            this.alertService.error(this.translation.instant("Failed to get list of opcos for parent")); //  + " [" + JSON.stringify(error) + "]");
                            this.session.setloading(false);
                            console.log("list error: " + JSON.stringify(error));
                        });

                } else {

                    // get deep list of opcos from cache                    
                    this.opcos = this.opcoService.getAllOpcosFromCache()
                    this.opcosDatasource = new MatTableDataSource(this.opcos);

                    // get deep list of opcos from live
                    this.session.setloading(true);
                    this._opcosObservable = this.opcoService.getAllOpcos()
                    this._opcosObservable.subscribe(
                        data => {
                            this.opcos = data;
                            this.opcosDatasource = new MatTableDataSource(this.opcos);
                            this.session.setloading(false);
                        },
                        error => {
                            this.alertService.error(this.translation.instant("Failed to get list of opcos")); //  + " [" + JSON.stringify(error) + "]");
                            this.session.setloading(false);
                            console.log("list error: " + JSON.stringify(error));
                        });
                }

                break;

            case "add":

                this.myForm.controls['parentopco'].disable();
                this.model = new Opco();
                this.model.parentOpCo = this.parentOpCo;

                break;

            case "edit":

                this.myForm.controls['code'].disable();
                this.myForm.controls['parentopco'].disable();

                this.model = this.opcoService.getOpcoFromCache(this.parentOpCo, this.code);

                // get opco detail
                this.session.setloading(true);
                this.opcoService.getOpco(this.parentOpCo, this.code)
                    .subscribe(
                        data => {
                            console.log("edit data: " + data);
                            console.log("edit data json: " + JSON.stringify(data));
                            this.model = data;
                            console.log('model json: ' + JSON.stringify(this.model));
                            if (!this.model.supplierSystemSpec) {
                                this.model.supplierSystemSpec = new SupplierSystemSpec();
                            }
                            this.myForm.controls['repeatservicepassword'].setValue(this.model.supplierSystemSpec.serviceRegistration.servicePassword);
                            this.session.setloading(false);
                        },
                        error => {
                            console.log("edit error: " + JSON.stringify(error));
                            this.alertService.error(this.translation.instant("Failed to get opco detail")); //  + " [" + JSON.stringify(error) + "]");
                            console.log("edit error json: " + JSON.stringify(error));
                            this.session.setloading(false);
                            return;
                        });

                break;

            case "view":

                this.myForm.disable();

                this.model = this.opcoService.getOpcoFromCache(this.parentOpCo, this.code);

                // get opco detail
                this.session.setloading(true);
                this.opcoService.getOpco(this.parentOpCo, this.code)
                    .subscribe(
                        data => {
                            console.log("edit data: " + data);
                            console.log("edit data json: " + JSON.stringify(data));
                            this.model = data;
                            console.log('model json: ' + JSON.stringify(this.model));
                            if (!this.model.supplierSystemSpec) {
                                this.model.supplierSystemSpec = new SupplierSystemSpec();
                            }
                            this.myForm.controls['repeatservicepassword'].setValue(this.model.supplierSystemSpec.serviceRegistration.servicePassword);
                            this.session.setloading(false);
                        },
                        error => {
                            console.log("edit error: " + JSON.stringify(error));
                            this.alertService.error(this.translation.instant("Failed to get opco detail")); //  + " [" + JSON.stringify(error) + "]");
                            console.log("edit error json: " + JSON.stringify(error));
                            this.session.setloading(false);
                            return;
                        });

                break;

            case "delete":

                this.myForm.disable();

                // first attempt to get Opco model from service cache
                this.model = this.opcoService.getOpcoFromCache(this.parentOpCo, this.code);

                // get opco detail
                this.session.setloading(true);
                this.opcoService.getOpco(this.parentOpCo, this.code)
                    .subscribe(
                        data => {
                            console.log("delete data: " + data);
                            console.log("delete data json: " + JSON.stringify(data));
                            this.model = data;
                            if (!this.model.supplierSystemSpec) {
                                this.model.supplierSystemSpec = new SupplierSystemSpec();
                            }
                            this.myForm.controls['repeatservicepassword'].setValue(this.model.supplierSystemSpec.serviceRegistration.servicePassword);
                            this.session.setloading(false);
                        },
                        error => {
                            console.log("delete error: " + JSON.stringify(error));
                            this.alertService.error(this.translation.instant("Failed to get opco detail")); //  + " [" + JSON.stringify(error) + "]");
                            console.log("delete error json: " + JSON.stringify(error));
                            this.session.setloading(false);
                            return;
                        });

                break;
        }
    }

    private addressContainerCompressed = false;
    toggleAddressContainerCompressed() {
        if (this.addressContainerCompressed)
            this.addressContainerCompressed = false;
        else
            this.addressContainerCompressed = true;
    }

    clickDelete() {

        //confirm
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            width: '250px',
            data: { title: 'Confirm Delete', content: 'Delete OpCo ' + this.model.code + '?' }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result === true) {
                this.session.setloading(true);
                this.opcoService.deleteOpco(this.model.parentOpCo, this.model.code)
                    .subscribe(
                        data => {
                            this.alertService.successKeepAfterNavigation(this.translation.instant("OpCo deleted"));
                            this.session.setloading(false);
                            this.router.navigate(['/opco/list', this.parentOpCo ? this.parentOpCo : '']);
                        },
                        error => {
                            this.session.setloading(false);
                            console.log(error);
                            this.alertService.error(this.translation.instant("Failed to delete OpCo")); //  + " [" + JSON.stringify(error) + "]");
                        });
            }
        });

    }

    clickView(event) {
        this.router.navigate(['/opco/view', event.data['parentOpCo'], event.data['code']]);
    }

    clickEdit(parentOpCo, code) {
        this.router.navigate(['/opco/edit', parentOpCo, code]);
    }

    clickAdd() {
        this.router.navigate(['/opco/add', this.parentOpCo ? this.parentOpCo : this.session.getSessionUser().organization]);
    }

    clickAddChildOpCo() {
        this.router.navigate(['/opco/add', this.model.code]);
    }

    clickCancel() {
        this.router.navigate(['/opco/list']);
    }

    clickManageOpcoUsers() {
        this.router.navigate(['/user/list', this.model.code]);
    }

    clickManageOpcoDealers() {
        this.router.navigate(['/dealer/list', this.model.code]);
    }

    clickManageOpcoChildren() {
        this.router.navigate(['/opco/list', this.model.code]);
    }

    submitOpcoCrudForm() {

        this.alertService.clearAlert();

        // validate form
        if (this.mode != 'delete') {
            this.passwordsmatch = (this.model.supplierSystemSpec.serviceRegistration.servicePassword === this.repeatservicepassword);
            this.formsubmitted = true;
            this.myForm.updateValueAndValidity();
            if (!this.myForm.valid || !this.passwordsmatch) {
                this.alertService.error(this.translation.instant("The form is not valid, please correct the values and try again"));
                return;
            }
            this.formsubmitted = false;
        }

        // submit form        
        switch (this.mode) {
            case "add":
                // create new opco
                this.opcoService.createOpco(this.model)
                    .subscribe(
                        data => {
                            this.alertService.successKeepAfterNavigation(this.translation.instant("OpCo created, please create the Opco Admin User"));
                            this.router.navigate(['/user/add', this.model.code]);
                        },
                        error => {
                            console.log(error);
                            this.alertService.error(this.translation.instant("Failed to add OpCo")); //  + " [" + JSON.stringify(error) + "]");
                        });
                break;

            case "edit":

                delete this.model['_id'];

                console.log("model updated as: " + JSON.stringify(this.model));
                // update opco
                this.opcoService.updateOpco(this.model)
                    .subscribe(
                        data => {
                            this.alertService.success(this.translation.instant("OpCo updated"));
                        },
                        error => {
                            console.log(error);
                            this.alertService.error(this.translation.instant("Failed to update OpCo")); //  + " [" + JSON.stringify(error) + "]");
                        });
                break;

            case "delete":
                // delete opco
                this.opcoService.deleteOpco(this.model.parentOpCo, this.model.code)
                    .subscribe(
                        data => {
                            this.alertService.successKeepAfterNavigation(this.translation.instant("OpCo deleted"));
                            this.router.navigate(['/opco/list', this.parentOpCo ? this.parentOpCo : '']);
                        },
                        error => {
                            console.log(error);
                            this.alertService.error(this.translation.instant("Failed to delete OpCo")); //  + " [" + JSON.stringify(error) + "]");
                        });
                break;
        }
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.opcosDatasource.filter = filterValue.trim().toLowerCase();
    }
}