import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OpcoPage } from './opco.page';

const routes: Routes = [
  {
    path: '',
    component: OpcoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OpcoPageRoutingModule {}
