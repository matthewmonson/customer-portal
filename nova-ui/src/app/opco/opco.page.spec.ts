import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OpcoPage } from './opco.page';

describe('OpcoPage', () => {
  let component: OpcoPage;
  let fixture: ComponentFixture<OpcoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpcoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OpcoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
